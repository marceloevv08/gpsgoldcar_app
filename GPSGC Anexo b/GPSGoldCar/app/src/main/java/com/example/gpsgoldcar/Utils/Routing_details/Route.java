package com.example.gpsgoldcar.Utils.Routing_details;

public class Route {
    private int pk;
    private OriginPlace origin_place;
    private TargetPlace target_place;
    private String routing_distance_kilometres;
    private String routing_time;
    private String geofence;

    public Route(int pk, OriginPlace origin_place, TargetPlace target_place ,String routing_distance_kilometres, String routing_time) {
        this.pk = pk;
        this.origin_place = origin_place;
        this.target_place = target_place;
        this.routing_distance_kilometres=routing_distance_kilometres;
        this.origin_place=origin_place;
    }

    public Route() {
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public OriginPlace getOrigin_place() {
        return origin_place;
    }

    public void setOrigin_place(OriginPlace origin_place) {
        this.origin_place = origin_place;
    }

    public TargetPlace getTarget_place() {
        return target_place;
    }

    public void setTarget_place(TargetPlace target_place) {
        this.target_place = target_place;
    }
    public String getRouting_distance_kilometres() {
        return routing_distance_kilometres;
    }

    public void setRouting_distance_kilometres(String routing_distance_kilometres) {
        this.routing_distance_kilometres = routing_distance_kilometres;
    }

    public String getRouting_time() {
        return routing_time;
    }

    public void setRouting_time(String routing_time) {
        this.routing_time = routing_time;
    }
    public String getGeofence() {
        return geofence;
    }

    public void setGeofence(String geofence) {
        this.geofence = geofence;
    }
}
