package com.example.gpsgoldcar.Utils.Routing_details;

public class Routing_details {
    private int pk;
    private int avl_unit;
    private int driver;
    private String distance_from_origin;
    private String distance_from_destination;
    private String percentage_from_route;

    public Routing_details() {
    }

    public Routing_details(int pk, int avl_unit, int driver, String distance_from_origin, String distance_from_destination, String percentage_from_route) {
        this.pk = pk;
        this.avl_unit = avl_unit;
        this.driver = driver;
        this.distance_from_origin = distance_from_origin;
        this.distance_from_destination = distance_from_destination;
        this.percentage_from_route = percentage_from_route;
    }


    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public int getAvl_unit() {
        return avl_unit;
    }

    public void setAvl_unit(int avl_unit) {
        this.avl_unit = avl_unit;
    }

    public int getDriver() {
        return driver;
    }

    public void setDriver(int driver) {
        this.driver = driver;
    }

    public String getDistance_from_origin() {
        return distance_from_origin;
    }

    public void setDistance_from_origin(String distance_from_origin) {
        this.distance_from_origin = distance_from_origin;
    }

    public String getDistance_from_destination() {
        return distance_from_destination;
    }

    public void setDistance_from_destination(String distance_from_destination) {
        this.distance_from_destination = distance_from_destination;
    }

    public String getPercentage_from_route() {
        return percentage_from_route;
    }

    public void setPercentage_from_route(String percentage_from_route) {
        this.percentage_from_route = percentage_from_route;
    }
}
