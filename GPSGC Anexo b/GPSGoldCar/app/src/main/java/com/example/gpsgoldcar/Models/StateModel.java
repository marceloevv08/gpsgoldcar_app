package com.example.gpsgoldcar.Models;

public class StateModel {

    public String getIdState() {
        return idState;
    }

    public void setIdState(String idState) {
        this.idState = idState;
    }

    private String idState;
    private String enterprise;

    public int getId_enterprise() {
        return id_enterprise;
    }

    public void setId_enterprise(int id_enterprise) {
        this.id_enterprise = id_enterprise;
    }

    private int id_enterprise;
    private String unit;
    private String origin;
    private String destination;
    private String state;
    private String start_date;
    private String end_date;
    private String advance;

    public StateModel() {
    }

    public StateModel(String idState, String enterprise, int id_enterprise, String unit, String origin, String destination, String state, String start_date, String end_date, String advance) {
        this.idState=idState;
        this.enterprise = enterprise;
        this.id_enterprise = id_enterprise;
        this.unit = unit;
        this.origin = origin;
        this.destination = destination;
        this.state = state;
        this.start_date = start_date;
        this.end_date = end_date;
        this.advance = advance;
    }



    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getAdvance() {
        return advance;
    }

    public void setAdvance(String advance) {
        this.advance = advance;
    }
}
