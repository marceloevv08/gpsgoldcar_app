package com.example.gpsgoldcar.Utils.SchedulesModel;

public class Route {
    private int id;
    private Origin_Place origin_place;
    private Target_Place target_place;
    private boolean is_active;

    public Route(int id, Origin_Place origin_place, Target_Place target_place, boolean is_active) {
        this.id = id;
        this.origin_place = origin_place;
        this.target_place = target_place;
        this.is_active = is_active;
    }

    public Route() {
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public Origin_Place getOrigin_place() {
        return origin_place;
    }

    public void setOrigin_place(Origin_Place origin_place) {
        this.origin_place = origin_place;
    }

    public Target_Place getTarget_place() {
        return target_place;
    }

    public void setTarget_place(Target_Place target_place) {
        this.target_place = target_place;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }
}
