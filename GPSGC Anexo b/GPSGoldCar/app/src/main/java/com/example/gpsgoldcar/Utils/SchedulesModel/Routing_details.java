package com.example.gpsgoldcar.Utils.SchedulesModel;

public class Routing_details {

    private String avl_unit;
    private int avl_unit_id;
    private String driver;
    private int driver_id;

    public Routing_details( String avl_unit, int avl_unit_id, String driver, int driver_id) {
        this.avl_unit = avl_unit;
        this.avl_unit_id = avl_unit_id;
        this.driver = driver;
        this.driver_id = driver_id;
    }


    public Routing_details() {
    }



    public String getAvl_unit() {
        return avl_unit;
    }

    public void setAvl_unit(String avl_unit) {
        this.avl_unit = avl_unit;
    }

    public int getAvl_unit_id() {
        return avl_unit_id;
    }

    public void setAvl_unit_id(int avl_unit_id) {
        this.avl_unit_id = avl_unit_id;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }
}
