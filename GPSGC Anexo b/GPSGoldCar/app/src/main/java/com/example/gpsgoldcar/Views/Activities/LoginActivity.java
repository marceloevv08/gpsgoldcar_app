package com.example.gpsgoldcar.Views.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.gpsgoldcar.Config.ConstValue;
import com.example.gpsgoldcar.Database.SqliteClass;
import com.example.gpsgoldcar.Models.DriverModel;
import com.example.gpsgoldcar.Models.ScheduleModel;
import com.example.gpsgoldcar.Models.StateModel;
import com.example.gpsgoldcar.Models.VehicleModel;
import com.example.gpsgoldcar.Network.CheckInternet;
import com.example.gpsgoldcar.Network.Protocol;
import com.example.gpsgoldcar.R;
import com.example.gpsgoldcar.Utils.AuthClass;
import com.example.gpsgoldcar.Utils.InterfaceAPI;
import com.example.gpsgoldcar.Utils.RetrofitClientInstance;
import com.example.gpsgoldcar.Utils.SchedulesModel.Filter;
import com.example.gpsgoldcar.Utils.SchedulesModel.Routing_details;
import com.example.gpsgoldcar.Utils.SchedulesModel.ScheduleModelInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class LoginActivity extends AppCompatActivity {
    Intent i;
    Context ctx = this;
    ProgressBar progressBar;
    Button btn_login;
    EditText et_username, et_password;
    String username, password;
    Protocol protocol;
    VehicleModel vehicleModel;
    DriverModel driverModel;
    ScheduleModel scheduleModel;
    String url_vehicles, url_drivers, url_states;
    CheckInternet checkInternet;
    StateModel stateModel;
    Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
    final InterfaceAPI api = retrofit.create(InterfaceAPI.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        protocol = new Protocol();
        checkInternet = new CheckInternet(ctx);
        btn_login = (Button) findViewById(R.id.bt_login);
        progressBar = (ProgressBar) findViewById(R.id.loading);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

        /** Mantener Sesión abierta **/
        SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
        String loged = sharedPref.getString("loged", "inactive");
        assert loged != null;
        if (loged.equals("active")) {
            Intent intent = new Intent(LoginActivity.this, Transition.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_username.getText().length() > 0 && et_password.getText().length() > 0) {
                    if (checkInternet.isNetworkConnected()) {
                        try {
                            et_password.onEditorAction(EditorInfo.IME_ACTION_DONE);
                            new LoginTask().execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toasty.warning(ctx, "Su dispositivo no cuenta con conexión a internet", Toast.LENGTH_SHORT, true).show();
                    }
                } else {
                    Toasty.warning(ctx, "No ha ingresado un usuario o una contraseña", Toast.LENGTH_SHORT, true).show();
                }
            }
        });
    }

    public void checkLogin() throws JSONException {
        username = et_username.getText().toString();
        password = et_password.getText().toString();
        AuthClass authentication = new AuthClass(username, password);
        Call<AuthClass> call = api.checkLogin(authentication);
        call.enqueue(new Callback<AuthClass>() {
            @Override
            public void onResponse(@NonNull Call<AuthClass> call, @NonNull Response<AuthClass> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        assert response.body() != null;

                        int user_id = response.body().getUser_id();
                        int idCompany = response.body().getEmployee_company_id();
                        String user_first_name = response.body().getUser_first_name();
                        String user_last_name = response.body().getUser_last_name();
                        String user_email = response.body().getUser_email();
                        boolean user_is_staff = response.body().isUser_is_staff();
                        boolean isAdmin = response.body().isUser_is_super();
                        boolean employee_is_staff = response.body().isEmployee_is_staff();
                        String nameCompany = response.body().getEmployee_company_name();

                        @SuppressLint("HardwareIds") String uuid = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
                        SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("token", response.body().getToken());
                        editor.putInt("user_id",user_id);
                        editor.putString("username", username);
                        editor.putString("password", password);
                        editor.putString("user_first_name",user_first_name);
                        editor.putString("user_last_name",user_last_name);
                        editor.putString("user_email",user_email);
                        editor.putBoolean("user_is_staff", user_is_staff);
                        editor.putBoolean("user_is_super", isAdmin);
                        editor.putInt("employee_company_id", idCompany);
                        editor.putString("employee_company_name", nameCompany);
                        editor.putBoolean("employee_is_staff", employee_is_staff);
                        editor.putString("uuid", uuid);
                        editor.putString("loged", "active");
                        editor.apply();
                        Toasty.success(ctx, "Bienvenido", Toast.LENGTH_SHORT, true).show();
                        i = new Intent(ctx, Transition.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                        finish();

                    } else {
                        Toasty.warning(ctx, "Usuario y/o contraseña inválidos", Toast.LENGTH_SHORT, true).show();
                        et_username.setText("");
                        et_password.setText("");
                    }
                } else {
                    Toasty.warning(ctx, "Usuario y/o contraseña inválidos", Toast.LENGTH_SHORT, true).show();
                    et_username.setText("");
                    et_password.setText("");
                }
            }


            @Override
            public void onFailure(@NonNull Call<AuthClass> call, @NonNull Throwable t) {
                Log.e("TAG", t.toString());
                t.printStackTrace();
            }
        });
    }

    public String createBasicAuth(String username, String password) {
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }


    class LoginTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            username = et_username.getText().toString();
            password = et_password.getText().toString();
            try {
                /**Login */
                checkLogin();
                /**Consulta para almacenar conductores , vehículos, estados y programaciones*/
                JSONObject jsonLogin = new JSONObject();
                jsonLogin.put("username", username);
                jsonLogin.put("password", password);
                JSONObject response = protocol.postLogin(ConstValue.AUTH_LOGIN, jsonLogin);
                if (response.isNull("token")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en el servidor", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else {
                    boolean isAdmin = response.getBoolean("user_is_super");
                    int idCompany = response.getInt("employee_company_id");

                    /**Guardando Conductores*/
                    String AUTH1 = createBasicAuth(username, password);
                    url_drivers = ConstValue.GET_DRIVERS;
                    JSONArray jsonArray1 = new JSONArray(protocol.getJson(url_drivers, AUTH1));
                    if (isAdmin) {
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            driverModel = new DriverModel();
                            driverModel.setId_driv(jsonObject1.getInt("pk"));
                            driverModel.setNombre(jsonObject1.getString("name"));
                            JSONArray jsonArray = (JSONArray) jsonObject1.get("party_identifiers");
                            JSONObject jsonObject2 = jsonArray.getJSONObject(0);
                            driverModel.setDni(jsonObject2.getString("identifier_number"));
                            driverModel.setF_dni(jsonObject2.getString("identifier_due_date"));
                            JSONObject jsonObject3 = jsonObject1.getJSONObject("party_driver");
                            driverModel.setLic_con(jsonObject3.getString("license_due_date"));
                            JSONObject jsonObject4 = jsonObject3.getJSONObject("driver_anexo_b");
                            driverModel.setSctr(jsonObject4.getString("sctr"));
                            driverModel.setPas_med(jsonObject4.getString("medical_pass"));
                            driverModel.setInduccion(jsonObject4.getString("induction"));
                            driverModel.setFtcheck(jsonObject4.getString("photo_check"));
                            driverModel.setId_compania(jsonObject3.getInt("company"));
                            driverModel.setEstado(jsonObject1.getString("is_active"));
                            SqliteClass.getInstance(ctx).databasehelp.appDriverSql.addDriver(driverModel);
                        }
                    } else {
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            JSONArray jsonArray = (JSONArray) jsonObject1.get("party_identifiers");
                            JSONObject jsonObject2 = jsonArray.getJSONObject(0);
                            JSONObject jsonObject3 = jsonObject1.getJSONObject("party_driver");
                            JSONObject jsonObject4 = jsonObject3.getJSONObject("driver_anexo_b");
                            driverModel = new DriverModel();
                            if (jsonObject3.getInt("company") == idCompany) {
                                driverModel.setId_driv(jsonObject1.getInt("pk"));
                                driverModel.setNombre(jsonObject1.getString("name"));
                                driverModel.setDni(jsonObject2.getString("identifier_number"));
                                driverModel.setF_dni(jsonObject2.getString("identifier_due_date"));
                                driverModel.setLic_con(jsonObject3.getString("license_due_date"));
                                driverModel.setSctr(jsonObject4.getString("sctr"));
                                driverModel.setPas_med(jsonObject4.getString("medical_pass"));
                                driverModel.setInduccion(jsonObject4.getString("induction"));
                                driverModel.setFtcheck(jsonObject4.getString("photo_check"));
                                driverModel.setId_compania(jsonObject3.getInt("company"));
                                driverModel.setEstado(jsonObject1.getString("is_active"));
                                SqliteClass.getInstance(ctx).databasehelp.appDriverSql.addDriver(driverModel);
                            }
                        }
                    }
                    /**Guardando Programaciones*/
                    Filter filter = new Filter();
                    filter.setAll_programming("true");
                    filter.setDate_end("");
                    filter.setDate_ini("");
                    filter.setPk_route("");
                    filter.setPk_state("");
                    if (isAdmin) {
                        filter.setPk_company("");
                    } else {
                        filter.setPk_company(String.valueOf(idCompany));
                    }
                    Call<List<ScheduleModelInterface>> getSchedules = api.getSchedules(AUTH1, filter);
                    List<ScheduleModelInterface> scheduleModelInterfaces = getSchedules.execute().body();
                    if (scheduleModelInterfaces != null) {
                        if (scheduleModelInterfaces.size() > 0) {
                            for (ScheduleModelInterface scheduleModelInterface : scheduleModelInterfaces) {
                                scheduleModel = new ScheduleModel();
                                scheduleModel.setId_schedules(scheduleModelInterface.getId());
                                Log.i("Carajo ids",scheduleModelInterface.getId()+"");
                                if (scheduleModelInterface.getPrograming() != null) {
                                    scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                    scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                    scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                    scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                    scheduleModel.setId_compania(scheduleModelInterface.getPrograming().getCompany().getPk());
                                    scheduleModel.setCompania(scheduleModelInterface.getPrograming().getCompany().getParty());
                                    List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                    String[] conductores = new String[routing_details_list.size()];
                                    String[] unidades = new String[routing_details_list.size()];
                                    if (routing_details_list.size() > 0) {
                                        for (int i = 0; i < routing_details_list.size(); i++) {
                                            conductores[i] = routing_details_list.get(i).getDriver();
                                            unidades[i] = routing_details_list.get(i).getAvl_unit();
                                        }
                                        scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                        scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                    } else {
                                        scheduleModel.setConductor("Sin asignar");
                                        scheduleModel.setUnidad("Sin asignar");
                                    }
                                    scheduleModel.setProducto(scheduleModelInterface.getPrograming().getProduct().getName());
                                    scheduleModel.setEstado(scheduleModelInterface.getState());

                                } else {
                                    scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                    scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                    scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                    scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                    scheduleModel.setId_compania(0);
                                    scheduleModel.setCompania("Sin asignar");
                                    List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                    String[] conductores = new String[routing_details_list.size()];
                                    String[] unidades = new String[routing_details_list.size()];
                                    if (routing_details_list.size() > 0) {
                                        for (int i = 0; i < routing_details_list.size(); i++) {
                                            conductores[i] = routing_details_list.get(i).getDriver();
                                            unidades[i] = routing_details_list.get(i).getAvl_unit();
                                        }
                                        scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                        scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                    } else {
                                        scheduleModel.setConductor("Sin asignar");
                                        scheduleModel.setUnidad("Sin asignar");
                                    }
                                    scheduleModel.setProducto("Sin asignar");
                                    scheduleModel.setEstado(scheduleModelInterface.getState());

                                }
                                SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.addSchedule(scheduleModel);
                            }
                        }

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(ctx, "Error en recuperación de programaciones o el usuario no contiene programaciones", Toast.LENGTH_LONG, true).show();
                            }
                        });

                    }

                    /**
                     * Guardando Estados
                     */
                    String AUTH3 = createBasicAuth(username, password);
                    url_states = ConstValue.GET_STATES;
                    JSONArray jsonArrayStates = new JSONArray(protocol.getJson(url_states, AUTH3));
                    if (jsonArrayStates.length() > 0) {
                        if (isAdmin) {
                            for (int i = 0; i < jsonArrayStates.length(); i++) {
                                JSONObject jsonState = jsonArrayStates.getJSONObject(i);
                                stateModel = new StateModel();
                                stateModel.setEnterprise(jsonState.getString("enterprise"));
                                stateModel.setId_enterprise(jsonState.getInt("id_enterprise"));
                                stateModel.setUnit(jsonState.getString("unit"));
                                stateModel.setOrigin(jsonState.getString("origin"));
                                stateModel.setDestination(jsonState.getString("destination"));
                                stateModel.setState(jsonState.getString("state"));
                                stateModel.setStart_date(jsonState.getString("star_date"));
                                stateModel.setEnd_date(jsonState.getString("end_date"));
                                stateModel.setAdvance(jsonState.getString("advance"));
                                SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);

                            }
                        } else {
                            for (int i = 0; i < jsonArrayStates.length(); i++) {
                                JSONObject jsonState = jsonArrayStates.getJSONObject(i);
                                stateModel = new StateModel();
                                if (jsonState.getInt("id_enterprise") == idCompany) {
                                    stateModel.setEnterprise(jsonState.getString("enterprise"));
                                    stateModel.setId_enterprise(jsonState.getInt("id_enterprise"));
                                    stateModel.setUnit(jsonState.getString("unit"));
                                    stateModel.setOrigin(jsonState.getString("origin"));
                                    stateModel.setDestination(jsonState.getString("destination"));
                                    stateModel.setState(jsonState.getString("state"));
                                    stateModel.setStart_date(jsonState.getString("star_date"));
                                    stateModel.setEnd_date(jsonState.getString("end_date"));
                                    stateModel.setAdvance(jsonState.getString("advance"));
                                    SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);
                                }
                            }
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(ctx, "Error en recuperación de estados o la empresa no contiene estados", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }

                    /**
                     * Guardando Vehiculos
                     **/
                    String AUTH = createBasicAuth(username, password);
                    url_vehicles = ConstValue.GET_VEHICLES;
                    JSONArray jsonArrayVehicles = new JSONArray(protocol.getJson(url_vehicles, AUTH));
                    if (jsonArrayVehicles.length() > 0) {
                        if (isAdmin) {
                            for (int i = 0; i < jsonArrayVehicles.length(); i++) {
                                JSONObject jsonVehicle = jsonArrayVehicles.getJSONObject(i);
                                vehicleModel = new VehicleModel();
                                vehicleModel.setId_vehicle(jsonVehicle.getInt("pk"));
                                vehicleModel.setId_company(jsonVehicle.getInt("company"));
                                vehicleModel.setEstado(jsonVehicle.getString("is_active"));
                                JSONObject jsonArrayVehicle = (JSONObject) jsonVehicle.get("avl_unit");
                                vehicleModel.setPlaca(jsonArrayVehicle.getString("avl_unit_name"));
                                JSONObject jsonObjectVehicle1 = jsonArrayVehicle.getJSONObject("vehicle");
                                vehicleModel.setSoat(jsonObjectVehicle1.getString("insurance_soat"));
                                vehicleModel.setPoliza(jsonObjectVehicle1.getString("insurance_policy"));
                                vehicleModel.setRev_tec(jsonObjectVehicle1.getString("technical_review"));
                                vehicleModel.setVig_contrato(jsonObjectVehicle1.getString("contract_validity"));
                                vehicleModel.setGps(jsonObjectVehicle1.getString("contrac_gps"));
                                SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.addVehicle(vehicleModel);
                            }
                        } else {
                            for (int i = 0; i < jsonArrayVehicles.length(); i++) {
                                JSONObject jsonVehicle = jsonArrayVehicles.getJSONObject(i);
                                vehicleModel = new VehicleModel();
                                if (jsonVehicle.getInt("company") == idCompany) {
                                    vehicleModel.setId_vehicle(jsonVehicle.getInt("pk"));
                                    vehicleModel.setId_company(jsonVehicle.getInt("company"));
                                    vehicleModel.setEstado(jsonVehicle.getString("is_active"));
                                    JSONObject jsonArrayVehicle = (JSONObject) jsonVehicle.get("avl_unit");
                                    vehicleModel.setPlaca(jsonArrayVehicle.getString("avl_unit_name"));
                                    JSONObject jsonObjectVehicle1 = jsonArrayVehicle.getJSONObject("vehicle");
                                    vehicleModel.setSoat(jsonObjectVehicle1.getString("insurance_soat"));
                                    vehicleModel.setPoliza(jsonObjectVehicle1.getString("insurance_policy"));
                                    vehicleModel.setRev_tec(jsonObjectVehicle1.getString("technical_review"));
                                    vehicleModel.setVig_contrato(jsonObjectVehicle1.getString("contract_validity"));
                                    vehicleModel.setGps(jsonObjectVehicle1.getString("contrac_gps"));
                                    SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.addVehicle(vehicleModel);
                                }
                            }
                        }

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(ctx, "Error en recuperación de vehículos o la empresa no contiene vehículos", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.GONE);
            /** Activar el estado de sessión main **/
            SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("logueado", "active");
            editor.apply();
            super.onPostExecute(s);
        }
    }
}




