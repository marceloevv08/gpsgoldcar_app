package com.example.gpsgoldcar.Utils.Routing_details;

public class AvlUnitRouting {
    private int pk;
    private String avl_unit_name;
    private AVLRD avl_unit_routing_detail;

    public AvlUnitRouting(int pk, String avl_unit_name, AVLRD avl_unit_routing_detail) {
        this.pk = pk;
        this.avl_unit_name = avl_unit_name;
        this.avl_unit_routing_detail = avl_unit_routing_detail;
    }

    public AvlUnitRouting() {
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getAvl_unit_name() {
        return avl_unit_name;
    }

    public void setAvl_unit_name(String avl_unit_name) {
        this.avl_unit_name = avl_unit_name;
    }

    public AVLRD getAvl_unit_routing_detail() {
        return avl_unit_routing_detail;
    }

    public void setAvl_unit_routing_detail(AVLRD avl_unit_routing_detail) {
        this.avl_unit_routing_detail = avl_unit_routing_detail;
    }
}
