package com.example.gpsgoldcar.Utils.Routing_details;

public class TargetPlace {
    private String type;
    private Properties properties;

    public TargetPlace() {
    }

    public TargetPlace(String type, Properties properties) {
        this.type = type;
        this.properties = properties;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
