package com.example.gpsgoldcar.Utils.Binnacle;

public class Values {
    private String value_past;
    private String value_new;

    public Values(String value_past, String value_new) {
        this.value_past = value_past;
        this.value_new = value_new;
    }

    public Values() {
    }

    public String getValue_past() {
        return value_past;
    }

    public void setValue_past(String value_past) {
        this.value_past = value_past;
    }

    public String getValue_new() {
        return value_new;
    }

    public void setValue_new(String value_new) {
        this.value_new = value_new;
    }
}
