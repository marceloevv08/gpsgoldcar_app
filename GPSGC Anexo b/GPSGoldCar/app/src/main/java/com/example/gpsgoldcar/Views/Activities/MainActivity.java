package com.example.gpsgoldcar.Views.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.example.gpsgoldcar.Config.ConstValue;
import com.example.gpsgoldcar.Database.SqliteClass;
import com.example.gpsgoldcar.Models.DriverModel;
import com.example.gpsgoldcar.Models.ScheduleModel;
import com.example.gpsgoldcar.Models.StateModel;
import com.example.gpsgoldcar.Models.VehicleModel;
import com.example.gpsgoldcar.Network.CheckInternet;
import com.example.gpsgoldcar.Network.Protocol;
import com.example.gpsgoldcar.R;
import com.example.gpsgoldcar.Utils.InterfaceAPI;
import com.example.gpsgoldcar.Utils.RetrofitClientInstance;
import com.example.gpsgoldcar.Utils.SchedulesModel.Filter;
import com.example.gpsgoldcar.Utils.SchedulesModel.Routing_details;
import com.example.gpsgoldcar.Utils.SchedulesModel.ScheduleModelInterface;
import com.example.gpsgoldcar.Views.Activities.Drivers.DriversActivity;
import com.example.gpsgoldcar.Views.Activities.Schedules.SchedulesActivity;
import com.example.gpsgoldcar.Views.Activities.Vehicles.VehiclesActivity;
import com.example.gpsgoldcar.Views.Dialogs.Dialogs;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonIOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import es.dmoral.toasty.Toasty;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, EasyPermissions.PermissionCallbacks {

    MaterialToolbar toolbar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Intent i;
    Context ctx;
    CardView schedules, state, drivers, vehicles1;
    String url_vehicles, url_drivers, url_states;
    Protocol protocol;
    VehicleModel vehicleModel;
    DriverModel driverModel;
    ScheduleModel scheduleModel;
    StateModel stateModel;
    CheckInternet checkInternet;
    Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
    final InterfaceAPI api = retrofit.create(InterfaceAPI.class);

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions();
        ctx = this;
        protocol = new Protocol();
        checkInternet = new CheckInternet(ctx);
        toolbar = (MaterialToolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.draw_lay);
        SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
        String username = sharedPref.getString("username", "");
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.bringToFront();
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.Usuario);
        navUsername.setText(username);
        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_d, R.string.close_d);
        drawerLayout.addDrawerListener(toogle);
        toogle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        schedules = (CardView) findViewById(R.id.listadom);
        state = (CardView) findViewById(R.id.estadom);
        drivers = (CardView) findViewById(R.id.driversm);
        vehicles1 = (CardView) findViewById(R.id.vehiclesm);
        schedules.setOnClickListener(this);
        state.setOnClickListener(this);
        drivers.setOnClickListener(this);
        vehicles1.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
        Toast.makeText(ctx, "No puede salir de la aplicación", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.map:
                i = new Intent(ctx,MapActivity.class);
                startActivity(i);
                break;*/
            case R.id.list:
                i = new Intent(ctx, SchedulesActivity.class);
                startActivity(i);
                navigationView.getMenu().findItem(R.id.state).setEnabled(false);
                navigationView.getMenu().findItem(R.id.drivers).setEnabled(false);
                navigationView.getMenu().findItem(R.id.vehicles).setEnabled(false);
                navigationView.getMenu().findItem(R.id.logout).setEnabled(false);
                navigationView.getMenu().findItem(R.id.update_data).setEnabled(false);
                return true;
            case R.id.state:
                i = new Intent(ctx, StatesActivity.class);
                startActivity(i);
                navigationView.getMenu().findItem(R.id.list).setEnabled(false);
                navigationView.getMenu().findItem(R.id.drivers).setEnabled(false);
                navigationView.getMenu().findItem(R.id.vehicles).setEnabled(false);
                navigationView.getMenu().findItem(R.id.logout).setEnabled(false);
                navigationView.getMenu().findItem(R.id.update_data).setEnabled(false);
                return true;
            case R.id.drivers:
                i = new Intent(ctx, DriversActivity.class);
                startActivity(i);
                navigationView.getMenu().findItem(R.id.state).setEnabled(false);
                navigationView.getMenu().findItem(R.id.list).setEnabled(false);
                navigationView.getMenu().findItem(R.id.vehicles).setEnabled(false);
                navigationView.getMenu().findItem(R.id.logout).setEnabled(false);
                navigationView.getMenu().findItem(R.id.update_data).setEnabled(false);
                return true;
            case R.id.logout:
                Dialogs.showLogoutDialog(MainActivity.this, ctx);
                return true;

            case R.id.vehicles:
                i = new Intent(ctx, VehiclesActivity.class);
                startActivity(i);
                navigationView.getMenu().findItem(R.id.state).setEnabled(false);
                navigationView.getMenu().findItem(R.id.drivers).setEnabled(false);
                navigationView.getMenu().findItem(R.id.list).setEnabled(false);
                navigationView.getMenu().findItem(R.id.logout).setEnabled(false);
                navigationView.getMenu().findItem(R.id.update_data).setEnabled(false);
                return true;
            case R.id.update_data:
                if (checkInternet.isNetworkConnected()) {
                    new updateTask().execute(true);
                } else {
                    Toast.makeText(ctx, "Su dispositivo no cuenta con conexión a Internet", Toast.LENGTH_LONG).show();
                }
                return true;

            default:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.listadom:
                schedules.setEnabled(false);
                state.setEnabled(false);
                drivers.setEnabled(false);
                vehicles1.setEnabled(false);
                i = new Intent(ctx, SchedulesActivity.class);
                startActivity(i);
                break;
            case R.id.estadom:
                schedules.setEnabled(false);
                state.setEnabled(false);
                drivers.setEnabled(false);
                vehicles1.setEnabled(false);
                i = new Intent(ctx, StatesActivity.class);
                startActivity(i);
                break;
            case R.id.driversm:
                schedules.setEnabled(false);
                state.setEnabled(false);
                drivers.setEnabled(false);
                vehicles1.setEnabled(false);
                i = new Intent(ctx, DriversActivity.class);
                startActivity(i);
                break;
            case R.id.vehiclesm:
                schedules.setEnabled(false);
                state.setEnabled(false);
                drivers.setEnabled(false);
                vehicles1.setEnabled(false);
                i = new Intent(ctx, VehiclesActivity.class);
                startActivity(i);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        schedules.setEnabled(true);
        state.setEnabled(true);
        drivers.setEnabled(true);
        vehicles1.setEnabled(true);
        navigationView.getMenu().findItem(R.id.list).setEnabled(true);
        navigationView.getMenu().findItem(R.id.state).setEnabled(true);
        navigationView.getMenu().findItem(R.id.drivers).setEnabled(true);
        navigationView.getMenu().findItem(R.id.vehicles).setEnabled(true);
        navigationView.getMenu().findItem(R.id.logout).setEnabled(true);
        navigationView.getMenu().findItem(R.id.update_data).setEnabled(true);
    }

    public void requestPermissions(){
        String[] perms={
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q){
            if(EasyPermissions.hasPermissions(this,perms)) {

            }
            else {
                EasyPermissions.requestPermissions(this,"Necesita permisos para continuar",123,perms);
            }
        }else {
            EasyPermissions.hasPermissions(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            );
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this,perms)){
            new AppSettingsDialog.Builder(this).build().show();
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint("StaticFieldLeak")
    class updateTask extends AsyncTask<Boolean, Void, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.deleteAppVehicleTable();
            SqliteClass.getInstance(ctx).databasehelp.appDriverSql.deleteAppDriverTable();
            SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.deleteAppScheduleTable();
            SqliteClass.getInstance(ctx).databasehelp.appStateSql.deleteAppStateTable();
            dialog = ProgressDialog.show(ctx, "", getString(R.string.action_loading), true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Intent intent = new Intent(ctx, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            finish();
            startActivity(intent);
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
            String username = sharedPref.getString("username", "");
            String password = sharedPref.getString("password", "");
            boolean isAdmin = sharedPref.getBoolean("user_is_super", true);
            int idCompany = sharedPref.getInt("employee_company_id", 0);
            try {
                /** Actualizando vehiculos**/
                String AUTH = createBasicAuth(username, password);
                url_vehicles = ConstValue.GET_VEHICLES;
                JSONArray jsonArrayVehicles = new JSONArray(protocol.getJson(url_vehicles, AUTH));
                if (jsonArrayVehicles.length() > 0) {
                    if (isAdmin) {
                        for (int i = 0; i < jsonArrayVehicles.length(); i++) {
                            JSONObject jsonVehicle = jsonArrayVehicles.getJSONObject(i);
                            vehicleModel = new VehicleModel();
                            vehicleModel.setId_vehicle(jsonVehicle.getInt("pk"));
                            vehicleModel.setId_company(jsonVehicle.getInt("company"));
                            vehicleModel.setEstado(jsonVehicle.getString("is_active"));
                            JSONObject jsonArrayVehicle = (JSONObject) jsonVehicle.get("avl_unit");
                            vehicleModel.setPlaca(jsonArrayVehicle.getString("avl_unit_name"));
                            JSONObject jsonObjectVehicle1 = jsonArrayVehicle.getJSONObject("vehicle");
                            vehicleModel.setSoat(jsonObjectVehicle1.getString("insurance_soat"));
                            vehicleModel.setPoliza(jsonObjectVehicle1.getString("insurance_policy"));
                            vehicleModel.setRev_tec(jsonObjectVehicle1.getString("technical_review"));
                            vehicleModel.setVig_contrato(jsonObjectVehicle1.getString("contract_validity"));
                            vehicleModel.setGps(jsonObjectVehicle1.getString("contrac_gps"));
                            SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.addVehicle(vehicleModel);
                        }
                    } else {
                        for (int i = 0; i < jsonArrayVehicles.length(); i++) {
                            JSONObject jsonVehicle = jsonArrayVehicles.getJSONObject(i);
                            vehicleModel = new VehicleModel();
                            if (jsonVehicle.getInt("company") == idCompany) {
                                vehicleModel.setId_vehicle(jsonVehicle.getInt("pk"));
                                vehicleModel.setId_company(jsonVehicle.getInt("company"));
                                vehicleModel.setEstado(jsonVehicle.getString("is_active"));
                                JSONObject jsonArrayVehicle = (JSONObject) jsonVehicle.get("avl_unit");
                                vehicleModel.setPlaca(jsonArrayVehicle.getString("avl_unit_name"));
                                JSONObject jsonObjectVehicle1 = jsonArrayVehicle.getJSONObject("vehicle");
                                vehicleModel.setSoat(jsonObjectVehicle1.getString("insurance_soat"));
                                vehicleModel.setPoliza(jsonObjectVehicle1.getString("insurance_policy"));
                                vehicleModel.setRev_tec(jsonObjectVehicle1.getString("technical_review"));
                                vehicleModel.setVig_contrato(jsonObjectVehicle1.getString("contract_validity"));
                                vehicleModel.setGps(jsonObjectVehicle1.getString("contrac_gps"));
                                SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.addVehicle(vehicleModel);
                                Log.i("Vehiculos", vehicleModel.toString());
                            }
                        }
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ctx, "Error en recuperación de vehiculos o la empresa no contiene vehiculos", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                /** Actualizando conductores**/
                String AUTH1 = createBasicAuth(username, password);
                url_drivers = ConstValue.GET_DRIVERS;
                JSONArray jsonArray1 = new JSONArray(protocol.getJson(url_drivers, AUTH1));
                if(jsonArray1.length()>0){
                    Log.i("Conductores", jsonArray1.toString());
                    if(isAdmin){
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            driverModel = new DriverModel();
                            driverModel.setId_driv(jsonObject1.getInt("pk"));
                            driverModel.setNombre(jsonObject1.getString("name"));
                            JSONArray jsonArray = (JSONArray) jsonObject1.get("party_identifiers");
                            JSONObject jsonObject2 = jsonArray.getJSONObject(0);
                            driverModel.setDni(jsonObject2.getString("identifier_number"));
                            driverModel.setF_dni(jsonObject2.getString("identifier_due_date"));
                            JSONObject jsonObject3 = jsonObject1.getJSONObject("party_driver");
                            driverModel.setLic_con(jsonObject3.getString("license_due_date"));
                            JSONObject jsonObject4 = jsonObject3.getJSONObject("driver_anexo_b");
                            driverModel.setSctr(jsonObject4.getString("sctr"));
                            driverModel.setPas_med(jsonObject4.getString("medical_pass"));
                            driverModel.setInduccion(jsonObject4.getString("induction"));
                            driverModel.setFtcheck(jsonObject4.getString("photo_check"));
                            driverModel.setId_compania(jsonObject3.getInt("company"));
                            driverModel.setEstado(jsonObject1.getString("is_active"));
                            SqliteClass.getInstance(ctx).databasehelp.appDriverSql.addDriver(driverModel);
                        }
                    }else{
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            JSONArray jsonArray = (JSONArray) jsonObject1.get("party_identifiers");
                            JSONObject jsonObject2 = jsonArray.getJSONObject(0);
                            JSONObject jsonObject3 = jsonObject1.getJSONObject("party_driver");
                            JSONObject jsonObject4 = jsonObject3.getJSONObject("driver_anexo_b");
                            driverModel = new DriverModel();
                            if(jsonObject3.getInt("company")== idCompany){
                                driverModel.setId_driv(jsonObject1.getInt("pk"));
                                driverModel.setNombre(jsonObject1.getString("name"));
                                driverModel.setDni(jsonObject2.getString("identifier_number"));
                                driverModel.setF_dni(jsonObject2.getString("identifier_due_date"));
                                driverModel.setLic_con(jsonObject3.getString("license_due_date"));
                                driverModel.setSctr(jsonObject4.getString("sctr"));
                                driverModel.setPas_med(jsonObject4.getString("medical_pass"));
                                driverModel.setInduccion(jsonObject4.getString("induction"));
                                driverModel.setFtcheck(jsonObject4.getString("photo_check"));
                                driverModel.setId_compania(jsonObject3.getInt("company"));
                                driverModel.setEstado(jsonObject1.getString("is_active"));
                                SqliteClass.getInstance(ctx).databasehelp.appDriverSql.addDriver(driverModel);
                            }
                        }
                    }
                }else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ctx, "Error en recuperación de conductores o la empresa no contiene conductores", Toast.LENGTH_LONG).show();
                        }
                    });
                }

                /** Actualizando schedules**/
                Filter filter = new Filter();
                filter.setAll_programming("true");
                filter.setDate_end("");
                filter.setDate_ini("");
                filter.setPk_route("");
                filter.setPk_state("");
                if (isAdmin) {
                    filter.setPk_company("");
                } else {
                    filter.setPk_company(String.valueOf(idCompany));
                }
                Call<List<ScheduleModelInterface>> getSchedules = api.getSchedules(AUTH1, filter);
                List<ScheduleModelInterface> scheduleModelInterfaces = getSchedules.execute().body();
                if (scheduleModelInterfaces != null) {
                    if (scheduleModelInterfaces.size() > 0) {
                        for (ScheduleModelInterface scheduleModelInterface : scheduleModelInterfaces) {
                            scheduleModel = new ScheduleModel();
                            scheduleModel.setId_schedules(scheduleModelInterface.getId());
                            if (scheduleModelInterface.getPrograming() != null) {
                                scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                scheduleModel.setId_compania(scheduleModelInterface.getPrograming().getCompany().getPk());
                                scheduleModel.setCompania(scheduleModelInterface.getPrograming().getCompany().getParty());
                                List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                String[] conductores = new String[routing_details_list.size()];
                                String[] unidades = new String[routing_details_list.size()];
                                if (routing_details_list.size() > 0) {
                                    for (int i = 0; i < routing_details_list.size(); i++) {
                                        conductores[i] = routing_details_list.get(i).getDriver();
                                        unidades[i] = routing_details_list.get(i).getAvl_unit();
                                    }
                                    scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                    scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                } else {
                                    scheduleModel.setConductor("Sin asignar");
                                    scheduleModel.setUnidad("Sin asignar");
                                }
                                scheduleModel.setProducto(scheduleModelInterface.getPrograming().getProduct().getName());
                                scheduleModel.setEstado(scheduleModelInterface.getState());

                            } else {
                                scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                scheduleModel.setId_compania(0);
                                scheduleModel.setCompania("Sin asignar");
                                List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                String[] conductores = new String[routing_details_list.size()];
                                String[] unidades = new String[routing_details_list.size()];
                                if (routing_details_list.size() > 0) {
                                    for (int i = 0; i < routing_details_list.size(); i++) {
                                        conductores[i] = routing_details_list.get(i).getDriver();
                                        unidades[i] = routing_details_list.get(i).getAvl_unit();
                                    }
                                    scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                    scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                } else {
                                    scheduleModel.setConductor("Sin asignar");
                                    scheduleModel.setUnidad("Sin asignar");
                                }
                                scheduleModel.setProducto("Sin asignar");
                                scheduleModel.setEstado(scheduleModelInterface.getState());

                            }
                            SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.addSchedule(scheduleModel);
                        }
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en recuperación de programaciones o el usuario no contiene programaciones", Toast.LENGTH_LONG, true).show();
                        }
                    });

                }
                /** Actualizando estados**/

                String AUTH3 = createBasicAuth(username, password);
                url_states = ConstValue.GET_STATES;
                JSONArray jsonArrayStates = new JSONArray(protocol.getJson(url_states, AUTH3));
                if (jsonArrayStates.length() > 0) {
                    Log.i("Programaciones Estados", jsonArrayStates.toString());
                    if (isAdmin) {
                        for (int i = 0; i < jsonArrayStates.length(); i++) {
                            JSONObject jsonState = jsonArrayStates.getJSONObject(i);
                            stateModel = new StateModel();
                            stateModel.setEnterprise(jsonState.getString("enterprise"));
                            stateModel.setId_enterprise(jsonState.getInt("id_enterprise"));
                            stateModel.setUnit(jsonState.getString("unit"));
                            stateModel.setOrigin(jsonState.getString("origin"));
                            stateModel.setDestination(jsonState.getString("destination"));
                            stateModel.setState(jsonState.getString("state"));
                            stateModel.setStart_date(jsonState.getString("star_date"));
                            stateModel.setEnd_date(jsonState.getString("end_date"));
                            stateModel.setAdvance(jsonState.getString("advance"));
                            SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);

                        }
                    } else {
                        for (int i = 0; i < jsonArrayStates.length(); i++) {
                            JSONObject jsonState = jsonArrayStates.getJSONObject(i);
                            stateModel = new StateModel();
                            if (jsonState.getInt("id_enterprise") == idCompany) {
                                stateModel.setEnterprise(jsonState.getString("enterprise"));
                                stateModel.setId_enterprise(jsonState.getInt("id_enterprise"));
                                stateModel.setUnit(jsonState.getString("unit"));
                                stateModel.setOrigin(jsonState.getString("origin"));
                                stateModel.setDestination(jsonState.getString("destination"));
                                stateModel.setState(jsonState.getString("state"));
                                stateModel.setStart_date(jsonState.getString("star_date"));
                                stateModel.setEnd_date(jsonState.getString("end_date"));
                                stateModel.setAdvance(jsonState.getString("advance"));
                                SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);
                            }
                        }
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ctx, "Error en recuperación de estados o el usuario no contiene estados", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String createBasicAuth(String username, String password) {
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }
}