package com.example.gpsgoldcar.Views.Activities.Drivers;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.gpsgoldcar.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.card.MaterialCardView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


public class DriverDetail extends AppCompatActivity {
    MaterialCardView driver_data,driver_lic,driver_ft,driver_sctr,driver_medpas,driver_ind,driver_state;
    TextView v_name,v_dni,v_fdni,v_lic,v_ind,v_ftck,v_sctr,v_med_pass,v_state;
    Date today = Calendar.getInstance().getTime();
    MaterialToolbar toolbar;
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd");
    String todayString = formatdate.format(today);
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drivers_table);
        toolbar = (MaterialToolbar) findViewById(R.id.toolbardet);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        /**Cardviews*/
        driver_data= (MaterialCardView) findViewById(R.id.driver_data);
        driver_lic= (MaterialCardView) findViewById(R.id.driver_lic);
        driver_ft= (MaterialCardView) findViewById(R.id.driver_ft);
        driver_sctr= (MaterialCardView) findViewById(R.id.driver_sct);
        driver_medpas= (MaterialCardView) findViewById(R.id.driv_med);
        driver_ind= (MaterialCardView) findViewById(R.id.driver_ind);
        driver_state= (MaterialCardView) findViewById(R.id.driver_st);

        /** Valores **/
        v_name = (TextView) findViewById(R.id.name_com);
        v_dni = (TextView) findViewById(R.id.dni);
        v_fdni = (TextView) findViewById(R.id.f_dni);
        v_lic = (TextView) findViewById(R.id.lic_con);
        v_ind = (TextView) findViewById(R.id.indu);
        v_ftck = (TextView) findViewById(R.id.ftck);
        v_sctr = (TextView) findViewById(R.id.sctr);
        v_med_pass= (TextView) findViewById(R.id.pas_med);
        v_state= (TextView) findViewById(R.id.est);

        /**Recibimos los valores*/
        String name = getIntent().getStringExtra("Name");
        String dni = getIntent().getStringExtra("Dni");
        String fdni = getIntent().getStringExtra("Fdni");
        String licon = getIntent().getStringExtra("Licon");
        String ind = getIntent().getStringExtra("Ind");
        String ftck = getIntent().getStringExtra("Ftchk");
        String sctr = getIntent().getStringExtra("Sctr");
        String medpass= getIntent().getStringExtra("Medpass");
        String state = getIntent().getStringExtra("State");

        /**Seteamos los valores*/
        v_name.setText(name);
        v_dni.setText(dni);
        v_fdni.setText(fdni);
        v_lic.setText(licon);
        v_ind.setText(ind);
        v_ftck.setText(ftck);
        v_sctr.setText(sctr);
        v_med_pass.setText(medpass);
        if(state.equals("true")){
            v_state.setText("Activo");
        }else {
            v_state.setText("Inactivo");
        }
        /**Semaforizacion*/
        try{
            Date f_dni = formatdate.parse(v_fdni.getText().toString());
            Date licenseDate = formatdate.parse(v_lic.getText().toString());
            Date induccion = formatdate.parse(v_ind.getText().toString());
            Date ftchk = formatdate.parse(v_ftck.getText().toString());
            Date d_sctr = formatdate.parse(v_sctr.getText().toString());
            Date med_pass = formatdate.parse(v_med_pass.getText().toString());
            Date todayDate = formatdate.parse(todayString);

            /**  INICIAMOS LA SEMAFORIZACIÓN **/

            /** Calculamos los dias para cada campo **/
            long daysDiffFdni = f_dni.getTime()-todayDate.getTime();
            long daysDiffLicense = licenseDate.getTime()-todayDate.getTime();
            long daysDiffInd = induccion.getTime()-todayDate.getTime();
            long daysDiffFtchk = ftchk.getTime()-todayDate.getTime();
            long daysDiffSctr = d_sctr.getTime()-todayDate.getTime();
            long daysDiffMedpass= med_pass.getTime()-todayDate.getTime();
            /** Casteamos los dias **/
            long daysDni = TimeUnit.DAYS.convert(daysDiffFdni,TimeUnit.MILLISECONDS);
            long daysLicense = TimeUnit.DAYS.convert(daysDiffLicense,TimeUnit.MILLISECONDS);
            long daysInd = TimeUnit.DAYS.convert(daysDiffInd,TimeUnit.MILLISECONDS);
            long daysFtchk = TimeUnit.DAYS.convert(daysDiffFtchk,TimeUnit.MILLISECONDS);
            long daysSctr = TimeUnit.DAYS.convert(daysDiffSctr,TimeUnit.MILLISECONDS);
            long daysPass = TimeUnit.DAYS.convert(daysDiffMedpass,TimeUnit.MILLISECONDS);

            /** Validamos **/
            //DNI
            if(daysDni>=30){
                driver_data.setCardBackgroundColor(Color.parseColor("#81C784"));
            }else if(15<daysDni){
                driver_data.setCardBackgroundColor(Color.parseColor("#ffca28"));
            }else {
                driver_data.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //LICENCIA
            if(daysLicense>=30){
                driver_lic.setCardBackgroundColor(Color.parseColor("#81C784"));
            }else if (15 <daysLicense){
                driver_lic.setCardBackgroundColor(Color.parseColor("#ffca28"));
            }
            else {
                driver_lic.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //INDUCCIÓN
            if(daysInd>=30){
                driver_ind.setCardBackgroundColor(Color.parseColor("#81C784"));
            }else if (15 < daysInd){
                driver_ind.setCardBackgroundColor(Color.parseColor("#ffca28"));
            }
            else {
                driver_ind.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //FOTOCHECK
            if(daysFtchk>=30){
                driver_ft.setCardBackgroundColor(Color.parseColor("#81C784"));
            }else if (15 < daysFtchk){
                driver_ft.setCardBackgroundColor(Color.parseColor("#ffca28"));
            }
            else {
                driver_ft.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //SCTR
            if(daysSctr>=30){
                driver_sctr.setCardBackgroundColor(Color.parseColor("#81C784"));
            }else if (15 < daysSctr){
                driver_sctr.setCardBackgroundColor(Color.parseColor("#ffca28"));
            }
            else {
                driver_sctr.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //PASE MEDICO
            if(daysPass>=30){
                driver_medpas.setCardBackgroundColor(Color.parseColor("#81C784"));
            }else if (15 < daysPass){
                driver_medpas.setCardBackgroundColor(Color.parseColor("#ffca28"));
            }
            else {
                driver_medpas.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            /** OBTENEMOS EL ESTADO DEL CONDUCTOR **/

            if(state.equals("true")){
                driver_state.setCardBackgroundColor(Color.parseColor("#81C784"));
            }else {
                driver_state.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
        }catch (ParseException e){
            e.printStackTrace();
        }


    }
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
}
