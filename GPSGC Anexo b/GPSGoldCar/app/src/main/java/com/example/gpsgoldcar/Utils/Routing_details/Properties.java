package com.example.gpsgoldcar.Utils.Routing_details;

public class Properties {
    private int pk;
    private String name;

    public Properties(int pk, String name) {
        this.pk = pk;
        this.name = name;
    }

    public Properties() {
    }
    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
