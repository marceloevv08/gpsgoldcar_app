package com.example.gpsgoldcar.Views.Activities.Schedules;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gpsgoldcar.Adapters.DriverUnitAdapter;
import com.example.gpsgoldcar.Models.DriUnitModel;
import com.example.gpsgoldcar.R;
import com.example.gpsgoldcar.Utils.Driver_Filtered.DriverFiltered;
import com.example.gpsgoldcar.Utils.InterfaceAPI;
import com.example.gpsgoldcar.Utils.Product;
import com.example.gpsgoldcar.Utils.RetrofitClientInstance;
import com.example.gpsgoldcar.Utils.Routing_details.Route;
import com.example.gpsgoldcar.Utils.ScheduleCreator.NewSchedule;
import com.example.gpsgoldcar.Utils.ScheduleCreator.RoutingDetails;
import com.example.gpsgoldcar.Utils.ScheduleCreator.Schedule;
import com.example.gpsgoldcar.Utils.Vehicles_Filtered.VehicleFiltered;
import com.example.gpsgoldcar.Views.Fragments.DatePickerFragment;
import com.example.gpsgoldcar.Views.Fragments.TimePickerFragment;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonIOException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class SchedulesCreator extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DriverUnitAdapter.OnItemClickListener {
    NestedScrollView createForm;
    TextInputLayout ti_date, ti_hour, ti_route, ti_product;
    EditText date, hour;
    ArrayList<String> routes, products, drivers, units;
    ArrayList<Integer> productId, driverId, unitId, routeid;
    ArrayList<String> geofence;
    List<DriUnitModel> cardsDU;
    RecyclerView recyclerView;
    DriverUnitAdapter adapter;
    AutoCompleteTextView route, product;
    Context ctx = this;
    MaterialToolbar toolbar;
    FloatingActionButton addProduct, addDUcard;
    MaterialButton create, cancel;
    int idruta, idproduct;
    ArrayAdapter<String> adapterProducts;
    Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
    final InterfaceAPI api = retrofit.create(InterfaceAPI.class);
    ArrayAdapter<String> adapterRoutes;
    //Fecha de hoy
    Date today = Calendar.getInstance().getTime();
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd");
    String todayString = formatdate.format(today);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_creator);
        //Barra de navegación
        toolbar = (MaterialToolbar) findViewById(R.id.toolbarschcre);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //Formulario completo
        createForm = (NestedScrollView) findViewById(R.id.creatorForm);
        //Lista reciclable del formulario conductor-unidad
        cardsDU = new ArrayList(Arrays.asList(new DriUnitModel(), new DriUnitModel()));
        //Fecha
        ti_date = (TextInputLayout) findViewById(R.id.date_filter);
        date = (EditText) findViewById(R.id.et_date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });
        //Hora
        ti_hour = (TextInputLayout) findViewById(R.id.hour_filter);
        hour = (EditText) findViewById(R.id.et_hour);
        hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerDialog();
            }
        });
        //Añadimos el producto nuevo desde el modal
        addProduct = (FloatingActionButton) findViewById(R.id.openDialog);
        addProduct.bringToFront();
        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProduct.setEnabled(false);
                createProduct();
            }
        });
        //Boton para añadir Cardviews
        addDUcard = (FloatingActionButton) findViewById(R.id.addCard);
        addDUcard.bringToFront();
        addDUcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItem(new DriUnitModel());
            }
        });
        //Declaro las listas para la data de los AutocompleteText
        routes = new ArrayList<String>();
        products = new ArrayList<String>();
        drivers = new ArrayList<String>();
        units = new ArrayList<String>();

        //Data a enviar
        geofence = new ArrayList<>();
        productId = new ArrayList<>();
        driverId = new ArrayList<>();
        unitId = new ArrayList<>();
        routeid = new ArrayList<>();

        //Recuperamos los datos
        GetRoutes getRoute = new GetRoutes();
        getRoute.execute();
        GetProducts getProduct = new GetProducts();
        getProduct.execute();
        GetDrivers getDrivers = new GetDrivers();
        getDrivers.execute();
        GetUnits getUnits = new GetUnits();
        getUnits.execute();

        //Llenamos las listas
        //Ruta
        ti_route = (TextInputLayout) findViewById(R.id.route_filter);
        route = (AutoCompleteTextView) findViewById(R.id.rutas);


        //Producto
        ti_product = (TextInputLayout) findViewById(R.id.product_filter);
        product = (AutoCompleteTextView) findViewById(R.id.productos);
        //Creamos los recycler view
        recyclerView = (RecyclerView) findViewById(R.id.recycler_dri_unit);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        adapter = new DriverUnitAdapter(ctx, drivers, units, driverId, unitId, cardsDU, this);
        recyclerView.setAdapter(adapter);

        // Sacamos la data necesaria por posicion del autocomplete textview
        route.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                idruta = (int) adapterView.getItemIdAtPosition(i);
            }
        });
        product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                idproduct = (int) adapterView.getItemIdAtPosition(i);
            }
        });

        //Validamos los campos
        validateFields();

        // Crear y cancelar
        create = (MaterialButton) findViewById(R.id.createSchedule);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //validamos que no esten vacíos para crearlos
                try {
                    if (validateEmptyFields()) {
                        if(validateDate()){
                            if(validateHour()){
                                createSchedule();
                            }else {
                                Toasty.error(ctx,"Usted está fuera del horario de trabajo, solo puede crear programaciones a partir del dia consiguiente.",Toast.LENGTH_SHORT).show();
                                if(postTwoDays()){
                                    if(validateHour()){
                                        createSchedule();
                                    }else {
                                        Toasty.error(ctx,"La hora no puede ser mayor al horario de trabajo",Toast.LENGTH_SHORT).show();
                                    }

                                }else {
                                    Toasty.error(ctx,"Usted está fuera del horario de trabajo, solo puede crear programaciones a partir del dia consiguiente.",Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        else {
                            Toasty.error(ctx,"No puede crear programaciones para fechas pasadas",Toast.LENGTH_SHORT,true).show();
                        }

                    } else {
                        createForm.scrollTo(0, 0);
                        Toasty.error(ctx, "No puede dejar campos vacíos", Toast.LENGTH_SHORT).show();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        });
        cancel = (MaterialButton) findViewById(R.id.cancelSchedule);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    private boolean validateDate() throws ParseException {
        boolean validDate;
        if(Objects.requireNonNull(formatdate.parse(date.getText().toString())).before(today)){
            validDate = false;
        }
        else {
            validDate = true;
        }
        return validDate;
    }

    private boolean validateHour() throws ParseException {
        boolean isValid;
        //Tengo que validar la hora
        //Current Time
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String currentTime =formatter.format(today);
        String [] currentTimedata = currentTime.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,Integer.parseInt(currentTimedata[0]));
        calendar.set(Calendar.MINUTE,Integer.parseInt(currentTimedata[1]));
        // Time selected
        String timeLimiter ="17:00";
        String [] timeSelected = timeLimiter.split(":");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR_OF_DAY,Integer.parseInt(timeSelected[0]));
        calendar1.set(Calendar.MINUTE,Integer.parseInt(timeSelected[1]));

        isValid = calendar.before(calendar1);
        return isValid;
    }
    private boolean postTwoDays() throws ParseException {
        boolean isValid;
        //Tengo que comparar las fechas
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = dateFormat.parse(date.getText().toString());
        Date todayDateaux = dateFormat.parse(todayString);
        Calendar c = Calendar.getInstance();
        c.setTime(todayDateaux);
        c.add(Calendar.DATE,2);
        String today=dateFormat.format(c.getTime());
        Date todayDate = dateFormat.parse(today);

        if(selectedDate.compareTo(todayDate)==0){
            isValid=true;
        }
        else {
            isValid=false;
        }
        return isValid;
    }


    //Metodo para mostrar el calendario
    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                int monthP = month + 1;
                String formattedMonth = "" + monthP;
                String formattedDayOfMonth = "" + day;

                if (monthP < 10) {

                    formattedMonth = "0" + monthP;
                }
                if (day < 10) {

                    formattedDayOfMonth = "0" + day;
                }

                date.setText(year + "-" + formattedMonth + "-" + formattedDayOfMonth);
            }
        });
        if (getSupportFragmentManager().findFragmentByTag("datePicker") == null) {
            newFragment.show(getSupportFragmentManager(), "datePicker");
        }
    }

    // Mostrar el reloj
    private void showTimePickerDialog() {
        DialogFragment timePicker = new TimePickerFragment();
        if (getSupportFragmentManager().findFragmentByTag("timePicker") == null) {
            timePicker.show(getSupportFragmentManager(), "timePicker");
        }
    }

    //Metodo para crear la autenticación
    public String createBasicAuth(String username, String password) {
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }

    //Seteamos la hora
    @SuppressLint("SetTextI18n")
    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        String formatedminute = "" + minute;
        if (minute < 10) {
            formatedminute = "0" + minute;
        }
        hour.setText(hourOfDay + ":" + formatedminute);
    }

    // Método para eliminar el formulario conductor-unidad
    @Override
    public void onDeleteClick(int position,DriUnitModel driUnitModel) {
        cardsDU.remove(driUnitModel);
        adapter.notifyItemRemoved(position);
    }

    // Método para añadir el formulario conductor-unidad
    public void addItem(DriUnitModel driUnitModel) {
        cardsDU.add(driUnitModel);
        adapter.notifyItemInserted(cardsDU.size() + 1);
    }

    public void validateFields() {
        //Ruta
        route.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (route.getText().toString().isEmpty()) {
                    ti_route.setError("Seleccione una Ruta");
                } else {
                    ti_route.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (route.getText().toString().isEmpty()) {
                    ti_route.setError("Seleccione una Ruta");
                } else {
                    ti_route.setErrorEnabled(false);
                }
            }
        });
        //Producto
        product.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (product.getText().toString().isEmpty()) {
                    ti_product.setError("Seleccione un Producto");
                } else {
                    ti_product.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (product.getText().toString().isEmpty()) {
                    ti_product.setError("Seleccione un Producto");
                } else {
                    ti_product.setErrorEnabled(false);
                }
            }
        });

    }

    //Validar campos para la creacion
    public boolean validateEmptyFields() {
        boolean isValid = true;

        //Hora
        if (hour.getText().toString().isEmpty()) {
            ti_hour.setError("Ingrese una Hora");
            isValid = false;
        } else {
            ti_hour.setErrorEnabled(false);
        }
        //Fecha

        if (date.getText().toString().isEmpty()) {
            ti_date.setError("Ingrese una Fecha");
            isValid = false;
        } else {
            ti_date.setErrorEnabled(false);
        }
        //Ruta
        if (route.getText().toString().isEmpty()) {
            ti_route.setError("Ingrese una Ruta");
            isValid = false;
        } else {
            ti_route.setErrorEnabled(false);
        }

        //Producto
        if (product.getText().toString().isEmpty()) {
            ti_product.setError("Ingrese un Producto");
            isValid = false;
        } else {
            ti_product.setErrorEnabled(false);
        }

        // Formulario Unit-Driver
        if (cardsDU.size() == 0) {
            Toasty.error(ctx, "No hay conductores y unidades en su formulario", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (hour.getText().toString().isEmpty() || date.getText().toString().isEmpty() || route.getText().toString().isEmpty() || product.getText().toString().isEmpty() || cardsDU.size() == 0) {
            isValid = false;
        }

        return isValid;
    }

    //RUTAS

    @SuppressLint("StaticFieldLeak")
    class GetRoutes extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ctx, "", "Cargando Datos", true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH = createBasicAuth(username, password);
                Call<List<Route>> getRoute = api.getRoutes(AUTH);
                List<Route> getRoutes = getRoute.execute().body();
                if (!getRoutes.equals(null)) {
                    if (getRoutes.size() > 0) {
                        for (Route route : getRoutes) {
                            routes.add(route.getOrigin_place().getProperties().getName() + " - " + route.getTarget_place().getProperties().getName());
                            geofence.add(route.getGeofence());
                            routeid.add(route.getPk());
                        }
                    }
                } else {
                    Toasty.error(ctx, "Error en la recuperación de Rutas", Toast.LENGTH_SHORT).show();
                }

                Log.i("Rutas", routes.toString());

            } catch (JsonIOException | IOException | NullPointerException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            adapterRoutes = new ArrayAdapter<String>(ctx,
                    android.R.layout.simple_list_item_1, routes);
            route.setAdapter(adapterRoutes);


        }
    }


    //Obtenemos los productos
    class GetProducts extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ctx, "", "Cargando Datos", true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH1 = createBasicAuth(username, password);
                Call<List<Product>> getProducts = api.getProducts(AUTH1);
                List<Product> prod = getProducts.execute().body();
                if (!prod.equals(null)) {
                    if (prod.size() > 0) {
                        for (Product product : prod) {
                            productId.add(product.getPk());
                            products.add(product.getName());
                        }
                    } else {
                        String data = "No hay datos disponibles";
                        products.add(data);
                    }
                } else {
                    Toasty.error(ctx, "Error en la recuperación de Productos", Toast.LENGTH_SHORT).show();
                }


                Log.i("Productos", products.toString());

            } catch (JsonIOException | IOException | NullPointerException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            adapterProducts = new ArrayAdapter<String>(ctx,
                    android.R.layout.simple_list_item_1, products);
            product.setAdapter(adapterProducts);


        }
    }


    //Obtenemos los conductores
    class GetDrivers extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ctx, "", "Cargando Datos", true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH2 = createBasicAuth(username, password);
                Call<List<DriverFiltered>> getDriversFiltered = api.getDrivers(AUTH2);
                List<DriverFiltered> getDF = getDriversFiltered.execute().body();
                if (!getDF.equals(null)) {
                    if (getDF.size() > 0) {
                        for (DriverFiltered driverFiltered : getDF) {
                            drivers.add(driverFiltered.getFields().getName());
                            driverId.add(driverFiltered.getPk());
                        }
                    } else {
                        String data = "No hay datos disponibles";
                        drivers.add(data);
                    }
                } else {
                    Toasty.error(ctx, "Error en la recuperación de Conductores", Toast.LENGTH_SHORT).show();

                }

            } catch (JsonIOException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            Log.i("DRIVERS", drivers.toString());
        }
    }

    //Obtenemos las unidades
    class GetUnits extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ctx, "", "Cargando Datos", true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH3 = createBasicAuth(username, password);
                Call<List<VehicleFiltered>> getVF = api.getVehicles(AUTH3);
                List<VehicleFiltered> vehicleFiltereds = getVF.execute().body();
                if (!vehicleFiltereds.equals(null)) {
                    if (vehicleFiltereds.size() > 0) {
                        for (VehicleFiltered vehicleFiltered : vehicleFiltereds) {
                            units.add(vehicleFiltered.getFields().getAvl_unit_name());
                            unitId.add(vehicleFiltered.getPk());
                        }
                    } else {
                        String data = "No hay datos disponibles";
                        units.add(data);
                    }

                } else {
                    Toasty.error(ctx, "Error en la recuperación de Vehículos", Toast.LENGTH_SHORT).show();

                }

            } catch (JsonIOException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            Log.i("VEHICLES", units.toString());
        }
    }

    //Metodo para crear el producto

    public void createProduct() {
        View view = getLayoutInflater().inflate(R.layout.product_modal, null);
        EditText name = view.findViewById(R.id.tv_product_name);
        EditText description = view.findViewById(R.id.tv_product_description);
        //Creamos el modal
        MaterialAlertDialogBuilder productModal = new MaterialAlertDialogBuilder(ctx, R.style.ProductDialog)
                .setView(view)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addProduct.setEnabled(true);
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                        String username = sharedPref.getString("username", "");
                        String password = sharedPref.getString("password", "");
                        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
                        String AUTH = createBasicAuth(username, password);
                        String nameProduct = name.getText().toString();
                        String nameDescription = description.getText().toString();
                        if (nameProduct.isEmpty() || nameDescription.isEmpty()) {
                            Toasty.error(getApplicationContext(), "No puede dejar campos vacíos", Toast.LENGTH_SHORT, true).show();
                        } else {
                            Product product = new Product(nameProduct, nameDescription);
                            final InterfaceAPI api = retrofit.create(InterfaceAPI.class);
                            Call<Product> call = api.createProduct(AUTH, product);
                            call.enqueue(new Callback<Product>() {
                                @Override
                                public void onResponse(Call<Product> call, Response<Product> response) {
                                    if (response.isSuccessful()) {
                                        Toasty.success(getApplicationContext(), "Producto Creado", Toast.LENGTH_SHORT, true).show();
                                    } else {
                                        Toasty.error(getApplicationContext(), "Error al crear Producto", Toast.LENGTH_SHORT, true).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Product> call, Throwable t) {
                                    t.printStackTrace();
                                    Toasty.error(getApplicationContext(), "Error al crear Producto", Toast.LENGTH_SHORT, true).show();
                                }
                            });
                        }
                        addProduct.setEnabled(true);

                    }
                });
        productModal.setCancelable(false);
        productModal.show();
    }


    //Método para crear el schedule
    public void createSchedule() {
        SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
        String username = sharedPref.getString("username", "");
        String password = sharedPref.getString("password", "");
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
        String AUTH = createBasicAuth(username, password);
        final InterfaceAPI api = retrofit.create(InterfaceAPI.class);
        //Datos de la ruta
        String geo = geofence.get(idruta);
        int route_id = routeid.get(idruta);
        //Datos del producto
        int product_id = productId.get(idproduct);
        //Hora - fecha - estado y flag
        String routing_time = hour.getText().toString() + ":00";
        String routing_date = date.getText().toString();
        String state = "PROGRAMED";
        boolean is_active = true;
        boolean isRDvalid = true;

        //Para conductores y unidades
        List<RoutingDetails> routing_detailsList = new ArrayList<RoutingDetails>();
        int idDriver,idUnit;
        String nameDriver, nameUnit;

        for (int i = 0; i < cardsDU.size(); i++) {
            //Driver
            if (cardsDU.get(i).getDriver() != null ) {
                if (cardsDU.get(i).getUnit() != null) {
                    idDriver = cardsDU.get(i).getDriverpk();
                    nameDriver = cardsDU.get(i).getDriver();
                    //Unit
                    idUnit = cardsDU.get(i).getUnitpk();
                    nameUnit = cardsDU.get(i).getUnit();
                    RoutingDetails routingDetailsobj = new RoutingDetails(nameDriver, nameUnit, idUnit, idDriver);
                    routing_detailsList.add(routingDetailsobj);
                    isRDvalid=true;

                } else {
                    Toasty.error(ctx, "Data de Vehículos inválida", Toast.LENGTH_SHORT, true).show();
                    isRDvalid=false;
                }

            } else {
                Toasty.error(ctx, "Data de Conductores inválida", Toast.LENGTH_SHORT, true).show();
                isRDvalid= false;
            }


        }


        Log.i("ROUTING DETAILS", routing_detailsList.toString());
        boolean validateEmptyData = validateEmptyFieldsRouting(routing_detailsList);
        boolean validateDuplicatedData = validateDuplicatedData(routing_detailsList);
        Log.i("Validar vacio", String.valueOf(validateEmptyData));
        if (routing_detailsList.size() > 0) {
            if(isRDvalid){

                if (validateDuplicatedData && validateEmptyData) {
                    Schedule schedule = new Schedule(product_id, geo);
                    NewSchedule newSchedule = new NewSchedule(route_id, schedule, routing_date, routing_time, is_active, routing_detailsList, state);
                    Call<NewSchedule> call = api.createSchedule(AUTH, newSchedule);
                    call.enqueue(new Callback<NewSchedule>() {
                        @Override
                        public void onResponse(Call<NewSchedule> call, Response<NewSchedule> response) {
                            if (response.isSuccessful()) {
                                Toasty.success(ctx, "Programación Creada", Toast.LENGTH_SHORT, true).show();
                                onBackPressed();
                                cleanFields();
                            } else {
                                Toasty.error(ctx, "Error al crear la programación", Toast.LENGTH_SHORT, true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<NewSchedule> call, Throwable t) {
                            t.printStackTrace();
                            Toasty.error(ctx, "Error al crear la programación", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else {
                    Toasty.error(ctx, "Data de Conductores o Vehículos repetida o vacía", Toast.LENGTH_SHORT, true).show();
                }
            }

        } else {
            Toasty.error(ctx, "No contiene conductores ni vehículos", Toast.LENGTH_SHORT, true).show();
        }


    }

    public void cleanFields() {
        date.setText("");
        ti_date.setErrorEnabled(false);
        hour.setText("");
        ti_hour.setErrorEnabled(false);
        route.setText("");
        ti_route.setErrorEnabled(false);
        product.setText("");
        ti_product.setErrorEnabled(false);

    }

    public boolean validateEmptyFieldsRouting(List<RoutingDetails> routing_detailsList) {
        boolean isValid = true;
        for (int i = 0; i < routing_detailsList.size(); i++) {
            if (routing_detailsList.get(i).getDriver().equals("")) {
                Toasty.error(ctx, "Datos de Conductores Vacíos", Toast.LENGTH_SHORT, true).show();
                isValid = false;
            } else if (routing_detailsList.get(i).getAvl_unit().equals("")) {
                Toasty.error(ctx, "Datos de Vechículos Vacíos", Toast.LENGTH_SHORT, true).show();
                isValid = false;
            } else {
                isValid = true;
            }

        }
        return isValid;
    }

    public boolean validateDuplicatedData(List<RoutingDetails> routing_detailsList) {
        boolean isValid = true;
        for (int i = 0; i < routing_detailsList.size(); i++) {
            for (int j = i + 1; j < routing_detailsList.size(); j++) {
                if (routing_detailsList.get(i).getDriver_id() == routing_detailsList.get(j).getDriver_id()) {
                    Toasty.error(ctx, "Datos de Conductores Repetidos", Toast.LENGTH_SHORT, true).show();
                    isValid = false;
                } else if (routing_detailsList.get(i).getAvl_unit_id() == routing_detailsList.get(j).getAvl_unit_id()) {
                    Toasty.error(ctx, "Datos de Vechículos Repetidos", Toast.LENGTH_SHORT, true).show();
                    isValid = false;
                } else {
                    isValid = true;
                }
            }
        }
        return isValid;
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
