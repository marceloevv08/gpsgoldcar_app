package com.example.gpsgoldcar.Utils.SchedulesModel;

public class Filter {
    private String all_programming;
    private String date_end;
    private String date_ini;
    private String pk_company;
    private String pk_route;
    private String pk_state;

    public Filter(String all_programming, String date_end, String date_ini, String pk_company, String pk_route, String pk_state) {
        this.all_programming = all_programming;
        this.date_end = date_end;
        this.date_ini = date_ini;
        this.pk_company = pk_company;
        this.pk_route = pk_route;
        this.pk_state = pk_state;
    }

    public Filter() {
    }

    public String isAll_programming() {
        return all_programming;
    }

    public void setAll_programming(String all_programming) {
        this.all_programming = all_programming;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getDate_ini() {
        return date_ini;
    }

    public void setDate_ini(String date_ini) {
        this.date_ini = date_ini;
    }

    public String getPk_company() {
        return pk_company;
    }

    public void setPk_company(String pk_company) {
        this.pk_company = pk_company;
    }

    public String getPk_route() {
        return pk_route;
    }

    public void setPk_route(String pk_route) {
        this.pk_route = pk_route;
    }

    public String getPk_state() {
        return pk_state;
    }

    public void setPk_state(String pk_state) {
        this.pk_state = pk_state;
    }
}
