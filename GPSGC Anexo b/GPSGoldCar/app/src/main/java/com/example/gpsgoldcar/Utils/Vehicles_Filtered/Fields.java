package com.example.gpsgoldcar.Utils.Vehicles_Filtered;

public class Fields {
    private int gps_device;
    private String avl_unit_name;
    private String avl_unit_image;

    public Fields() {
    }

    public Fields(int gps_device, String avl_unit_name, String avl_unit_image) {
        this.gps_device = gps_device;
        this.avl_unit_name = avl_unit_name;
        this.avl_unit_image = avl_unit_image;
    }

    public int getGps_device() {
        return gps_device;
    }

    public void setGps_device(int gps_device) {
        this.gps_device = gps_device;
    }

    public String getAvl_unit_name() {
        return avl_unit_name;
    }

    public void setAvl_unit_name(String avl_unit_name) {
        this.avl_unit_name = avl_unit_name;
    }

    public String getAvl_unit_image() {
        return avl_unit_image;
    }

    public void setAvl_unit_image(String avl_unit_image) {
        this.avl_unit_image = avl_unit_image;
    }
}
