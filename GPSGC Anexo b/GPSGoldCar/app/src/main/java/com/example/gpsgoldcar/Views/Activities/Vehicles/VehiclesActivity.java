package com.example.gpsgoldcar.Views.Activities.Vehicles;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gpsgoldcar.Adapters.VehicleAdapter;
import com.example.gpsgoldcar.Database.SqliteClass;
import com.example.gpsgoldcar.Models.VehicleModel;
import com.example.gpsgoldcar.R;
import com.google.android.material.appbar.MaterialToolbar;
import java.util.ArrayList;
import java.util.Objects;

public class VehiclesActivity extends AppCompatActivity implements VehicleAdapter.OnItemClickListener {
    Context ctx =this;
    MaterialToolbar toolbar;
    RecyclerView recyclerView;
    ArrayList<VehicleModel> vehicles;
    VehicleAdapter adapter;
    EditText placav;
    Intent i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicles);

        toolbar = (MaterialToolbar) findViewById(R.id.toolbarv);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_vehicle);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        vehicles = SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.getAllVehicles();
        adapter = new VehicleAdapter(ctx,vehicles,this);
        recyclerView.setAdapter(adapter);
        placav = (EditText) findViewById(R.id.placav);
        placav.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

    }
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
    private void filter(String s) {
        ArrayList<VehicleModel> driversList = new ArrayList<>();
        for(VehicleModel vehicle: vehicles){
            if(vehicle.getPlaca().toLowerCase().contains(s.toLowerCase())){
                driversList.add(vehicle);
            }
        }
        adapter.filteredList(driversList);
    }

    @Override
    public void onItemClick(VehicleModel vehicleModel) {
        String placa = vehicleModel.getPlaca();
        String soat = vehicleModel.getSoat();
        String poliza = vehicleModel.getPoliza();
        String rev_tec = vehicleModel.getRev_tec();
        String vig_cont = vehicleModel.getRev_tec();
        String gps = vehicleModel.getGps();
        String estado = vehicleModel.getEstado();
        Intent i = new Intent(this, VehicleDetail.class);
        i.putExtra("Placa",placa);
        i.putExtra("Soat",soat);
        i.putExtra("Poliza",poliza);
        i.putExtra("Rev_tec",rev_tec);
        i.putExtra("Vig_cont",vig_cont);
        i.putExtra("Gps",gps);
        i.putExtra("Estado",estado);
        startActivity(i);

    }
}
