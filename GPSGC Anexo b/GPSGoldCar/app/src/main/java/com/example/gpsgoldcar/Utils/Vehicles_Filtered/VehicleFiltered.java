package com.example.gpsgoldcar.Utils.Vehicles_Filtered;

public class VehicleFiltered {
    private String model;
    private int pk;
    private Fields fields;

    public VehicleFiltered() {
    }

    public VehicleFiltered(String model, int pk, Fields fields) {
        this.model = model;
        this.pk = pk;
        this.fields = fields;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }
}
