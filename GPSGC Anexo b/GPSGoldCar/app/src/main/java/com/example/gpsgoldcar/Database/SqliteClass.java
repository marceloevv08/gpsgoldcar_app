package com.example.gpsgoldcar.Database;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.gpsgoldcar.Models.DriverModel;
import com.example.gpsgoldcar.Models.ScheduleModel;
import com.example.gpsgoldcar.Models.StateModel;
import com.example.gpsgoldcar.Models.VehicleModel;

import java.util.ArrayList;

public class SqliteClass {
    /** TABLE_APP_VEHICLES **/
    public static final String TABLE_VEHICLES = "vehicles";
    public static final String KEY_VEID= "ve_id";
    public static final String KEY_VEIDVE= "ve_id_vehicle";
    public static final String KEY_VEIDCOM= "ve_id_company";
    public static final String KEY_VEPLA= "ve_placa";
    public static final String KEY_VESOAT= "ve_soat";
    public static final String KEY_VEPOL= "ve_poliza";
    public static final String KEY_VEREV= "ve_rev_tec";
    public static final String KEY_VEIVIG= "ve_vig_cont";
    public static final String KEY_VEGPS= "ve_gps";
    public static final String KEY_VEEST= "ve_estado";

    /** TABLE_APP_DRIVERS **/
    public static final String TABLE_DRIVERS = "drivers";
    public static final String KEY_DVID= "dv_id";
    public static final String KEY_DVIDDV= "dv_id_driver";
    public static final String KEY_DVNOM= "dv_nombre";
    public static final String KEY_DVDNI= "dv_dni";
    public static final String KEY_DVFDNI= "dv_f_dni";
    public static final String KEY_DVLC= "dv_lic_con";
    public static final String KEY_DVIND= "dv_induccion";
    public static final String KEY_DVFCK= "dv_fotocheck";
    public static final String KEY_DVSCTR= "dv_sctr";
    public static final String KEY_DVPMED= "dv_pas_med";
    public static final String KEY_DVIDCOMP = "dv_id_comp";
    public static final String KEY_DVEST= "dv_estado";

    /** TABLE_APP_SCHEDULES **/
    public static final String TABLE_SCHEDULES = "schedules";
    public static final String KEY_SID= "s_id";
    public static final String KEY_SIDSCH= "s_id_schedule";
    public static final String KEY_SORI= "s_origin";
    public static final String KEY_SDES= "s_destino";
    public static final String KEY_SDATE= "s_fecha";
    public static final String KEY_SHOUR= "s_hora";
    public static final String KEY_IDCOMP= "s_id_compania";
    public static final String KEY_SCOMP= "s_compania";
    public static final String KEY_SDRI= "s_conductor";
    public static final String KEY_SUNI= "s_unidad";
    public static final String KEY_SPRO= "s_producto";
    public static final String KEY_SEST= "dv_estado";

    /** TABLE_APP_STATES_PROGRAMINGS**/
    public static final  String TABLE_STATES = "programing_states";
    public static final String KEY_IDST = "pst_id";
    public static final String KEY_IDSTET = "pst_id_enterprise";
    public static final String KEY_ENTST = "pst_enterprise";
    public static final String KEY_UST = "pst_unit";
    public static final String KEY_ORIST = "pst_origin";
    public static final String KEY_DESST = "pst_destination";
    public static final String KEY_STST = "pst_state";
    public static final String KEY_SDTST = "pst_start_date";
    public static final String KEY_EDTST = "pst_end_date";
    public static final String KEY_ADST = "pst_advance";


    public DBHelper databasehelp;
    private static SqliteClass SqliteInstance = null;

    private SqliteClass(Context context) {
        databasehelp = new DBHelper(context);
    }

    public static SqliteClass getInstance(Context context) {
        if (SqliteInstance == null) {
            SqliteInstance = new SqliteClass(context);
        }
        return SqliteInstance;
    }
    public class DBHelper extends SQLiteOpenHelper{
        public Context context;
        private static final int DATABASE_VERSION = 1;
        private static final String DATABASE_NAME = "GPSGC_forviews.db";
        public AppVehicleSql appVehicleSql;
        public AppDriverSql appDriverSql;
        public AppScheduleSql appScheduleSql;
        public AppStateSql appStateSql;

        public DBHelper (Context context){
            super(context,DATABASE_NAME,null,DATABASE_VERSION);
            this.context = context;
            appVehicleSql = new AppVehicleSql();
            appDriverSql = new AppDriverSql();
            appScheduleSql = new AppScheduleSql();
            appStateSql = new AppStateSql();

        }
        @Override
        public void onCreate(SQLiteDatabase DB) {
            /** TABLE_APP_VEHICLES **/
            String CREATE_TABLE_VEHICLES= "CREATE TABLE "+ TABLE_VEHICLES+ "("
                    + KEY_VEID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_VEIDVE + " TEXT,"
                    + KEY_VEIDCOM + " TEXT," + KEY_VEPLA + " TEXT," + KEY_VESOAT + " TEXT,"
                    + KEY_VEPOL + " TEXT," + KEY_VEREV + " TEXT," + KEY_VEIVIG + " TEXT,"
                    + KEY_VEGPS + " TEXT," + KEY_VEEST + " TEXT )";

            /** TABLE_APP_DRIVERS **/
            String CREATE_TABLE_DRIVERS= "CREATE TABLE "+ TABLE_DRIVERS+ "("
                    + KEY_DVID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_DVIDDV + " TEXT,"
                    + KEY_DVNOM + " TEXT," + KEY_DVDNI + " TEXT," + KEY_DVFDNI + " TEXT,"
                    + KEY_DVLC + " TEXT," + KEY_DVIND + " TEXT," + KEY_DVFCK + " TEXT,"
                    + KEY_DVSCTR + " TEXT," + KEY_DVPMED + " TEXT," + KEY_DVIDCOMP + " TEXT,"+
                    KEY_DVEST +" TEXT )";

            /** TABLE_APP_SCHEDULES **/
            String CREATE_TABLE_SCHEDULES= "CREATE TABLE "+ TABLE_SCHEDULES+ "("
                    + KEY_SID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_SIDSCH + " TEXT,"
                    + KEY_SORI + " TEXT," + KEY_SDES + " TEXT," + KEY_SDATE + " TEXT,"
                    + KEY_SHOUR + " TEXT," +  KEY_IDCOMP + " TEXT," +KEY_SCOMP + " TEXT," + KEY_SDRI + " TEXT,"
                    + KEY_SUNI + " TEXT," + KEY_SPRO + " TEXT," +KEY_SEST +" TEXT )";

            /** TABLE_APP_STATES **/
            String CREATE_TABLE_STATES= "CREATE TABLE "+ TABLE_STATES+ "("
                    + KEY_IDST + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_IDSTET + " TEXT,"
                    + KEY_ENTST + " TEXT," + KEY_UST + " TEXT," + KEY_ORIST + " TEXT,"
                    + KEY_DESST + " TEXT," + KEY_STST + " TEXT," + KEY_SDTST + " TEXT,"
                    + KEY_EDTST + " TEXT," + KEY_ADST +" TEXT )";


            /** EXECSQL_CREATE **/
            DB.execSQL(CREATE_TABLE_VEHICLES);
            DB.execSQL(CREATE_TABLE_DRIVERS);
            DB.execSQL(CREATE_TABLE_SCHEDULES);
            DB.execSQL(CREATE_TABLE_STATES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase DB, int oldVersion, int newVersion) {
            /** EXECSQL_DROP **/
            DB.execSQL("DROP TABLE IF EXISTS " + TABLE_VEHICLES);
            DB.execSQL("DROP TABLE IF EXISTS " + TABLE_DRIVERS);
            DB.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULES);
            DB.execSQL("DROP TABLE IF EXISTS " + TABLE_STATES);
            onCreate(DB);
        }
        public class AppVehicleSql{
            public AppVehicleSql(){}

            public void deleteAppVehicleTable(){
                SQLiteDatabase DB = databasehelp.getWritableDatabase();
                DB.delete(TABLE_VEHICLES,null,null);
                DB.execSQL( "DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_VEHICLES + "'");
                DB.close();
            }
            public void addVehicle(VehicleModel vmodel){
                SQLiteDatabase DB = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_VEIDVE,vmodel.getId_vehicle());
                values.put(KEY_VEIDCOM,vmodel.getId_company());
                values.put(KEY_VEPLA,vmodel.getPlaca());
                values.put(KEY_VESOAT,vmodel.getSoat());
                values.put(KEY_VEPOL,vmodel.getPoliza());
                values.put(KEY_VEREV,vmodel.getRev_tec());
                values.put(KEY_VEIVIG,vmodel.getVig_contrato());
                values.put(KEY_VEGPS,vmodel.getGps());
                values.put(KEY_VEEST,vmodel.getEstado());
                DB.insert(TABLE_VEHICLES,null,values);
            }
            public ArrayList<VehicleModel> getAllVehicles() {
                ArrayList<VehicleModel> models = new ArrayList<VehicleModel>();
                String selectQuery = "SELECT * FROM " + TABLE_VEHICLES;
                SQLiteDatabase DB = databasehelp.getWritableDatabase();
                Cursor cursor = DB.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do{
                        VehicleModel model = new VehicleModel();
                        model.setId_vehicle(cursor.getInt(cursor.getColumnIndex(KEY_VEID)));
                        model.setId_company(cursor.getInt(cursor.getColumnIndex(KEY_VEIDCOM)));
                        model.setPlaca(cursor.getString(cursor.getColumnIndex(KEY_VEPLA)));
                        model.setSoat(cursor.getString(cursor.getColumnIndex(KEY_VESOAT)));
                        model.setPoliza(cursor.getString(cursor.getColumnIndex(KEY_VEPOL)));
                        model.setRev_tec(cursor.getString(cursor.getColumnIndex(KEY_VEREV)));
                        model.setVig_contrato(cursor.getString(cursor.getColumnIndex(KEY_VEIVIG)));
                        model.setGps(cursor.getString(cursor.getColumnIndex(KEY_VEGPS)));
                        model.setEstado(cursor.getString(cursor.getColumnIndex(KEY_VEEST)));
                        models.add(model);
                    } while (cursor.moveToNext());

                }
                cursor.close();
                DB.close();
                return models;
            }

            public ArrayList<VehicleModel> getVehiclesByCompany(int id) {
                ArrayList<VehicleModel> models = new ArrayList<VehicleModel>();
                String selectQuery = "SELECT * FROM " + TABLE_VEHICLES + " WHERE " + KEY_VEIDCOM + "='" + id + "'" ;
                SQLiteDatabase DB = databasehelp.getWritableDatabase();
                Cursor cursor = DB.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do{
                        VehicleModel model = new VehicleModel();
                        model.setId_vehicle(cursor.getInt(cursor.getColumnIndex(KEY_VEID)));
                        model.setId_company(cursor.getInt(cursor.getColumnIndex(KEY_VEIDCOM)));
                        model.setPlaca(cursor.getString(cursor.getColumnIndex(KEY_VEPLA)));
                        model.setSoat(cursor.getString(cursor.getColumnIndex(KEY_VESOAT)));
                        model.setPoliza(cursor.getString(cursor.getColumnIndex(KEY_VEPOL)));
                        model.setRev_tec(cursor.getString(cursor.getColumnIndex(KEY_VEREV)));
                        model.setVig_contrato(cursor.getString(cursor.getColumnIndex(KEY_VEIVIG)));
                        model.setGps(cursor.getString(cursor.getColumnIndex(KEY_VEGPS)));
                        model.setEstado(cursor.getString(cursor.getColumnIndex(KEY_VEEST)));
                        models.add(model);
                    } while (cursor.moveToNext());
                }
                cursor.close();
                return models;
            }
        }

        public class AppDriverSql{
            public AppDriverSql(){}
            public void deleteAppDriverTable(){
                SQLiteDatabase DB = databasehelp.getWritableDatabase();
                DB.delete(TABLE_DRIVERS,null,null);
                DB.execSQL( "DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_DRIVERS + "'");
                DB.close();
            }
            public void addDriver(DriverModel dmodel){
                SQLiteDatabase DB = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_DVIDDV,dmodel.getId_driv());
                values.put(KEY_DVNOM,dmodel.getNombre());
                values.put(KEY_DVDNI,dmodel.getDni());
                values.put(KEY_DVFDNI,dmodel.getF_dni());
                values.put(KEY_DVLC,dmodel.getLic_con());
                values.put(KEY_DVIND,dmodel.getInduccion());
                values.put(KEY_DVFCK,dmodel.getFtcheck());
                values.put(KEY_DVSCTR,dmodel.getSctr());
                values.put(KEY_DVPMED,dmodel.getPas_med());
                values.put(KEY_DVEST,dmodel.getEstado());
                DB.insert(TABLE_DRIVERS,null,values);
            }
            public ArrayList<DriverModel> getAllDrivers() {
                ArrayList<DriverModel> models = new ArrayList<DriverModel>();
                String selectQuery = "SELECT * FROM " + TABLE_DRIVERS;
                SQLiteDatabase DB = databasehelp.getWritableDatabase();
                Cursor cursor = DB.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do{
                        DriverModel model = new DriverModel();
                        model.setId_driv(cursor.getInt(cursor.getColumnIndex(KEY_DVID)));
                        model.setNombre(cursor.getString(cursor.getColumnIndex(KEY_DVNOM)));
                        model.setDni(cursor.getString(cursor.getColumnIndex(KEY_DVDNI)));
                        model.setF_dni(cursor.getString(cursor.getColumnIndex(KEY_DVFDNI)));
                        model.setLic_con(cursor.getString(cursor.getColumnIndex(KEY_DVLC)));
                        model.setInduccion(cursor.getString(cursor.getColumnIndex(KEY_DVIND)));
                        model.setFtcheck(cursor.getString(cursor.getColumnIndex(KEY_DVFCK)));
                        model.setSctr(cursor.getString(cursor.getColumnIndex(KEY_DVSCTR)));
                        model.setPas_med(cursor.getString(cursor.getColumnIndex(KEY_DVPMED)));
                        model.setId_compania(cursor.getInt(cursor.getColumnIndex(KEY_DVIDCOMP)));
                        model.setEstado(cursor.getString(cursor.getColumnIndex(KEY_DVEST)));
                        models.add(model);
                    } while (cursor.moveToNext());

                }
                cursor.close();
                DB.close();
                return models;
            }
            public ArrayList<DriverModel> getDriverByNombre(String nombre) {
                ArrayList<DriverModel> models = new ArrayList<DriverModel>();
                String selectQuery = "SELECT * FROM " + TABLE_DRIVERS + " WHERE " + KEY_DVNOM + "='" + nombre + "'" ;
                SQLiteDatabase DB = databasehelp.getWritableDatabase();
                Cursor cursor = DB.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do{
                        DriverModel model = new DriverModel();
                        model.setId_driv(cursor.getInt(cursor.getColumnIndex(KEY_DVIDDV)));
                        model.setNombre(cursor.getString(cursor.getColumnIndex(KEY_DVNOM)));
                        model.setDni(cursor.getString(cursor.getColumnIndex(KEY_DVDNI)));
                        model.setF_dni(cursor.getString(cursor.getColumnIndex(KEY_DVFDNI)));
                        model.setLic_con(cursor.getString(cursor.getColumnIndex(KEY_DVLC)));
                        model.setInduccion(cursor.getString(cursor.getColumnIndex(KEY_DVIND)));
                        model.setFtcheck(cursor.getString(cursor.getColumnIndex(KEY_DVFCK)));
                        model.setSctr(cursor.getString(cursor.getColumnIndex(KEY_DVSCTR)));
                        model.setPas_med(cursor.getString(cursor.getColumnIndex(KEY_DVPMED)));
                        model.setEstado(cursor.getString(cursor.getColumnIndex(KEY_DVEST)));
                        models.add(model);
                    } while (cursor.moveToNext());
                }
                cursor.close();
                return models;
            }

        }
    }
    public class AppScheduleSql{
        public AppScheduleSql(){}

        public void deleteAppScheduleTable(){
            SQLiteDatabase DB = databasehelp.getWritableDatabase();
            DB.delete(TABLE_SCHEDULES,null,null);
            DB.execSQL( "DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_SCHEDULES + "'");
            DB.close();
        }
        public void addSchedule(ScheduleModel schedulemodel){
            SQLiteDatabase DB = databasehelp.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_SIDSCH,schedulemodel.getId_schedules());
            values.put(KEY_SORI,schedulemodel.getOrigen());
            values.put(KEY_SDES,schedulemodel.getDestino());
            values.put(KEY_SDATE,schedulemodel.getFecha());
            values.put(KEY_SHOUR,schedulemodel.getHora());
            values.put(KEY_IDCOMP,schedulemodel.getId_compania());
            values.put(KEY_SCOMP,schedulemodel.getCompania());
            values.put(KEY_SDRI,schedulemodel.getConductor());
            values.put(KEY_SUNI,schedulemodel.getUnidad());
            values.put(KEY_SPRO,schedulemodel.getProducto());
            values.put(KEY_SEST,schedulemodel.getEstado());
            DB.insert(TABLE_SCHEDULES,null,values);
        }
        public ArrayList<ScheduleModel> getAllSchedules() {
            ArrayList<ScheduleModel> models = new ArrayList<ScheduleModel>();
            String selectQuery = "SELECT * FROM " + TABLE_SCHEDULES;
            SQLiteDatabase DB = databasehelp.getWritableDatabase();
            Cursor cursor = DB.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do{
                    ScheduleModel model = new ScheduleModel();
                    model.setId_schedules(cursor.getInt(cursor.getColumnIndex(KEY_SIDSCH)));
                    model.setOrigen(cursor.getString(cursor.getColumnIndex(KEY_SORI)));
                    model.setDestino(cursor.getString(cursor.getColumnIndex(KEY_SDES)));
                    model.setFecha(cursor.getString(cursor.getColumnIndex(KEY_SDATE)));
                    model.setHora(cursor.getString(cursor.getColumnIndex(KEY_SHOUR)));
                    model.setId_compania(cursor.getInt(cursor.getColumnIndex(KEY_IDCOMP)));
                    model.setCompania(cursor.getString(cursor.getColumnIndex(KEY_SCOMP)));
                    model.setConductor(cursor.getString(cursor.getColumnIndex(KEY_SDRI)));
                    model.setUnidad(cursor.getString(cursor.getColumnIndex(KEY_SUNI)));
                    model.setProducto(cursor.getString(cursor.getColumnIndex(KEY_SPRO)));
                    model.setEstado(cursor.getString(cursor.getColumnIndex(KEY_SEST)));
                    models.add(model);
                } while (cursor.moveToNext());

            }
            cursor.close();
            DB.close();
            return models;
        }
        public ArrayList<ScheduleModel> getSchedulesByCompany(int id) {
            ArrayList<ScheduleModel> models = new ArrayList<ScheduleModel>();
            String selectQuery = "SELECT * FROM " + TABLE_SCHEDULES + " WHERE " + KEY_IDCOMP + "='" + id + "'" ;
            SQLiteDatabase DB = databasehelp.getWritableDatabase();
            Cursor cursor = DB.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do{
                    ScheduleModel model = new ScheduleModel();
                    model.setId_schedules(cursor.getInt(cursor.getColumnIndex(KEY_SIDSCH)));
                    model.setOrigen(cursor.getString(cursor.getColumnIndex(KEY_SORI)));
                    model.setDestino(cursor.getString(cursor.getColumnIndex(KEY_SDES)));
                    model.setFecha(cursor.getString(cursor.getColumnIndex(KEY_SDATE)));
                    model.setHora(cursor.getString(cursor.getColumnIndex(KEY_SHOUR)));
                    model.setId_compania(cursor.getInt(cursor.getColumnIndex(KEY_IDCOMP)));
                    model.setCompania(cursor.getString(cursor.getColumnIndex(KEY_SCOMP)));
                    model.setConductor(cursor.getString(cursor.getColumnIndex(KEY_SDRI)));
                    model.setUnidad(cursor.getString(cursor.getColumnIndex(KEY_SUNI)));
                    model.setProducto(cursor.getString(cursor.getColumnIndex(KEY_SPRO)));
                    model.setEstado(cursor.getString(cursor.getColumnIndex(KEY_SEST)));
                    models.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();
            DB.close();
            return models;
        }
    }
    public class AppStateSql{
        public AppStateSql(){}
        public void deleteAppStateTable(){
            SQLiteDatabase DB = databasehelp.getWritableDatabase();
            DB.delete(TABLE_STATES,null,null);
            DB.execSQL( "DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_STATES + "'");
            DB.close();
        }
        public void addState(StateModel stateModel){
            SQLiteDatabase DB = databasehelp.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_IDSTET,stateModel.getId_enterprise());
            values.put(KEY_ENTST,stateModel.getEnterprise());
            values.put(KEY_UST,stateModel.getUnit());
            values.put(KEY_ORIST,stateModel.getOrigin());
            values.put(KEY_DESST,stateModel.getDestination());
            values.put(KEY_STST,stateModel.getState());
            values.put(KEY_SDTST,stateModel.getStart_date());
            values.put(KEY_EDTST,stateModel.getEnd_date());
            values.put(KEY_ADST,stateModel.getAdvance());
            DB.insert(TABLE_STATES,null,values);
        }
        public ArrayList<StateModel> getAllStates() {
            ArrayList<StateModel> models = new ArrayList<StateModel>();
            String selectQuery = "SELECT * FROM " + TABLE_STATES;
            SQLiteDatabase DB = databasehelp.getWritableDatabase();
            Cursor cursor = DB.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do{
                    StateModel model = new StateModel();
                    model.setIdState(cursor.getString(cursor.getColumnIndex(KEY_IDST)));
                    model.setId_enterprise(cursor.getInt(cursor.getColumnIndex(KEY_IDSTET)));
                    model.setEnterprise(cursor.getString(cursor.getColumnIndex(KEY_ENTST)));
                    model.setUnit(cursor.getString(cursor.getColumnIndex(KEY_UST)));
                    model.setOrigin(cursor.getString(cursor.getColumnIndex(KEY_ORIST)));
                    model.setDestination(cursor.getString(cursor.getColumnIndex(KEY_DESST)));
                    model.setState(cursor.getString(cursor.getColumnIndex(KEY_STST)));
                    model.setStart_date(cursor.getString(cursor.getColumnIndex(KEY_SDTST)));
                    model.setEnd_date(cursor.getString(cursor.getColumnIndex(KEY_EDTST)));
                    model.setAdvance(cursor.getString(cursor.getColumnIndex(KEY_ADST)));
                    models.add(model);
                } while (cursor.moveToNext());

            }
            cursor.close();
            DB.close();
            return models;
        }
        public ArrayList<StateModel> getStateByID(int id) {
            ArrayList<StateModel> models = new ArrayList<StateModel>();
            String selectQuery = "SELECT * FROM " + TABLE_STATES + " WHERE " + KEY_IDSTET + "='" + id + "'" ;
            SQLiteDatabase DB = databasehelp.getWritableDatabase();
            Cursor cursor = DB.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do{
                    StateModel model = new StateModel();
                    model.setIdState(cursor.getString(cursor.getColumnIndex(KEY_IDST)));
                    model.setId_enterprise(cursor.getInt(cursor.getColumnIndex(KEY_IDSTET)));
                    model.setEnterprise(cursor.getString(cursor.getColumnIndex(KEY_ENTST)));
                    model.setUnit(cursor.getString(cursor.getColumnIndex(KEY_UST)));
                    model.setOrigin(cursor.getString(cursor.getColumnIndex(KEY_ORIST)));
                    model.setDestination(cursor.getString(cursor.getColumnIndex(KEY_DESST)));
                    model.setState(cursor.getString(cursor.getColumnIndex(KEY_STST)));
                    model.setStart_date(cursor.getString(cursor.getColumnIndex(KEY_SDTST)));
                    model.setEnd_date(cursor.getString(cursor.getColumnIndex(KEY_EDTST)));
                    model.setAdvance(cursor.getString(cursor.getColumnIndex(KEY_ADST)));
                    models.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();
            return models;
        }


    }
}


