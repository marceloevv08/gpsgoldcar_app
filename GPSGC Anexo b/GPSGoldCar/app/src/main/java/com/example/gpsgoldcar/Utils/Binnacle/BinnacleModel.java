package com.example.gpsgoldcar.Utils.Binnacle;

import java.util.List;

public class BinnacleModel {
    private int programing;
    private String description;
    private String date_register;
    private int user_id;
    private List<Values> values;

    public BinnacleModel(int programing, String description, String date_register, int user_id, List<Values> values) {
        this.programing = programing;
        this.description = description;
        this.date_register = date_register;
        this.user_id = user_id;
        this.values = values;
    }

    public BinnacleModel() {
    }

    public int getPrograming() {
        return programing;
    }

    public void setPrograming(int programing) {
        this.programing = programing;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate_register() {
        return date_register;
    }

    public void setDate_register(String date_register) {
        this.date_register = date_register;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public List<Values> getValues() {
        return values;
    }

    public void setValues(List<Values> values) {
        this.values = values;
    }
}
