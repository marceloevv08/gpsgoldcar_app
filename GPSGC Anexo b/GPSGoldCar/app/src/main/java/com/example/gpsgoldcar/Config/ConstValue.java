package com.example.gpsgoldcar.Config;

public class ConstValue {
    public static String BASE_URL = "http://anexob.gpsgoldcar.com/api/"; //Production
    public static String AUTH_LOGIN = BASE_URL + "authentication/login/";
    public static String GET_VEHICLES = BASE_URL + "anexob/vehicles/";
    public static String GET_DRIVERS = BASE_URL+ "anexob/drivers/all/";
    public static String GET_STATES = BASE_URL + "anexob/stateprogramings/";;
}
