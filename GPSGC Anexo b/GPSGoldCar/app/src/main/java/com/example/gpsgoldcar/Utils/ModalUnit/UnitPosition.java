package com.example.gpsgoldcar.Utils.ModalUnit;

public class UnitPosition {
    private int pk;
    private String avl_unit_name;
    private Gps_device gps_device;

    public UnitPosition(int pk, String avl_unit_name, Gps_device gps_device) {
        this.pk = pk;
        this.avl_unit_name = avl_unit_name;
        this.gps_device = gps_device;
    }

    public UnitPosition() {
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getAvl_unit_name() {
        return avl_unit_name;
    }

    public void setAvl_unit_name(String avl_unit_name) {
        this.avl_unit_name = avl_unit_name;
    }

    public Gps_device getGps_device() {
        return gps_device;
    }

    public void setGps_device(Gps_device gps_device) {
        this.gps_device = gps_device;
    }
}
