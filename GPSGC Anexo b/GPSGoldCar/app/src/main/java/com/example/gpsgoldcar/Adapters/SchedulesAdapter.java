package com.example.gpsgoldcar.Adapters;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gpsgoldcar.Database.SqliteClass;
import com.example.gpsgoldcar.Models.ScheduleModel;
import com.example.gpsgoldcar.R;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class SchedulesAdapter extends RecyclerView.Adapter<SchedulesAdapter.ViewHolder> {
    Context ctx;
    List<ScheduleModel> schedules;
    OnItemClickListener onItemClickListener;


    public SchedulesAdapter(Context ctx, List<ScheduleModel> schedules, OnItemClickListener onItemClickListener) {
        this.ctx = ctx;
        this.schedules = schedules;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View schView = inflater.inflate(R.layout.schedule_cardview, parent, false);
        schView.setBackgroundColor(Color.parseColor("#fafafa"));
        return new ViewHolder(schView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(schedules.get(position));

    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    public void filteredList(ArrayList<ScheduleModel> schedulesList) {
        schedules = schedulesList;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(ScheduleModel scheduleModel);
        void  onDetailClick(ScheduleModel scheduleModel);
        void onEditClick(ScheduleModel scheduleModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        MaterialCardView schedule_table;
        TextView v_origin, v_dest, v_date, v_com,v_st;
        ArrayList<ScheduleModel> list;
        FloatingActionButton button,button1,button2;
        OnItemClickListener onItemClickListener;

        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            this.onItemClickListener = onItemClickListener;
            schedule_table = (MaterialCardView) itemView.findViewById(R.id.schedules_card);

            /** Valores **/
            v_origin = (TextView) itemView.findViewById(R.id.origin_sch);
            v_dest = (TextView) itemView.findViewById(R.id.destiny_sch);
            v_com = (TextView) itemView.findViewById(R.id.company_sch);
            v_st = (TextView) itemView.findViewById(R.id.state_sch);
            v_date = (TextView) itemView.findViewById(R.id.date_sch);

            /**Botón Unidades*/
            button = (FloatingActionButton) itemView.findViewById(R.id.schedule_units);
            button.setOnClickListener(this);
            /**Botón detalles*/
            button1 = (FloatingActionButton) itemView.findViewById(R.id.schedules_detail);
            button1.setOnClickListener(this);
            /**Botón editar*/
            button2 =(FloatingActionButton) itemView.findViewById(R.id.schedules_edit);
            button2.setOnClickListener(this);

        }

        public void bind(ScheduleModel scheduleModel) {

            list = SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.getAllSchedules();
            v_origin.setText(scheduleModel.getOrigen());
            v_dest.setText(scheduleModel.getDestino());
            v_date.setText(scheduleModel.getFecha());
            v_com.setText(scheduleModel.getCompania());
            v_st.setText(scheduleModel.getEstado());

            switch (v_st.getText().toString()){
                case "PROGRAMED":
                    v_st.setBackgroundResource(R.drawable.programed_shape);
                    v_st.setTextColor(Color.WHITE);
                    break;
                case "CANCELED":
                    v_st.setBackgroundResource(R.drawable.canceled_shape);
                    v_st.setTextColor(Color.WHITE);
                    break;
                case "CONFIRMED":
                    v_st.setBackgroundResource(R.drawable.confirmed_shape);
                    v_st.setTextColor(Color.WHITE);
                    break;
                case "DELAYED":
                    v_st.setBackgroundResource(R.drawable.delayed_shape);
                    v_st.setTextColor(Color.WHITE);
                    break;
                case  "ON_ROUTE":
                    v_st.setBackgroundResource(R.drawable.onroute_shape);
                    v_st.setBackgroundColor(Color.parseColor("#343a40"));
                    v_st.setTextColor(Color.WHITE);
                    break;
                case "ARRIVED":
                    v_st.setBackgroundResource(R.drawable.arrived_shape);
                    v_st.setTextColor(Color.WHITE);
                    break;
                case "COMPLETED":
                    v_st.setBackgroundResource(R.drawable.completed_shape);
                    v_st.setTextColor(Color.WHITE);
                    break;

            }



        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.schedule_units:
                    onItemClickListener.onItemClick(schedules.get(getAdapterPosition()));
                    break;
                case R.id.schedules_detail:
                    onItemClickListener.onDetailClick(schedules.get(getAdapterPosition()));
                    break;
                case R.id.schedules_edit:
                    onItemClickListener.onEditClick(schedules.get(getAdapterPosition()));
                    break;
            }

        }


    }
}
