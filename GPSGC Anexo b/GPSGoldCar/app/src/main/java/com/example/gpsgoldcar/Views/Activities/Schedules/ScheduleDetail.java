package com.example.gpsgoldcar.Views.Activities.Schedules;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.gpsgoldcar.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.card.MaterialCardView;

import java.util.Objects;

public class ScheduleDetail extends AppCompatActivity {
    MaterialCardView cardView;
    MaterialToolbar toolbar;
    TextView origin_sch,destiny_sch,company_sch,driver_sch,date_sch,hour_sch,unit_sch,product_sch,st_sch;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedules_table);
        cardView = findViewById(R.id.schedules_card);
        toolbar = (MaterialToolbar) findViewById(R.id.toolbarschdet);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        /**Cabeceras*/
        origin_sch = findViewById(R.id.origin_sch);
        destiny_sch = findViewById(R.id.destiny_sch);
        company_sch = findViewById(R.id.company_sch);
        driver_sch = findViewById(R.id.driver_sch);
        date_sch = findViewById(R.id.date_sch);
        hour_sch = findViewById(R.id.hour_sch);
        unit_sch = findViewById(R.id.unit_sch);
        product_sch = findViewById(R.id.product_sch);
        st_sch = findViewById(R.id.state_sch);
        /** Obtenemos los valores*/
        String  origin = getIntent().getStringExtra("Origen");
        String destiny = getIntent().getStringExtra("Destino");
        String company = getIntent().getStringExtra("Compañia");
        String drivers = getIntent().getStringExtra("Conductores");
        String date = getIntent().getStringExtra("Fecha");
        String hour = getIntent().getStringExtra("Hora");
        String vehicles = getIntent().getStringExtra("Vehiculos");
        String product = getIntent().getStringExtra("Producto");
        String state = getIntent().getStringExtra("Estado");
        /**Seteamos los valores*/
        origin_sch.setText(origin);
        destiny_sch.setText(destiny);
        company_sch.setText(company);
        driver_sch.setText(drivers);
        date_sch.setText(date);
        hour_sch.setText(hour);
        unit_sch.setText(vehicles);
        product_sch.setText(product);
        st_sch.setText(state);

    }
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
}
