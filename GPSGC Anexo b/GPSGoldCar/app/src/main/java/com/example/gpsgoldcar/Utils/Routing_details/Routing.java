package com.example.gpsgoldcar.Utils.Routing_details;

import java.util.List;

public class Routing {
    private int pk;
    private Route route;
    private List<Routing_details> routing_details;


    public Routing() {
    }



    public Routing(int pk, Route route, List<Routing_details> routing_details) {
        this.pk = pk;
        this.route = route;
        this.routing_details = routing_details;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public List<Routing_details> getRouting_details() {
        return routing_details;
    }

    public void setRouting_details(List<Routing_details> routing_details) {
        this.routing_details = routing_details;
    }
}
