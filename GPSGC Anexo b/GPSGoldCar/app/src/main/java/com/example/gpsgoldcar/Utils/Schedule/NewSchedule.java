package com.example.gpsgoldcar.Utils.Schedule;

import java.util.Arrays;
import java.util.List;

public class NewSchedule {
    private int route_id;
    private Schedule programing;
    private String routing_date;
    private String routing_time;
    private boolean  is_active;
    private List<RoutingDetails> routing_details;
    private String state;

    public NewSchedule() {
    }

    public NewSchedule(int route_id, Schedule programing, String routing_date, String routing_time, boolean is_active,List<RoutingDetails> routing_details, String state) {
        this.route_id = route_id;
        this.programing = programing;
        this.routing_date = routing_date;
        this.routing_time = routing_time;
        this.is_active = is_active;
        this.routing_details = routing_details;
        this.state = state;
    }


    public Schedule getPrograming() {
        return programing;
    }

    public void setPrograming(Schedule programing) {
        this.programing = programing;
    }

    public int getRoute_id() {
        return route_id;
    }

    public void setRoute_id(int route_id) {
        this.route_id = route_id;
    }

    public List<RoutingDetails> getRoutingDetails() {
        return routing_details;
    }

    public void setRoutingDetails(List<RoutingDetails> routingDetails) {
        this.routing_details = routingDetails;
    }

    public String getRouting_time() {
        return routing_time;
    }

    public void setRouting_time(String routing_time) {
        this.routing_time = routing_time;
    }

    public String getRouting_date() {
        return routing_date;
    }

    public void setRouting_date(String routing_date) {
        this.routing_date = routing_date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public String toString(){
        return Arrays.toString(routing_details.toArray());
    }

}
