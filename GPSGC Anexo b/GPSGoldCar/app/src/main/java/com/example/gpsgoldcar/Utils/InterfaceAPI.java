package com.example.gpsgoldcar.Utils;
import com.example.gpsgoldcar.Utils.Binnacle.BinnacleModel;
import com.example.gpsgoldcar.Utils.Driver_Filtered.DriverFiltered;
import com.example.gpsgoldcar.Utils.ModalUnit.Geofence;
import com.example.gpsgoldcar.Utils.ModalUnit.Point;
import com.example.gpsgoldcar.Utils.ModalUnit.UnitPosition;
import com.example.gpsgoldcar.Utils.Routing_details.Route;
import com.example.gpsgoldcar.Utils.Routing_details.AvlUnitRouting;
import com.example.gpsgoldcar.Utils.ScheduleCreator.NewSchedule;
import com.example.gpsgoldcar.Utils.ScheduleEditor.ScheduleEdited;
import com.example.gpsgoldcar.Utils.SchedulesModel.Filter;
import com.example.gpsgoldcar.Utils.SchedulesModel.ScheduleModelInterface;
import com.example.gpsgoldcar.Utils.Vehicles_Filtered.VehicleFiltered;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

public interface InterfaceAPI {
    //Login
    @POST("/api/authentication/login/")
    Call<AuthClass> checkLogin(@Body AuthClass body);

    //Obtener Programaciones
    @POST("api/anexob/programings_filter/")
    Call<List<ScheduleModelInterface>> getSchedules(@Header ("Authorization") String auth, @Body Filter body);

    //Obtener última posición de los vehiculos
    @GET("/api/avls/last_position/")
    Call<List<UnitPosition>> getLastPositions(@Header ("Authorization") String auth);

    //Obtener Geozonas segun unidad
    @POST("/api/anexob/avl_in_geofence/")
    Call<Geofence> getGeofenceByPosition(@Header ("Authorization") String auth , @Body Point body);

    //Obtener RoutingDetails
    @GET
    Call<AvlUnitRouting> getDataRouting(@Url String url, @Header ("Authorization") String auth);

    //Obtener Productos
    @GET("/api/anexob/products/")
    Call<List<Product>> getProducts(@Header ("Authorization") String auth);

    //Crear nuevos Productos
    @POST("/api/anexob/products/")
    Call<Product> createProduct(@Header ("Authorization") String auth ,@Body Product body);

    //Crear nuevas Programaciones
    @POST("/api/anexob/programings/")
    Call<NewSchedule> createSchedule(@Header ("Authorization") String auth ,@Body NewSchedule body);

    //Obtener rutas de la empresa
    @GET("/api/avls/routes/")
    Call <List<Route>> getRoutes(@Header ("Authorization") String auth );

    //Obtener Compañias
    @GET("/api/companies/companies/")
    Call<List<Company>> getCompanies(@Header ("Authorization") String auth);

    //Obtener Conductores Filtrados
    @GET("/api/anexob/driver_filtered/")
    Call<List<DriverFiltered>> getDrivers(@Header ("Authorization") String auth);

    //Obtener Vehiculos Filtrados
    @GET("/api/anexob/vehicle_filtered/")
    Call<List<VehicleFiltered>> getVehicles(@Header ("Authorization") String auth);

    //Editar Programaciones
    //Obtenemos la programacion a editar
    @GET
    Call<ScheduleModelInterface> getSchedule(@Url String url,  @Header ("Authorization") String auth);

    //Eviamos los datos editados
    @PUT
    Call<ScheduleModelInterface> updateSchedule(@Url String url, @Header ("Authorization") String auth, @Body ScheduleEdited body);

    @POST("api/anexob/bitacora_history/")
    Call<BinnacleModel> postBinaccle(@Header ("Authorization") String auth , @Body BinnacleModel body);



}
