package com.example.gpsgoldcar.Utils.Routing_details;

public class AVLRD {
    private int pk;
    private Routing routing;
    public AVLRD() {
    }

    public AVLRD(int pk, Routing routing) {
        this.pk = pk;
        this.routing = routing;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public Routing getRouting() {
        return routing;
    }

    public void setRouting(Routing routing) {
        this.routing = routing;
    }
}
