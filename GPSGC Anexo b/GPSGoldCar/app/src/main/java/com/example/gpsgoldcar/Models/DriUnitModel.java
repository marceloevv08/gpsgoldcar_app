package com.example.gpsgoldcar.Models;

public class DriUnitModel {
    private int driverpk;
    private String driver;
    private String unit;
    private int unitpk;

    public DriUnitModel(String driver, String unit) {
        this.driver = driver;
        this.unit = unit;
    }

    public DriUnitModel() {
    }

    public DriUnitModel(int driverpk, String driver, String unit, int unitpk) {
        this.driverpk = driverpk;
        this.driver = driver;
        this.unit = unit;
        this.unitpk = unitpk;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getDriverpk() {
        return driverpk;
    }

    public void setDriverpk(int driverpk) {
        this.driverpk = driverpk;
    }

    public int getUnitpk() {
        return unitpk;
    }

    public void setUnitpk(int unitpk) {
        this.unitpk = unitpk;
    }
}
