package com.example.gpsgoldcar.Views.Activities;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.gpsgoldcar.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.card.MaterialCardView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class VehicleDetail extends AppCompatActivity {
    MaterialCardView c_soat,c_poliza,c_rev_tec,c_vig_cont,c_gps,c_estado;
    TextView placa, soat_t, poliza, revtec, vigencia,gps_t, estado;
    Date today = Calendar.getInstance().getTime();
    MaterialToolbar toolbar;
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd");
    String todayString = formatdate.format(today);
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicles_table);
        toolbar = (MaterialToolbar) findViewById(R.id.toolbarunitdet);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        /**Cardviews*/
        c_soat = (MaterialCardView) findViewById(R.id.c_soat);
        c_poliza = (MaterialCardView) findViewById(R.id.c_poliza);
        c_rev_tec = (MaterialCardView) findViewById(R.id.c_rev_tec);
        c_vig_cont= (MaterialCardView) findViewById(R.id.c_vig_contrato);
        c_gps = (MaterialCardView) findViewById(R.id.c_gps);
        c_estado = (MaterialCardView) findViewById(R.id.c_st);

        /**Valores*/
        placa= (TextView) findViewById(R.id.placa);
        soat_t= (TextView) findViewById(R.id.soat);
        poliza= (TextView) findViewById(R.id.poliza);
        revtec= (TextView) findViewById(R.id.rev_tec);
        vigencia= (TextView) findViewById(R.id.vig_con);
        gps_t= (TextView) findViewById(R.id.gps);
        estado = (TextView)findViewById(R.id.estado);

        /**Recibimos los valores */
        String placav = getIntent().getStringExtra("Placa");
        String soat = getIntent().getStringExtra("Soat");
        String polizav = getIntent().getStringExtra("Poliza");
        String rev_tec = getIntent().getStringExtra("Rev_tec");
        String vig_cont = getIntent().getStringExtra("Vig_cont");
        String gps = getIntent().getStringExtra("Gps");
        String estadov = getIntent().getStringExtra("Estado");

        /**Seteamos los valores*/
        placa.setText(placav);
        soat_t.setText(soat);
        poliza.setText(polizav);
        revtec.setText(rev_tec);
        vigencia.setText(vig_cont);
        gps_t.setText(gps);
        if(estadov.equals("true")){
            estado.setText("Activo");
        }else {
            estado.setText("Inactivo");
        }

        /**Semaforizacion */
        try {
            /** Obetenemos las fechas de cada campo**/
            Date soatDate = formatdate.parse(soat);
            Date poliDate = formatdate.parse(polizav);
            Date revDate = formatdate.parse(rev_tec);
            Date vigDate = formatdate.parse(vig_cont);
            Date gpsDate = formatdate.parse(gps);
            Date todayDate = formatdate.parse(todayString);
            /** Calculamos la diferencia entre la fecha actual y la fecha de cada campo*/
            long daysSoatDifference = soatDate.getTime()-todayDate.getTime();
            long daysPolDifference = poliDate.getTime()-todayDate.getTime();
            long daysRevDifference = revDate.getTime()-todayDate.getTime();
            long daysVigDifference = vigDate.getTime()-todayDate.getTime();
            long daysGpsDifference = gpsDate.getTime()-todayDate.getTime();
            /** Casteamos los días de diferencia*/
            long daysSoat = TimeUnit.DAYS.convert(daysSoatDifference,TimeUnit.MILLISECONDS);
            long daysPoli = TimeUnit.DAYS.convert(daysPolDifference,TimeUnit.MILLISECONDS);
            long daysRev = TimeUnit.DAYS.convert(daysRevDifference,TimeUnit.MILLISECONDS);
            long daysVig = TimeUnit.DAYS.convert(daysVigDifference,TimeUnit.MILLISECONDS);
            long daysGPS = TimeUnit.DAYS.convert(daysGpsDifference,TimeUnit.MILLISECONDS);

            /**Validamos cada campo*/
            //SOAT
            if(daysSoat>=30) {
                c_soat.setCardBackgroundColor(Color.parseColor("#81C784"));
            }
            else if(15<= daysSoat) {
                c_soat.setCardBackgroundColor(Color.parseColor("#ffca28"));

            }else {
                c_soat.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //POLIZA
            if(daysPoli>=30) {
                c_poliza.setCardBackgroundColor(Color.parseColor("#81C784"));
            }
            else if(15<= daysPoli) {
                c_poliza.setCardBackgroundColor(Color.parseColor("#ffca28"));

            }else {
                c_poliza.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //REVISIÓN_TECNICA
            if(daysRev>=30) {
                c_rev_tec.setCardBackgroundColor(Color.parseColor("#81C784"));
            }
            else if(15<= daysRev) {
                c_rev_tec.setCardBackgroundColor(Color.parseColor("#ffca28"));

            }else {
                c_rev_tec.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //VIGENCIA
            if(daysVig>=30) {
                c_vig_cont.setCardBackgroundColor(Color.parseColor("#81C784"));
            }
            else if(15<= daysVig) {
                c_vig_cont.setCardBackgroundColor(Color.parseColor("#ffca28"));

            }else {
                c_vig_cont.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }
            //GPS
            if(daysGPS>=30) {
                c_gps.setCardBackgroundColor(Color.parseColor("#81C784"));
            }
            else if(15<= daysGPS) {
                c_gps.setCardBackgroundColor(Color.parseColor("#ffca28"));

            }else {
                c_gps.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }

            /** OBTENEMOS EL ESTDO DEL CONDUCTOR **/

            if(estadov.equals("true")){
                c_estado.setCardBackgroundColor(Color.parseColor("#81C784"));
            }else {
                c_estado.setCardBackgroundColor(Color.parseColor("#EF5350"));
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
}
