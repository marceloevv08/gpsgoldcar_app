package com.example.gpsgoldcar.Models;

public class DriverModel {
    private int idd;
    private String nombre;
    private String dni;
    private String f_dni;
    private String lic_con;
    private String induccion;
    private String ftcheck;
    private String sctr;
    private String pas_med;
    private int id_compania;
    private String estado;

    public DriverModel() {
    }
    public DriverModel(int idd, String nombre, String dni, String f_dni, String lic_con, String induccion, String ftcheck, String sctr, String pas_med, int id_compania, String estado, int id_driv) {
        this.idd = idd;
        this.nombre = nombre;
        this.dni = dni;
        this.f_dni = f_dni;
        this.lic_con = lic_con;
        this.induccion = induccion;
        this.ftcheck = ftcheck;
        this.sctr = sctr;
        this.pas_med = pas_med;
        this.id_compania = id_compania;
        this.estado = estado;
        this.id_driv = id_driv;
    }

    public int getId_driv() {
        return id_driv;
    }

    public void setId_driv(int id_driv) {
        this.id_driv = id_driv;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getF_dni() {
        return f_dni;
    }

    public void setF_dni(String f_dni) {
        this.f_dni = f_dni;
    }

    public String getLic_con() {
        return lic_con;
    }

    public void setLic_con(String lic_con) {
        this.lic_con = lic_con;
    }

    public String getInduccion() {
        return induccion;
    }

    public void setInduccion(String induccion) {
        this.induccion = induccion;
    }

    public String getFtcheck() {
        return ftcheck;
    }

    public void setFtcheck(String ftcheck) {
        this.ftcheck = ftcheck;
    }

    public String getSctr() {
        return sctr;
    }

    public void setSctr(String sctr) {
        this.sctr = sctr;
    }

    public String getPas_med() {
        return pas_med;
    }

    public void setPas_med(String pas_med) {
        this.pas_med = pas_med;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    private int id_driv;

    public int getIdd() {
        return idd;
    }

    public void setIdd(int idd) {
        this.idd = idd;
    }

    public String getNombre() {
        return nombre;
    }
    public int getId_compania() {
        return id_compania;
    }

    public void setId_compania(int id_compania) {
        this.id_compania = id_compania;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
