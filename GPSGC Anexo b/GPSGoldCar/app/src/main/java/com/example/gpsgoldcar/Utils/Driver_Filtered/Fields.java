package com.example.gpsgoldcar.Utils.Driver_Filtered;

public class Fields {
    private String name;
    private String picture;
    private boolean is_active;
    private String created;
    private String updated;

    public Fields(String name, String picture, boolean is_active, String created, String updated) {
        this.name = name;
        this.picture = picture;
        this.is_active = is_active;
        this.created = created;
        this.updated = updated;
    }

    public Fields() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }
}
