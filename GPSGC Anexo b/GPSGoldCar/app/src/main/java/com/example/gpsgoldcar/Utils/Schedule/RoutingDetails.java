package com.example.gpsgoldcar.Utils.Schedule;

public class RoutingDetails {
    private String driver;
    private String avl_unit;
    private int avl_unit_id;
    private int driver_id;

    public RoutingDetails() {
    }


    public RoutingDetails(String driver, String avl_unit, int avl_unit_id, int driver_id) {
        this.driver = driver;
        this.avl_unit = avl_unit;
        this.avl_unit_id = avl_unit_id;
        this.driver_id = driver_id;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getAvl_unit() {
        return avl_unit;
    }

    public void setAvl_unit(String avl_unit) {
        this.avl_unit = avl_unit;
    }

    public int getAvl_unit_id() {
        return avl_unit_id;
    }

    public void setAvl_unit_id(int avl_unit_id) {
        this.avl_unit_id = avl_unit_id;
    }

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }
    public String toString(){
        return "driver:"+ getDriver()+",avl_unit:"+getAvl_unit()+",avl_unit_id:"+getAvl_unit_id()+",driver_id:"+getDriver_id();
    }
}
