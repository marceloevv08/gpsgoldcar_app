package com.connecttix.gpsgoldcar.Utils.Routing_details;

public class OriginPlace {
    private String type;
    private Properties properties;

    public OriginPlace(String type, Properties properties) {
        this.type = type;
        this.properties = properties;
    }

    public OriginPlace() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

}
