package com.connecttix.gpsgoldcar.Utils.ScheduleCreator;

public class Schedule {
    private int product_id;
    private String geofence;



    public Schedule() {
    }

    public Schedule(int product_id, String geofence) {
        this.product_id = product_id;
        this.geofence = geofence;
    }

    public String getGeofence() {
        return geofence;
    }

    public void setGeofence(String geofence) {
        this.geofence = geofence;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
}
