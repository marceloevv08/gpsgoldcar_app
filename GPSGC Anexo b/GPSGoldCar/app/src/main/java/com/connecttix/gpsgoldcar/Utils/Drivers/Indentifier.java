package com.connecttix.gpsgoldcar.Utils.Drivers;

public class Indentifier {
    private int pk;
    private String code;
    private String name;

    public Indentifier(int pk, String code, String name) {
        this.pk = pk;
        this.code = code;
        this.name = name;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
