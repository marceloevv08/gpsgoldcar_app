package com.connecttix.gpsgoldcar.Utils.Drivers;

public class Party_Identifier {
    private int pk;
    private Indentifier indentifier;
    private String identifier_number;
    private String identifier_due_date;

    public Party_Identifier(int pk, Indentifier indentifier, String identifier_number, String identifier_due_date) {
        this.pk = pk;
        this.indentifier = indentifier;
        this.identifier_number = identifier_number;
        this.identifier_due_date = identifier_due_date;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public Indentifier getIndentifier() {
        return indentifier;
    }

    public void setIndentifier(Indentifier indentifier) {
        this.indentifier = indentifier;
    }

    public String getIdentifier_number() {
        return identifier_number;
    }

    public void setIdentifier_number(String identifier_number) {
        this.identifier_number = identifier_number;
    }

    public String getIdentifier_due_date() {
        return identifier_due_date;
    }

    public void setIdentifier_due_date(String identifier_due_date) {
        this.identifier_due_date = identifier_due_date;
    }
}
