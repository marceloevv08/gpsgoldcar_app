package com.connecttix.gpsgoldcar.Views.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.connecttix.gpsgoldcar.BuildConfig;
import com.connecttix.gpsgoldcar.R;
import com.connecttix.gpsgoldcar.Utils.InterfaceAPI;
import com.connecttix.gpsgoldcar.Utils.ModalUnit.Geofence;
import com.connecttix.gpsgoldcar.Utils.ModalUnit.LastPosition;
import com.connecttix.gpsgoldcar.Utils.ModalUnit.Point;
import com.connecttix.gpsgoldcar.Utils.ModalUnit.UnitPosition;
import com.connecttix.gpsgoldcar.Utils.Retrofit.RetrofitClientInstance;
import com.connecttix.gpsgoldcar.Utils.Routing_details.OriginPlace;
import com.connecttix.gpsgoldcar.Utils.Routing_details.AvlUnitRouting;
import com.connecttix.gpsgoldcar.Utils.Routing_details.TargetPlace;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class MapActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    MapView map = null;
    ArrayList<OverlayItem> unitmap;
    Drawable marker;
    LocationManager locationManager;
    Location mylocation;
    Context ctx = this;


    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
        setContentView(R.layout.activity_map);
        map = (MapView) findViewById(R.id.map);
        map.getTileProvider().clearTileCache();
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);
        map.setBuiltInZoomControls(false);
        map.getController().setZoom(11.0);
        unitmap = new ArrayList<OverlayItem>();
        marker = this.getResources().getDrawable(R.drawable.marker);
        requestPermissions();
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mylocation= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Double latitude= mylocation.getLatitude();
        Double longitude = mylocation.getLongitude();
        GeoPoint mylocation= new GeoPoint(latitude,longitude);
        map.getController().animateTo(mylocation);
        map.getController().setCenter(mylocation);

        new getLasPositions().execute();


    }

    public void onResume() {
        super.onResume();
        map.onResume();
    }

    public void onPause() {
        super.onPause();
        map.onPause();
    }


    public String createBasicAuth(String username, String password) {
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }

    class getLasPositions extends AsyncTask<Boolean, Void, String> {

        String unidades = getIntent().getStringExtra("Unidades");
        List<String> unida = new ArrayList<>(Arrays.asList(unidades.split("\n")));

        @Override
        protected String doInBackground(Boolean... booleans) {
            SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
            String username = sharedPref.getString("username", "");
            String password = sharedPref.getString("password", "");
            Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
            String AUTH = createBasicAuth(username, password);
            final InterfaceAPI api = retrofit.create(InterfaceAPI.class);
            Call<List<UnitPosition>> call = api.getLastPositions(AUTH);
            call.enqueue(new Callback<List<UnitPosition>>() {
                @Override
                public void onResponse(Call<List<UnitPosition>> call, Response<List<UnitPosition>> response) {
                    if (response.isSuccessful()) {
                        List<UnitPosition> unidades = response.body();
                        for (UnitPosition unidad : unidades) {
                            for(int i = 0; i< unida.size();i++){
                                if(unida.get(i).equals(unidad.getAvl_unit_name())){
                                    LastPosition lastPosition = unidad.getGps_device().getLast_position();
                                    if (lastPosition != null) {
                                        Double latitude = Double.parseDouble(unidad.getGps_device().getLast_position().getLatitude());
                                        Double longitude = Double.parseDouble(unidad.getGps_device().getLast_position().getLongitude());
                                        GeoPoint unit_avl = new GeoPoint(latitude, longitude);
                                        Marker unit = new Marker(map);
                                        unit.setPosition(unit_avl);
                                        unit.setIcon(marker);
                                        unit.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                        unit.setTitle(unidad.getAvl_unit_name());
                                        unit.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
                                            @SuppressLint("SetTextI18n")
                                            @Override
                                            public boolean onMarkerClick(Marker marker, MapView mapView) {
                                                Dialog modal = new Dialog(MapActivity.this);
                                                Objects.requireNonNull(modal.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
                                                modal.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                                modal.setContentView(R.layout.vehicle_modal);
                                                modal.setCancelable(true);
                                                // Datos
                                                //PLACA
                                                TextView placa = (TextView) modal.findViewById(R.id.dialog_placa);
                                                placa.setText(String.valueOf(unidad.getAvl_unit_name()));
                                                // DATE
                                                TextView date = (TextView) modal.findViewById(R.id.dialog_date);
                                                String date1= String.valueOf(unidad.getGps_device().getLast_position().getTimestamp()).substring(0,10);
                                                String time = String.valueOf(unidad.getGps_device().getLast_position().getTimestamp())
                                                        .substring(String.valueOf(unidad.getGps_device().getLast_position().getTimestamp()).indexOf("T")+1,
                                                                String.valueOf(unidad.getGps_device().getLast_position().getTimestamp()).indexOf("T")+9);
                                                date.setText(date1+" "+time);
                                                //SPEED
                                                TextView speed = (TextView) modal.findViewById(R.id.dialog_speed);
                                                speed.setText(String.valueOf(unidad.getGps_device().getLast_position().getSpeed()) + " Km/h");

                                                //GEOZONE
                                                TextView geozonetv = (TextView) modal.findViewById(R.id.dialog_geozone);
                                                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                                                String username = sharedPref.getString("username", "");
                                                String password = sharedPref.getString("password", "");
                                                Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
                                                String AUTH = createBasicAuth(username, password);
                                                final InterfaceAPI api = retrofit.create(InterfaceAPI.class);
                                                Point position = new Point(latitude, longitude);
                                                Call<Geofence> call1 = api.getGeofenceByPosition(AUTH, position);
                                                call1.enqueue(new Callback<Geofence>() {
                                                    @Override
                                                    public void onResponse(Call<Geofence> call, Response<Geofence> response) {
                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                String geozone = response.body().getGeofence();
                                                                geozonetv.setText(geozone);
                                                            } else {
                                                                String geozone = "-";
                                                                geozonetv.setText(geozone);
                                                            }
                                                        } else {
                                                            String geozone = "-";
                                                            geozonetv.setText(geozone);
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<Geofence> call, Throwable t) {
                                                        t.printStackTrace();
                                                    }
                                                });
                                                //DISTANCE:TIME && ROUTE
                                                TextView route_sheet = (TextView) modal.findViewById(R.id.dialog_route);
                                                TextView dis_time = (TextView) modal.findViewById(R.id.dialog_distance);
                                                TextView timetv = (TextView) modal.findViewById(R.id.dialog_time);
                                                ProgressBar progressBarPercent = (ProgressBar) modal.findViewById(R.id.bar_progress);
                                                TextView percenttv = (TextView) modal.findViewById(R.id.percent);
                                                TextView dist_percent = (TextView) modal.findViewById(R.id.total_distance);
                                                Call<AvlUnitRouting> call2 = api.getDataRouting("http://anexob.gpsgoldcar.com/api/avls/avlunit-routing/" + unidad.getPk(), AUTH);
                                                call2.enqueue(new Callback<AvlUnitRouting>() {
                                                    @SuppressLint("SetTextI18n")
                                                    @Override
                                                    public void onResponse(Call<AvlUnitRouting> call, Response<AvlUnitRouting> response) {
                                                        if (response.isSuccessful()) {
                                                            if (response.body() != null) {
                                                                if (response.body().getAvl_unit_routing_detail() != null) {
                                                                    if (response.body().getAvl_unit_routing_detail().getRouting() != null) {
                                                                        OriginPlace originplace = response.body().getAvl_unit_routing_detail().getRouting().getRoute().getOrigin_place();
                                                                        TargetPlace targetPlace = response.body().getAvl_unit_routing_detail().getRouting().getRoute().getTarget_place();
                                                                        if (originplace != null) {
                                                                            String origin = originplace.getProperties().getName();
                                                                            String target = targetPlace.getProperties().getName();
                                                                            route_sheet.setText(origin + "-" + target);
                                                                            String di_time = response.body().getAvl_unit_routing_detail().getRouting().getRoute().getRouting_distance_kilometres() + " Km ";
                                                                            String time = response.body().getAvl_unit_routing_detail().getRouting().getRoute().getRouting_time() + " h";
                                                                            dis_time.setText(di_time);
                                                                            timetv.setText(time);
                                                                            String percent= response.body().getAvl_unit_routing_detail().getRouting().getRouting_details().get(0).getPercentage_from_route();
                                                                            double percetParse = Double.parseDouble(percent);
                                                                            if(percetParse<0){
                                                                                percetParse=0;
                                                                            }
                                                                            progressBarPercent.setProgress((int) Math.round(percetParse));
                                                                            percenttv.setText(percetParse+"%");
                                                                            dist_percent.setText(di_time);

                                                                        } else {
                                                                            dis_time.setText("-");
                                                                            timetv.setText("-");
                                                                            route_sheet.setText("-");
                                                                            progressBarPercent.setProgress(0);
                                                                            percenttv.setText("0%");
                                                                            dist_percent.setText("-");
                                                                        }
                                                                    } else {
                                                                        dis_time.setText("-");
                                                                        timetv.setText("-");
                                                                        route_sheet.setText("-");
                                                                        progressBarPercent.setProgress(0);
                                                                        percenttv.setText("0%");
                                                                        dist_percent.setText("-");
                                                                    }
                                                                } else {
                                                                    dis_time.setText("-");
                                                                    timetv.setText("-");
                                                                    route_sheet.setText("-");
                                                                    progressBarPercent.setProgress(0);
                                                                    percenttv.setText("0%");
                                                                    dist_percent.setText("-");
                                                                }
                                                            } else {
                                                                dis_time.setText("-");
                                                                timetv.setText("-");
                                                                route_sheet.setText("-");
                                                                progressBarPercent.setProgress(0);
                                                                percenttv.setText("0%");
                                                                dist_percent.setText("-");
                                                            }
                                                        } else {
                                                            dis_time.setText("-");
                                                            timetv.setText("-");
                                                            route_sheet.setText("-");
                                                            progressBarPercent.setProgress(0);
                                                            percenttv.setText("0%");
                                                            dist_percent.setText("-");
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<AvlUnitRouting> call, Throwable t) {
                                                        t.printStackTrace();
                                                    }
                                                });

                                                modal.show();
                                                return true;
                                            }
                                        });
                                        map.getController().animateTo(unit_avl);
                                        map.getController().setCenter(unit_avl);
                                        map.getOverlays().add(unit);
                                    } else {
                                        Toasty.warning(MapActivity.this, "Algunas unidades no se visualizan", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }

                        }
                    } else {
                        Toasty.error(MapActivity.this, "Sin respuesta del servidor", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<List<UnitPosition>> call, Throwable t) {
                    Log.e("TAG", t.toString());
                    t.printStackTrace();
                }
            });
            return null;
        }
    }

    public void requestPermissions(){
        String[] perms={
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q){
            if(EasyPermissions.hasPermissions(this,perms)) {

            }
            else {
                EasyPermissions.requestPermissions(this,"Necesita permisos para continuar",123,perms);
            }
        }else {
            EasyPermissions.hasPermissions(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            );
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this,perms)){
            new AppSettingsDialog.Builder(this).build().show();
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
