package com.connecttix.gpsgoldcar.Utils.Vehicles;

public class VehicleModelInterface {
    private int pk;
    private String imei;
    private int company;
    private String is_active;
    private Avl_Unit avl_unit;

    public VehicleModelInterface(int pk, String imei, int company, String is_active, Avl_Unit avl_unit) {
        this.pk = pk;
        this.imei = imei;
        this.company = company;
        this.is_active = is_active;
        this.avl_unit = avl_unit;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }

    public String isIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public Avl_Unit getAvl_unit() {
        return avl_unit;
    }

    public void setAvl_unit(Avl_Unit avl_unit) {
        this.avl_unit = avl_unit;
    }
}
