package com.connecttix.gpsgoldcar.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.VehicleModel;
import com.connecttix.gpsgoldcar.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.ViewHolder> {
    Context context;
    List<VehicleModel> vehicles;
    OnItemClickListener onItemClickListener;

    public VehicleAdapter(Context context, List<VehicleModel> vehicles, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.vehicles = vehicles;
        this.onItemClickListener = onItemClickListener;
    }


    @NonNull
    @Override
    public VehicleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(context);
        View vehView = inflater.inflate(R.layout.vehicles_cardview,parent,false);
        return new ViewHolder(vehView,onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull VehicleAdapter.ViewHolder holder, int position) {
        holder.bind(vehicles.get(position));

    }
    public void filteredList(ArrayList<VehicleModel> vehiclesList) {
        vehicles = vehiclesList;
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return vehicles.size();
    }

    public interface OnItemClickListener{
        void onItemClick(VehicleModel vehicleModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        MaterialCardView vehiclecardview;
        ArrayList<VehicleModel> list;
        TextView plate ,state, company;
        MaterialButton button;
        OnItemClickListener onItemClickListener;


        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            this.onItemClickListener = onItemClickListener;
            vehiclecardview = (MaterialCardView) itemView.findViewById(R.id.vehicles_card);
            company = (TextView) itemView.findViewById(R.id.vehicle_enterprise);
            plate = (TextView) itemView.findViewById(R.id.vehicle_plate);
            state = (TextView) itemView.findViewById(R.id.vehicle_state);
            /**Botón*/
            button = (MaterialButton) itemView.findViewById(R.id.vehicle_detail);
            button.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(vehicles.get(getAdapterPosition()));
        }

        public void bind(VehicleModel vehicleModel) {
            list = SqliteClass.getInstance(context).databasehelp.appVehicleSql.getAllVehicles();
            company.setText(vehicleModel.getName_company());
            plate.setText(vehicleModel.getPlaca());
            String state1 = vehicleModel.getEstado();
            if(state1.equals("true")){
                state.setText("Activo");
            }else {
                state.setText("Inactivo");
            }
        }
    }
}
