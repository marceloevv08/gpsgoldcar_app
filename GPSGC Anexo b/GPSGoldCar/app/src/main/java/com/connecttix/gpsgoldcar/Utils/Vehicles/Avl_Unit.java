package com.connecttix.gpsgoldcar.Utils.Vehicles;

import java.util.List;

public class Avl_Unit {
    private int pk;
    private String avl_unit_name;
    private Vehicle vehicle;
    private List<Avl_unit_routing_detail> avl_unit_routing_details;

    public Avl_Unit(int pk, String avl_unit_name, Vehicle vehicle, List<Avl_unit_routing_detail> avl_unit_routing_details) {
        this.pk = pk;
        this.avl_unit_name = avl_unit_name;
        this.vehicle = vehicle;
        this.avl_unit_routing_details = avl_unit_routing_details;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getAvl_unit_name() {
        return avl_unit_name;
    }

    public void setAvl_unit_name(String avl_unit_name) {
        this.avl_unit_name = avl_unit_name;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public List<Avl_unit_routing_detail> getAvl_unit_routing_details() {
        return avl_unit_routing_details;
    }

    public void setAvl_unit_routing_details(List<Avl_unit_routing_detail> avl_unit_routing_details) {
        this.avl_unit_routing_details = avl_unit_routing_details;
    }
}
