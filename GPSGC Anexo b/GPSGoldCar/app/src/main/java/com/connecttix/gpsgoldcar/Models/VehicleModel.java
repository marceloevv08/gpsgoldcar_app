package com.connecttix.gpsgoldcar.Models;

public class VehicleModel {
    private int id;
    private int id_vehicle;
    private int id_company;
    private String name_company;
    private String placa;
    private String soat;
    private String poliza;
    private String rev_tec;
    private String vig_contrato;
    private String gps;
    private String estado;

    public VehicleModel(){}

    public VehicleModel(int id, int id_vehicle, int id_company, String name_company, String placa, String soat, String poliza, String rev_tec, String vig_contrato, String gps, String estado) {
        this.id = id;
        this.id_vehicle = id_vehicle;
        this.id_company = id_company;
        this.name_company = name_company;
        this.placa = placa;
        this.soat = soat;
        this.poliza = poliza;
        this.rev_tec = rev_tec;
        this.vig_contrato = vig_contrato;
        this.gps = gps;
        this.estado = estado;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_vehicle() {
        return id_vehicle;
    }

    public void setId_vehicle(int id_vehicle) {
        this.id_vehicle = id_vehicle;
    }
    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getSoat() {
        return soat;
    }

    public void setSoat(String soat) {
        this.soat = soat;
    }

    public String getPoliza() {
        return poliza;
    }

    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }

    public String getRev_tec() {
        return rev_tec;
    }

    public void setRev_tec(String rev_tec) {
        this.rev_tec = rev_tec;
    }

    public String getVig_contrato() {
        return vig_contrato;
    }

    public void setVig_contrato(String vig_contrato) {
        this.vig_contrato = vig_contrato;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getName_company() {
        return name_company;
    }

    public void setName_company(String name_company) {
        this.name_company = name_company;
    }
}
