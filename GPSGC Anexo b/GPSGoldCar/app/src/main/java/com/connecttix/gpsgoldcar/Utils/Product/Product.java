package com.connecttix.gpsgoldcar.Utils.Product;

public class Product {
    private int pk;
    private String name;
    private String description;

    public Product() {
    }

    public Product(int pk,String name, String description) {
        this.pk=pk;
        this.name = name;
        this.description = description;
    }

    public Product(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
