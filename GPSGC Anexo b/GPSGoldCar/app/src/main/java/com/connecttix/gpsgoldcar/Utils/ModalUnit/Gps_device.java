package com.connecttix.gpsgoldcar.Utils.ModalUnit;

public class Gps_device {
    private int id;
    private String imei;
    private int company;
    private LastPosition last_position;

    public Gps_device(int id, String imei, int company, LastPosition last_position) {
        this.id = id;
        this.imei = imei;
        this.company = company;
        this.last_position = last_position;
    }

    public Gps_device() {
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }

    public LastPosition getLast_position() {
        return last_position;
    }

    public void setLast_position(LastPosition last_position) {
        this.last_position = last_position;
    }
}
