package com.connecttix.gpsgoldcar.Utils.SchedulesModel;

public class Company {
    private int pk;
    private String party;

    public Company(int pk, String party) {
        this.pk = pk;
        this.party = party;
    }

    public Company() {
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getParty() {
        return party;
    }

    public void setParty(String party) {
        this.party = party;
    }
}
