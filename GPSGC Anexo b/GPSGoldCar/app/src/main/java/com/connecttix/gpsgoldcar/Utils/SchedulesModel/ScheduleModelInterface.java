package com.connecttix.gpsgoldcar.Utils.SchedulesModel;

import java.util.List;

public class ScheduleModelInterface {
    private int id;
    private Programing programing;
    private Route route;
    private List<Routing_details> routing_details;
    private String routing_time;
    private String routing_date;
    private String state;
    private boolean is_active;

    public ScheduleModelInterface(int id, Programing programing, Route route, List<Routing_details> routing_details, String routing_time, String routing_date, String state, boolean is_active) {
        this.id = id;
        this.programing = programing;
        this.route = route;
        this.routing_details = routing_details;
        this.routing_time = routing_time;
        this.routing_date = routing_date;
        this.state = state;
        this.is_active = is_active;
    }

    public ScheduleModelInterface() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Programing getPrograming() {
        return programing;
    }

    public void setPrograming(Programing programing) {
        this.programing = programing;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public List<Routing_details> getRouting_details() {
        return routing_details;
    }

    public void setRouting_details(List<Routing_details> routing_details) {
        this.routing_details = routing_details;
    }

    public String getRouting_time() {
        return routing_time;
    }

    public void setRouting_time(String routing_time) {
        this.routing_time = routing_time;
    }

    public String getRouting_date() {
        return routing_date;
    }

    public void setRouting_date(String routing_date) {
        this.routing_date = routing_date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }
}
