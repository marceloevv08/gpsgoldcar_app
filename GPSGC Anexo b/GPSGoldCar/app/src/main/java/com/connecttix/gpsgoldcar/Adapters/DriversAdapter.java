package com.connecttix.gpsgoldcar.Adapters;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.DriverModel;
import com.connecttix.gpsgoldcar.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import java.util.ArrayList;
import java.util.List;

public class DriversAdapter extends RecyclerView.Adapter<DriversAdapter.ViewHolder> {
    Context context;
    List<DriverModel> drivers;
    OnItemClickListener onItemClickListener;

    public DriversAdapter(Context context, List<DriverModel> drivers,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.drivers = drivers;
        this.onItemClickListener=onItemClickListener;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View driView = inflater.inflate(R.layout.drivers_cardview,parent,false);
        driView.setBackgroundColor(Color.parseColor("#fafafa"));
        return new ViewHolder(driView,onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(drivers.get(position));
    }

    @Override
    public int getItemCount() {
        return drivers.size();
    }
    public void filteredList(ArrayList<DriverModel> driversList) {
        drivers = driversList;
        notifyDataSetChanged();
    }


    public interface OnItemClickListener {
        void onItemClick(DriverModel driverModel);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        MaterialCardView drivercardview;
        TextView driver_company,driver_name,driver_dni,driver_state;
        ArrayList<DriverModel> list;
        MaterialButton button;
        OnItemClickListener onItemClickListener;


        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            this.onItemClickListener = onItemClickListener;
            drivercardview = (MaterialCardView) itemView.findViewById(R.id.drivers_card);
            driver_company =(TextView) itemView.findViewById(R.id.driver_enterprise);
            driver_name = (TextView) itemView.findViewById(R.id.driver_name);
            driver_dni = (TextView) itemView.findViewById(R.id.driver_dni);
            driver_state = (TextView) itemView.findViewById(R.id.driver_state);
            /**Botón*/
            button = (MaterialButton) itemView.findViewById(R.id.driver_detail);
            button.setOnClickListener(this);

        }
        public void bind(DriverModel driverModel) {
            list = SqliteClass.getInstance(context).databasehelp.appDriverSql.getAllDrivers();
            driver_company.setText(driverModel.getCompania());
            driver_name.setText(driverModel.getNombre());
            driver_dni.setText(driverModel.getDni());
            String state = driverModel.getEstado();
            if(state.equals("true")){
                driver_state.setText("Activo");
            }else {
                driver_state.setText("Inactivo");
            }
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(drivers.get(getAdapterPosition()));
        }
    }
}
