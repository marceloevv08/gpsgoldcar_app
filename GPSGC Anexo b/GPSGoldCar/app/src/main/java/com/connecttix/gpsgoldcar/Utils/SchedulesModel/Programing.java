package com.connecttix.gpsgoldcar.Utils.SchedulesModel;

public class Programing {
    private int pk;
    private Company company;
    private Product product;
    private String geofence;

    public Programing(int pk, Company company, Product product, String geofence) {
        this.pk = pk;
        this.company = company;
        this.product = product;
        this.geofence = geofence;
    }

    public Programing() {
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getGeofence() {
        return geofence;
    }

    public void setGeofence(String geofence) {
        this.geofence = geofence;
    }
}
