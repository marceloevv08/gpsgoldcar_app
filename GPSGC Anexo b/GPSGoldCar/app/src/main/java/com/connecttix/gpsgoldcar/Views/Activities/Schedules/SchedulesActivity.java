package com.connecttix.gpsgoldcar.Views.Activities.Schedules;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.connecttix.gpsgoldcar.Adapters.SchedulesAdapter;
import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.ScheduleModel;
import com.connecttix.gpsgoldcar.Network.Protocol;
import com.connecttix.gpsgoldcar.R;
import com.connecttix.gpsgoldcar.Utils.Company.Company;
import com.connecttix.gpsgoldcar.Utils.InterfaceAPI;
import com.connecttix.gpsgoldcar.Utils.Retrofit.RetrofitClientInstance;
import com.connecttix.gpsgoldcar.Utils.Routing_details.Route;
import com.connecttix.gpsgoldcar.Views.Activities.MapActivity;
import com.connecttix.gpsgoldcar.Views.Fragments.DatePickerFragment;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonIOException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Retrofit;

public class SchedulesActivity extends AppCompatActivity implements SchedulesAdapter.OnItemClickListener {
    TextInputLayout ti_date,ti_comp,ti_state,ti_route;
    EditText date;
    Context ctx = this;
    MaterialToolbar toolbar;
    RecyclerView recyclerView;
    ArrayList<ScheduleModel> schedules;
    SchedulesAdapter adapter;
    ArrayList<String> companies, routes;
    Protocol protocol;
    AutoCompleteTextView compan, rout, st;
    Button search, cleaner;
    FloatingActionButton floatingActionButton;
    ArrayAdapter<String> adapterRoutes, adapterCompanies, adapterState;
    Dialog dialog;

    private static final String[] states = new String[]{
            "PROGRAMED", "CANCELED", "CONFIRMED", "DELAYED", "ON_ROUTE",
            "ARRIVED", "COMPLETED"
    };
    Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
    final InterfaceAPI api = retrofit.create(InterfaceAPI.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref = ctx.getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
        boolean isAdmin = sharedPref.getBoolean("user_is_super", true);
        int idCompany = sharedPref.getInt("employee_company_id", 0);
        protocol = new Protocol();
        companies = new ArrayList<String>();
        routes = new ArrayList<String>();
        //VISTAS
        setContentView(R.layout.activity_schedules);
        toolbar = (MaterialToolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ti_route = (TextInputLayout) findViewById(R.id.route_filter);
        ti_comp = (TextInputLayout) findViewById(R.id.company_filter);
        ti_state=(TextInputLayout) findViewById(R.id.state_filter1);
        ti_date = (TextInputLayout) findViewById(R.id.date_filter);


        // Listas
        compan = (AutoCompleteTextView) findViewById(R.id.companies);
        rout = (AutoCompleteTextView) findViewById(R.id.rutas);
        date = (EditText) findViewById(R.id.et_date);
        st = (AutoCompleteTextView) findViewById(R.id.state_filter);

        //Botones
        search = (Button) findViewById(R.id.search_schedules);
        cleaner = (Button) findViewById(R.id.clean_filter);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.createButton);

        // RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recycler_schedules);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));

        //Ruta
        GetRoutes getRoutes = new GetRoutes();
        getRoutes.execute();
        //Estados
        adapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, states);
        st.setAdapter(adapterState);

        //Fecha
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { showDatePickerDialog(); }
        });

        //Crear nueva programación
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionButton.setEnabled(false);
                Intent i = new Intent(ctx, SchedulesCreator.class);
                startActivity(i);
            }
        });

        if (isAdmin) {
            //Compañia
            GetCompanies getCompanies = new GetCompanies();
            getCompanies.execute();


            //Limpiar campos
            cleaner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rout.setText("");
                    compan.setText("");
                    st.setText("");
                    date.setText("");
                    adapter.filteredList(schedules);
                }
            });
            //Busqueda
            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    filter(rout.getText().toString(), compan.getText().toString(), st.getText().toString(), date.getText().toString());
                }
            });
            // Cargamos las programaciones
            schedules = SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.getAllSchedules();
            adapter = new SchedulesAdapter(ctx, schedules, this);
            recyclerView.setAdapter(adapter);

        } else {
            ti_comp.setVisibility(View.GONE);
            compan.setVisibility(View.GONE);

            //Limpiar campos
            cleaner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rout.setText("");
                    st.setText("");
                    date.setText("");
                    adapter.filteredList(schedules);
                }
            });

            //Busqueda
            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    filter(rout.getText().toString(), "", st.getText().toString(), date.getText().toString());
                }
            });

            schedules = SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.getSchedulesByCompany(idCompany);
            adapter = new SchedulesAdapter(ctx, schedules, this);
            recyclerView.setAdapter(adapter);
        }

    }

    //Mostrar el calendario
    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                int monthP = month + 1;
                String formattedMonth = "" + monthP;
                String formattedDayOfMonth = "" + day;

                if (monthP < 10) {

                    formattedMonth = "0" + monthP;
                }
                if (day < 10) {

                    formattedDayOfMonth = "0" + day;
                }

                date.setText(year + "-" + formattedMonth + "-" + formattedDayOfMonth);
            }
        });
        if(getSupportFragmentManager().findFragmentByTag("datePicker")==null){
            newFragment.show(getSupportFragmentManager(), "datePicker");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        floatingActionButton.setEnabled(true);
    }

    @Override
    public void onItemClick(ScheduleModel scheduleModel) {
        String units = scheduleModel.getUnidad();
        Intent i = new Intent(ctx, MapActivity.class);
        i.putExtra("Unidades", units);
        startActivity(i);
    }

    @Override
    public void onDetailClick(ScheduleModel scheduleModel) {
        String origin = scheduleModel.getOrigen();
        String destiny = scheduleModel.getDestino();
        String company = scheduleModel.getCompania();
        String drivers = scheduleModel.getConductor();
        String date = scheduleModel.getFecha();
        String hour = scheduleModel.getHora();
        String vehicles = scheduleModel.getUnidad();
        String product = scheduleModel.getProducto();
        String state = scheduleModel.getEstado();
        Intent i = new Intent(ctx, ScheduleDetail.class);
        i.putExtra("Origen", origin);
        i.putExtra("Destino", destiny);
        i.putExtra("Compañia", company);
        i.putExtra("Conductores", drivers);
        i.putExtra("Fecha", date);
        i.putExtra("Hora", hour);
        i.putExtra("Vehiculos", vehicles);
        i.putExtra("Producto", product);
        i.putExtra("Estado", state);
        startActivity(i);
    }

    @Override
    public void onEditClick(ScheduleModel scheduleModel) {
        String pk = String.valueOf(scheduleModel.getId_schedules());
        Intent i = new Intent(ctx,SchedulesEdit.class);
        i.putExtra("pk",pk);
        startActivity(i);
    }


    @SuppressLint("StaticFieldLeak")
    class GetCompanies extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ctx, "", "Cargando Compañias", true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH = createBasicAuth(username, password);
                Call<List<Company>> getCompany = api.getCompanies(AUTH);
                List<Company> getCompanies = getCompany.execute().body();
                if(!getCompanies.equals(null)){
                    if(getCompanies.size()>0){
                        for (Company company : getCompanies) {
                            companies.add(company.getParty());
                        }
                    }
                }else {
                    Toasty.error(ctx, "Error en recuperar compañías", Toast.LENGTH_LONG).show();
                }

            } catch (JsonIOException | IOException | NullPointerException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            dialog.dismiss();
            adapterCompanies = new ArrayAdapter<String>(ctx,
                    android.R.layout.simple_list_item_1, companies);
            compan.setAdapter(adapterCompanies);
            super.onPostExecute(s);
        }
    }


    //RUTAS

    @SuppressLint("StaticFieldLeak")
    class GetRoutes extends AsyncTask<Boolean, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ctx, "", "Cargando Datos", true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH = createBasicAuth(username, password);
                Call<List<Route>> getRoute = api.getRoutes(AUTH);
                List<Route> routes1 = getRoute.execute().body();
                if(!routes1.equals(null)){
                    if(routes1.size()>0){
                        for (Route route : routes1) {
                            routes.add(route.getOrigin_place().getProperties().getName() + " - " + route.getTarget_place().getProperties().getName());
                        }
                    }else {
                        String data = "No hay datos disponibles";
                        routes.add(data);
                    }
                }else {
                    Toasty.error(ctx,"Error en recuperación de rutas",Toast.LENGTH_LONG).show();
                }


            } catch (JsonIOException | IOException | NullPointerException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            adapterRoutes = new ArrayAdapter<String>(ctx,
                    android.R.layout.simple_list_item_1, routes);
            rout.setAdapter(adapterRoutes);
        }
    }


    public String createBasicAuth(String username, String password) {
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }

    public void filter(String r, String c, String e, String d) {
        ArrayList<ScheduleModel> schedulesList = new ArrayList<>();

        // Ruta, compañia, estado, fecha
        if (!r.isEmpty() && !c.isEmpty() && !d.isEmpty() && !e.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                String route = schedule.getOrigen().concat(" - " + schedule.getDestino());
                if (((schedule.getOrigen()).toLowerCase().equals(r.toLowerCase()) || (schedule.getDestino()).toLowerCase().equals(r.toLowerCase()) || route.toLowerCase().equals(r.toLowerCase())) &&
                        ((schedule.getCompania().toLowerCase().equals(c.toLowerCase()))) && (schedule.getEstado().toLowerCase().equals(e.toLowerCase())) && (schedule.getFecha().toLowerCase().equals(d.toLowerCase()))) {
                    schedulesList.add(schedule);
                }
            }
        }
        //Ruta, compañia,estado
        else if (!r.isEmpty() && !c.isEmpty() && !e.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                String route = schedule.getOrigen().concat(" - " + schedule.getDestino());
                if (((schedule.getOrigen()).toLowerCase().equals(r.toLowerCase()) || (schedule.getDestino()).toLowerCase().equals(r.toLowerCase()) || route.toLowerCase().equals(r.toLowerCase())) &&
                        ((schedule.getCompania().toLowerCase().equals(c.toLowerCase()))) && (schedule.getEstado().toLowerCase().equals(e.toLowerCase()))) {
                    schedulesList.add(schedule);
                }
            }
        }
        //Compañia,estado,fecha
        else if (!c.isEmpty() && !e.isEmpty() && !d.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                if (((schedule.getCompania().toLowerCase().equals(c.toLowerCase()))) && (schedule.getEstado().toLowerCase().equals(e.toLowerCase())) && (schedule.getFecha().toLowerCase().equals(d.toLowerCase()))) {
                    schedulesList.add(schedule);
                }
            }
        }
        //Ruta, compañia, fecha
        else if (!r.isEmpty() && !c.isEmpty() && !d.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                String route = schedule.getOrigen().concat(" - " + schedule.getDestino());
                if (((schedule.getOrigen()).toLowerCase().equals(r.toLowerCase()) || (schedule.getDestino()).toLowerCase().equals(r.toLowerCase()) || route.toLowerCase().equals(r.toLowerCase())) &&
                        ((schedule.getCompania().toLowerCase().equals(c.toLowerCase()))) && (schedule.getFecha().toLowerCase().equals(d.toLowerCase()))) {
                    schedulesList.add(schedule);
                }
            }
        }
        //Ruta, compañia
        else if (!r.isEmpty() && !c.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                String route = schedule.getOrigen().concat(" - " + schedule.getDestino());
                if ((schedule.getOrigen()).toLowerCase().equals(r.toLowerCase()) || (schedule.getDestino()).toLowerCase().equals(r.toLowerCase()) || route.toLowerCase().equals(r.toLowerCase()) &&
                        ((schedule.getCompania().toLowerCase().equals(c.toLowerCase())))) {
                    schedulesList.add(schedule);
                }
            }
        }
        //Ruta, fecha
        else if (!r.isEmpty() && !d.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                String route = schedule.getOrigen().concat(" - " + schedule.getDestino());
                if (((schedule.getOrigen()).toLowerCase().equals(r.toLowerCase()) || (schedule.getDestino()).toLowerCase().equals(r.toLowerCase()) || route.toLowerCase().equals(r.toLowerCase())) &&
                        (schedule.getFecha().toLowerCase().equals(d.toLowerCase()))) {
                    schedulesList.add(schedule);
                }
            }

        }
        //Ruta,estado
        else if (!r.isEmpty() && !e.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                String route = schedule.getOrigen().concat(" - " + schedule.getDestino());
                if ((schedule.getOrigen()).toLowerCase().equals(r.toLowerCase()) || (schedule.getDestino()).toLowerCase().equals(r.toLowerCase()) || route.toLowerCase().equals(r.toLowerCase()) &&
                        ((schedule.getEstado().toLowerCase().equals(e.toLowerCase())))) {
                    schedulesList.add(schedule);
                }
            }
        }
        //Compañia, fecha
        else if (!c.isEmpty() && !d.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                if ((schedule.getCompania().toLowerCase().equals(c.toLowerCase())) && ((schedule.getFecha().toLowerCase().equals(d.toLowerCase())))) {
                    schedulesList.add(schedule);
                }
            }

        }
        //Compañia, estado
        else if (!c.isEmpty() && !e.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                if ((schedule.getCompania().toLowerCase().equals(c.toLowerCase())) && ((schedule.getEstado().toLowerCase().equals(e.toLowerCase())))) {
                    schedulesList.add(schedule);
                }
            }
        }
        // Estado, fecha
        else if (!e.isEmpty() && !d.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                if (((schedule.getEstado().toLowerCase().equals(e.toLowerCase()))) && ((schedule.getFecha().toLowerCase().equals(d.toLowerCase())))) {
                    schedulesList.add(schedule);
                }
            }

        } else if (!r.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                String route = schedule.getOrigen().concat(" - " + schedule.getDestino());
                if ((schedule.getOrigen()).toLowerCase().equals(r.toLowerCase()) || (schedule.getDestino()).toLowerCase().equals(r.toLowerCase()) || route.toLowerCase().equals(r.toLowerCase())) {
                    schedulesList.add(schedule);
                }
            }
        } else if (!c.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                if ((schedule.getCompania().toLowerCase().equals(c.toLowerCase()))) {
                    schedulesList.add(schedule);
                }
            }
        } else if (!d.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                if ((schedule.getFecha().toLowerCase().equals(d.toLowerCase()))) {
                    schedulesList.add(schedule);
                }
            }
        } else if (!e.isEmpty()) {
            for (ScheduleModel schedule : schedules) {
                if ((schedule.getEstado().toLowerCase().equals(e.toLowerCase()))) {
                    schedulesList.add(schedule);
                }
            }
        }
        adapter.filteredList(schedulesList);

    }
}
