package com.connecttix.gpsgoldcar.Models;

public class ScheduleModel {
    private int ids;
    private int id_schedules;
    private String origen;
    private String destino;
    private String fecha;
    private String hora;
    private int id_compania;
    private String compania;
    private String conductor;
    private String unidad;
    private String producto;
    private String estado;

    public ScheduleModel(){}

    public ScheduleModel(int ids, int id_schedules, String origen, String destino, String fecha, String hora, int id_compania, String compania, String conductor, String unidad, String producto, String estado) {
        this.ids = ids;
        this.id_schedules = id_schedules;
        this.origen = origen;
        this.destino = destino;
        this.fecha = fecha;
        this.hora = hora;
        this.id_compania = id_compania;
        this.compania = compania;
        this.conductor = conductor;
        this.unidad = unidad;
        this.producto = producto;
        this.estado = estado;
    }
    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public int getId_schedules() {
        return id_schedules;
    }

    public void setId_schedules(int id_schedules) {
        this.id_schedules = id_schedules;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    public int getId_compania() { return id_compania;
    }

    public void setId_compania(int id_compania) { this.id_compania = id_compania;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
