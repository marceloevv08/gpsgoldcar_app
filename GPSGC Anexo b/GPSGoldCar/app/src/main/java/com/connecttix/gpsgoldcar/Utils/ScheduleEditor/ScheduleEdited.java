package com.connecttix.gpsgoldcar.Utils.ScheduleEditor;

import com.connecttix.gpsgoldcar.Utils.ScheduleCreator.RoutingDetails;

import java.util.List;

public class ScheduleEdited {
    public ScheduleEdited() {
    }

    private int route_id;
    private Programing programing;
    private String routing_date;
    private String routing_time;
    private String is_active;
    private List<RoutingDetails> routing_details;
    private String state;
    private int id ;


    public ScheduleEdited(int route_id, Programing programing, String routing_date, String routing_time, String is_active, List<RoutingDetails> routing_details, String state, int id) {
        this.route_id = route_id;
        this.programing = programing;
        this.routing_date = routing_date;
        this.routing_time = routing_time;
        this.is_active = is_active;
        this.routing_details = routing_details;
        this.state = state;
        this.id = id;
    }

    public int getRoute_id() {
        return route_id;
    }

    public void setRoute_id(int route_id) {
        this.route_id = route_id;
    }

    public Programing getPrograming() {
        return programing;
    }

    public void setPrograming(Programing programing) {
        this.programing = programing;
    }

    public String getRouting_date() {
        return routing_date;
    }

    public void setRouting_date(String routing_date) {
        this.routing_date = routing_date;
    }

    public String getRouting_time() {
        return routing_time;
    }

    public void setRouting_time(String routing_time) {
        this.routing_time = routing_time;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public List<RoutingDetails> getRouting_details() {
        return routing_details;
    }

    public void setRouting_details(List<RoutingDetails> routing_details) {
        this.routing_details = routing_details;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
