package com.connecttix.gpsgoldcar.Views.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.connecttix.gpsgoldcar.Config.ConstValue;
import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.DriverModel;
import com.connecttix.gpsgoldcar.Models.ScheduleModel;
import com.connecttix.gpsgoldcar.Models.StateModel;
import com.connecttix.gpsgoldcar.Models.VehicleModel;
import com.connecttix.gpsgoldcar.Network.CheckInternet;
import com.connecttix.gpsgoldcar.Network.Protocol;
import com.connecttix.gpsgoldcar.R;
import com.connecttix.gpsgoldcar.Utils.Company.Company;
import com.connecttix.gpsgoldcar.Utils.Drivers.DriversAll;
import com.connecttix.gpsgoldcar.Utils.Schedules_State.SchedulesStateAll;
import com.connecttix.gpsgoldcar.Utils.Vehicles.VehicleModelInterface;
import com.connecttix.gpsgoldcar.Utils.InterfaceAPI;
import com.connecttix.gpsgoldcar.Utils.LoginAuth.AuthClass;
import com.connecttix.gpsgoldcar.Utils.Retrofit.RetrofitClientInstance;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.Filter;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.Routing_details;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.ScheduleModelInterface;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class LoginActivity extends AppCompatActivity {
    Intent i;
    Context ctx = this;
    ProgressBar progressBar;
    Button btn_login;
    TextInputLayout ti_username, ti_password;
    EditText et_username, et_password;
    String username, password;
    Protocol protocol;
    com.connecttix.gpsgoldcar.Models.VehicleModel vehicleModel;
    com.connecttix.gpsgoldcar.Models.DriverModel driverModel;
    ScheduleModel scheduleModel;
    String url_drivers;
    CheckInternet checkInternet;
    StateModel stateModel;
    Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
    final InterfaceAPI api = retrofit.create(InterfaceAPI.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        protocol = new Protocol();
        checkInternet = new CheckInternet(ctx);
        btn_login = (Button) findViewById(R.id.bt_login);
        progressBar = (ProgressBar) findViewById(R.id.loading);
        ti_username = (TextInputLayout) findViewById(R.id.ti_username);
        ti_password = (TextInputLayout) findViewById(R.id.ti_password);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

        /** Mantener Sesión abierta **/
        SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
        String loged = sharedPref.getString("loged", "inactive");
        assert loged != null;
        if (loged.equals("active")) {
            Intent intent = new Intent(LoginActivity.this, Transition.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }

        et_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ti_username.setStartIconTintList(ContextCompat.getColorStateList(ctx, R.color.colorPrimary));
                } else {
                    ti_username.setStartIconTintList(ContextCompat.getColorStateList(ctx, R.color.gray2));
                }

            }
        });
        et_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ti_password.setStartIconTintList(ContextCompat.getColorStateList(ctx, R.color.colorPrimary));
                } else {
                    ti_password.setStartIconTintList(ContextCompat.getColorStateList(ctx, R.color.gray2));
                }
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_username.getText().length() > 0 && et_password.getText().length() > 0) {
                    if (checkInternet.isNetworkConnected()) {
                        try {
                            et_password.onEditorAction(EditorInfo.IME_ACTION_DONE);
                            new LoginTask().execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toasty.warning(ctx, "Su dispositivo no cuenta con conexión a internet", Toast.LENGTH_SHORT, true).show();
                    }
                } else {
                    Toasty.warning(ctx, "No ha ingresado un usuario o una contraseña", Toast.LENGTH_SHORT, true).show();
                }
            }
        });
    }

    public void checkLogin() throws JSONException {
        username = et_username.getText().toString();
        password = et_password.getText().toString();
        AuthClass authentication = new AuthClass(username, password);
        Call<AuthClass> call = api.checkLogin(authentication);
        call.enqueue(new Callback<AuthClass>() {
            @Override
            public void onResponse(@NonNull Call<AuthClass> call, @NonNull Response<AuthClass> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        assert response.body() != null;

                        int user_id = response.body().getUser_id();
                        int idCompany = response.body().getEmployee_company_id();
                        String user_first_name = response.body().getUser_first_name();
                        String user_last_name = response.body().getUser_last_name();
                        String user_email = response.body().getUser_email();
                        boolean user_is_staff = response.body().isUser_is_staff();
                        boolean isAdmin = response.body().isUser_is_super();
                        boolean employee_is_staff = response.body().isEmployee_is_staff();
                        String nameCompany = response.body().getEmployee_company_name();

                        @SuppressLint("HardwareIds") String uuid = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
                        SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("token", response.body().getToken());
                        editor.putInt("user_id", user_id);
                        editor.putString("username", username);
                        editor.putString("password", password);
                        editor.putString("user_first_name", user_first_name);
                        editor.putString("user_last_name", user_last_name);
                        editor.putString("user_email", user_email);
                        editor.putBoolean("user_is_staff", user_is_staff);
                        editor.putBoolean("user_is_super", isAdmin);
                        editor.putInt("employee_company_id", idCompany);
                        editor.putString("employee_company_name", nameCompany);
                        editor.putBoolean("employee_is_staff", employee_is_staff);
                        editor.putString("uuid", uuid);
                        editor.putString("loged", "active");
                        editor.apply();
                        Toasty.success(ctx, "Bienvenido", Toast.LENGTH_SHORT, true).show();
                        i = new Intent(ctx, Transition.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                        finish();

                    } else {
                        Toasty.warning(ctx, "Usuario y/o contraseña inválidos", Toast.LENGTH_SHORT, true).show();
                        et_username.setText("");
                        et_password.setText("");
                    }
                } else {
                    Toasty.warning(ctx, "Usuario y/o contraseña inválidos", Toast.LENGTH_SHORT, true).show();
                    et_username.setText("");
                    et_password.setText("");
                }
            }


            @Override
            public void onFailure(@NonNull Call<AuthClass> call, @NonNull Throwable t) {
                Log.e("TAG", t.toString());
                t.printStackTrace();
            }
        });
    }

    public String createBasicAuth(String username, String password) {
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }


    class LoginTask extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            username = et_username.getText().toString();
            password = et_password.getText().toString();
            try {
                /**Login */
                checkLogin();
                /**Consulta para almacenar conductores , vehículos, estados y programaciones*/
                JSONObject jsonLogin = new JSONObject();
                jsonLogin.put("username", username);
                jsonLogin.put("password", password);
                JSONObject response = protocol.postLogin(ConstValue.AUTH_LOGIN, jsonLogin);
                if (response.isNull("token")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en el servidor", Toast.LENGTH_SHORT, true).show();
                        }
                    });
                } else {
                    boolean isAdmin = response.getBoolean("user_is_super");
                    int idCompany = response.getInt("employee_company_id");
                    /**Obtenemos las Compañías*/
                    String AUTH1 = createBasicAuth(username, password);
                    Call<List<Company>> getCompanies = api.getCompanies(AUTH1);
                    List<Company> companies = getCompanies.execute().body();
                    /**Guardando Conductores*/
                    Call<List<DriversAll>> getDrivers = api.getAllDrivers(AUTH1);
                    List<DriversAll> driversList = getDrivers.execute().body();

                    if(driversList!=null) {
                        if (driversList.size() > 0) {
                            if (isAdmin) {
                                for (DriversAll driver : driversList) {
                                    driverModel = new DriverModel();
                                    driverModel.setId_driv(driver.getPk());
                                    if(companies!=null){
                                        if(companies.size()>0){
                                            for(Company company : companies){
                                                if(company.getPk()==driver.getParty_driver().getCompany()){
                                                    driverModel.setCompania(company.getParty());
                                                }
                                            }
                                        }
                                    }else {
                                        driverModel.setCompania("Sin asignar");
                                    }
                                    driverModel.setNombre(driver.getName());
                                    driverModel.setDni(driver.getParty_identifiers().get(0).getIdentifier_number());
                                    driverModel.setF_dni(driver.getParty_identifiers().get(0).getIdentifier_due_date());
                                    driverModel.setLic_con(driver.getParty_driver().getLicense_due_date());
                                    driverModel.setSctr(driver.getParty_driver().getDriver_anexo_b().getSctr());
                                    driverModel.setPas_med(driver.getParty_driver().getDriver_anexo_b().getMedical_pass());
                                    driverModel.setInduccion(driver.getParty_driver().getDriver_anexo_b().getInduction());
                                    driverModel.setFtcheck(driver.getParty_driver().getDriver_anexo_b().getPhoto_check());
                                    driverModel.setId_compania(driver.getParty_driver().getCompany());

                                    driverModel.setEstado(driver.getIs_active());
                                    SqliteClass.getInstance(ctx).databasehelp.appDriverSql.addDriver(driverModel);
                                }
                            } else {
                                for (DriversAll driver : driversList) {
                                    driverModel = new DriverModel();
                                    if (driver.getParty_driver().getCompany() == idCompany) {
                                        driverModel.setId_driv(driver.getPk());
                                        if(companies!=null){
                                            if(companies.size()>0){
                                                for(Company company : companies){
                                                    if(company.getPk()==driver.getParty_driver().getCompany()){
                                                        driverModel.setCompania(company.getParty());
                                                    }
                                                }
                                            }
                                        }else {
                                            driverModel.setCompania("Sin asignar");
                                        }
                                        driverModel.setNombre(driver.getName());
                                        driverModel.setDni(driver.getParty_identifiers().get(0).getIdentifier_number());
                                        driverModel.setF_dni(driver.getParty_identifiers().get(0).getIdentifier_due_date());
                                        driverModel.setLic_con(driver.getParty_driver().getLicense_due_date());
                                        driverModel.setSctr(driver.getParty_driver().getDriver_anexo_b().getSctr());
                                        driverModel.setPas_med(driver.getParty_driver().getDriver_anexo_b().getMedical_pass());
                                        driverModel.setInduccion(driver.getParty_driver().getDriver_anexo_b().getInduction());
                                        driverModel.setFtcheck(driver.getParty_driver().getDriver_anexo_b().getPhoto_check());
                                        driverModel.setId_compania(driver.getParty_driver().getCompany());
                                        driverModel.setEstado(driver.getIs_active());
                                        SqliteClass.getInstance(ctx).databasehelp.appDriverSql.addDriver(driverModel);
                                    }
                                }
                            }
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toasty.warning(ctx, "La empresa no contiene conductores", Toast.LENGTH_LONG, true).show();
                                }
                            });
                        }

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(ctx, "Error en recuperación de conductores", Toast.LENGTH_LONG, true).show();
                            }
                        });

                    }
                    /**Guardando Programaciones*/
                    Filter filter = new Filter();
                    filter.setAll_programming("true");
                    filter.setDate_end("");
                    filter.setDate_ini("");
                    filter.setPk_route("");
                    filter.setPk_state("");
                    if (isAdmin) {
                        filter.setPk_company("");
                    } else {
                        filter.setPk_company(String.valueOf(idCompany));
                    }
                    Call<List<ScheduleModelInterface>> getSchedules = api.getSchedules(AUTH1, filter);
                    List<ScheduleModelInterface> scheduleModelInterfaces = getSchedules.execute().body();
                    if (scheduleModelInterfaces != null) {
                        if (scheduleModelInterfaces.size() > 0) {
                            for (ScheduleModelInterface scheduleModelInterface : scheduleModelInterfaces) {
                                scheduleModel = new ScheduleModel();
                                scheduleModel.setId_schedules(scheduleModelInterface.getId());
                                if (scheduleModelInterface.getPrograming() != null) {
                                    scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                    scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                    scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                    scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                    scheduleModel.setId_compania(scheduleModelInterface.getPrograming().getCompany().getPk());
                                    scheduleModel.setCompania(scheduleModelInterface.getPrograming().getCompany().getParty());
                                    List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                    String[] conductores = new String[routing_details_list.size()];
                                    String[] unidades = new String[routing_details_list.size()];
                                    if (routing_details_list.size() > 0) {
                                        for (int i = 0; i < routing_details_list.size(); i++) {
                                            conductores[i] = routing_details_list.get(i).getDriver();
                                            unidades[i] = routing_details_list.get(i).getAvl_unit();
                                        }
                                        scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                        scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                    } else {
                                        scheduleModel.setConductor("Sin asignar");
                                        scheduleModel.setUnidad("Sin asignar");
                                    }
                                    scheduleModel.setProducto(scheduleModelInterface.getPrograming().getProduct().getName());
                                    scheduleModel.setEstado(scheduleModelInterface.getState());

                                } else {
                                    scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                    scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                    scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                    scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                    scheduleModel.setId_compania(0);
                                    scheduleModel.setCompania("Sin asignar");
                                    List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                    String[] conductores = new String[routing_details_list.size()];
                                    String[] unidades = new String[routing_details_list.size()];
                                    if (routing_details_list.size() > 0) {
                                        for (int i = 0; i < routing_details_list.size(); i++) {
                                            conductores[i] = routing_details_list.get(i).getDriver();
                                            unidades[i] = routing_details_list.get(i).getAvl_unit();
                                        }
                                        scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                        scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                    } else {
                                        scheduleModel.setConductor("Sin asignar");
                                        scheduleModel.setUnidad("Sin asignar");
                                    }
                                    scheduleModel.setProducto("Sin asignar");
                                    scheduleModel.setEstado(scheduleModelInterface.getState());

                                }
                                SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.addSchedule(scheduleModel);
                            }
                        }else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toasty.warning(ctx, "La empresa no contiene programaciones", Toast.LENGTH_LONG, true).show();
                                }
                            });
                        }

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(ctx, "Error en recuperación de programaciones", Toast.LENGTH_LONG, true).show();
                            }
                        });

                    }

                    /**
                     * Guardando Estados
                     */
                    String AUTH3 = createBasicAuth(username, password);
                    Call<List<SchedulesStateAll>> getSchedulesState = api.getSchedulesState(AUTH3);
                    List<SchedulesStateAll> schedulesStates = getSchedulesState.execute().body();
                    if (schedulesStates != null) {
                        if (schedulesStates.size() > 0) {
                            if (isAdmin) {
                                for (SchedulesStateAll scheduleState : schedulesStates) {
                                    stateModel = new StateModel();
                                    stateModel.setEnterprise(scheduleState.getEnterprise());
                                    stateModel.setId_enterprise(scheduleState.getId_enterprise());
                                    stateModel.setUnit(scheduleState.getUnit());
                                    stateModel.setOrigin(scheduleState.getOrigin());
                                    stateModel.setDestination(scheduleState.getDestination());
                                    stateModel.setState(scheduleState.getState());
                                    stateModel.setStart_date(scheduleState.getStar_date());
                                    stateModel.setEnd_date(scheduleState.getEnd_date());
                                    stateModel.setAdvance(scheduleState.getAdvance());
                                    SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);
                                }
                            } else {

                                for (SchedulesStateAll scheduleState : schedulesStates) {
                                    stateModel = new StateModel();
                                    if (scheduleState.getId_enterprise() == idCompany) {
                                        stateModel.setEnterprise(scheduleState.getEnterprise());
                                        stateModel.setId_enterprise(scheduleState.getId_enterprise());
                                        stateModel.setUnit(scheduleState.getUnit());
                                        stateModel.setOrigin(scheduleState.getOrigin());
                                        stateModel.setDestination(scheduleState.getDestination());
                                        stateModel.setState(scheduleState.getState());
                                        stateModel.setStart_date(scheduleState.getStar_date());
                                        stateModel.setEnd_date(scheduleState.getEnd_date());
                                        stateModel.setAdvance(scheduleState.getAdvance());
                                        SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);
                                    }
                                }

                            }

                        }else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toasty.warning(ctx, "La empresa no contiene estados", Toast.LENGTH_LONG, true).show();
                                }
                            });
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(ctx, "Error en recuperación de estados", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }

                    /**
                     * Guardando Vehiculos
                     **/
                    String AUTH = createBasicAuth(username, password);
                    Call<List<VehicleModelInterface>> getAllVehicles = api.getAllVehicles(AUTH);
                    List<VehicleModelInterface> vehicleModelInterfaceList = getAllVehicles.execute().body();

                    if (vehicleModelInterfaceList != null) {
                        if (vehicleModelInterfaceList.size() > 0) {
                            if (isAdmin) {
                                for (VehicleModelInterface vehicle : vehicleModelInterfaceList) {
                                    vehicleModel = new VehicleModel();
                                    vehicleModel.setId_vehicle(vehicle.getPk());
                                    vehicleModel.setId_company(vehicle.getCompany());
                                    if(companies!=null){
                                        if(companies.size()>0){
                                            for(Company company : companies){
                                                if(company.getPk()==vehicle.getCompany()){
                                                    vehicleModel.setName_company(company.getParty());
                                                }
                                            }
                                        }
                                    }else {
                                        vehicleModel.setName_company("Sin asignar");
                                    }
                                    vehicleModel.setEstado(vehicle.isIs_active());
                                    vehicleModel.setPlaca(vehicle.getAvl_unit().getAvl_unit_name());
                                    vehicleModel.setSoat(vehicle.getAvl_unit().getVehicle().getInsurance_soat());
                                    vehicleModel.setPoliza(vehicle.getAvl_unit().getVehicle().getInsurance_policy());
                                    vehicleModel.setRev_tec(vehicle.getAvl_unit().getVehicle().getTechnical_review());
                                    vehicleModel.setVig_contrato(vehicle.getAvl_unit().getVehicle().getContract_validity());
                                    vehicleModel.setGps(vehicle.getAvl_unit().getVehicle().getContrac_gps());
                                    SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.addVehicle(vehicleModel);
                                }
                            } else {
                                for (VehicleModelInterface vehicle : vehicleModelInterfaceList) {
                                    vehicleModel = new VehicleModel();
                                    vehicleModel.setId_vehicle(vehicle.getPk());
                                    int company = vehicle.getCompany();
                                    if (company == idCompany) {
                                        if(companies!=null){
                                            if(companies.size()>0){
                                                for(Company compan : companies){
                                                    if(compan.getPk()==vehicle.getCompany()){
                                                        vehicleModel.setName_company(compan.getParty());
                                                    }
                                                }
                                            }
                                        }else {
                                            vehicleModel.setName_company("Sin asignar");
                                        }
                                        vehicleModel.setEstado(vehicle.isIs_active());
                                        vehicleModel.setPlaca(vehicle.getAvl_unit().getAvl_unit_name());
                                        vehicleModel.setSoat(vehicle.getAvl_unit().getVehicle().getInsurance_soat());
                                        vehicleModel.setPoliza(vehicle.getAvl_unit().getVehicle().getInsurance_policy());
                                        vehicleModel.setRev_tec(vehicle.getAvl_unit().getVehicle().getTechnical_review());
                                        vehicleModel.setVig_contrato(vehicle.getAvl_unit().getVehicle().getContract_validity());
                                        vehicleModel.setGps(vehicle.getAvl_unit().getVehicle().getContrac_gps());
                                        SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.addVehicle(vehicleModel);
                                    }
                                }
                            }

                        }else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toasty.warning(ctx, "La empresa no contiene vehículos", Toast.LENGTH_LONG, true).show();
                                }
                            });
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.error(ctx, "Error en recuperación de vehículos", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.GONE);
            /** Activar el estado de sessión main **/
            SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("logueado", "active");
            editor.apply();
            super.onPostExecute(s);
        }
    }
}




