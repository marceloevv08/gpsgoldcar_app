package com.connecttix.gpsgoldcar.Utils;

public class AuthClass {

    private String token;
    private int user_id;
    private String username;
    private String user_first_name;
    private String user_last_name;
    private String user_email;
    private boolean user_is_staff;
    private boolean user_is_super;
    private int employee_company_id;
    private String employee_company_name;
    private boolean employee_is_staff;
    private String password;
    public AuthClass() {
    }

    public AuthClass(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_first_name() {
        return user_first_name;
    }

    public void setUser_first_name(String user_first_name) {
        this.user_first_name = user_first_name;
    }

    public String getUser_last_name() {
        return user_last_name;
    }

    public void setUser_last_name(String user_last_name) {
        this.user_last_name = user_last_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public boolean isUser_is_super() {
        return user_is_super;
    }

    public void setUser_is_staff(boolean user_is_staff) {
        this.user_is_super = user_is_super;
    }

    public int getEmployee_company_id() {
        return employee_company_id;
    }

    public void setEmployee_company_id(int employee_company_id) {
        this.employee_company_id = employee_company_id;
    }

    public String getEmployee_company_name() {
        return employee_company_name;
    }

    public void setEmployee_company_name(String employee_company_name) {
        this.employee_company_name = employee_company_name;
    }

    public boolean isEmployee_is_staff() {
        return employee_is_staff;
    }

    public void setEmployee_is_staff(boolean employee_is_staff) {
        this.employee_is_staff = employee_is_staff;
    }


    public boolean isUser_is_staff() {
        return user_is_staff;
    }

    public void setUser_is_super(boolean user_is_super) {
        this.user_is_super = user_is_super;
    }
}
