package com.connecttix.gpsgoldcar.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.StateModel;
import com.connecttix.gpsgoldcar.R;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

public class StatesAdapter extends RecyclerView.Adapter<StatesAdapter.ViewHolder> {
    Context ctx;
    List<StateModel> states;
    public StatesAdapter(Context ctx,List<StateModel> states){
        this.ctx= ctx;
        this.states=states;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View stateView = inflater.inflate(R.layout.states_table, parent,false);
        stateView.setBackgroundColor(Color.parseColor("#fafafa"));
        return new ViewHolder(stateView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(states.get(position));

    }

    @Override
    public int getItemCount() {
        return states.size();
    }

    public void filteredList(ArrayList<StateModel> stateList) {
        states = stateList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        MaterialCardView state_table;
        TextView v_e_state,v_u_state,v_o_state,v_d_state,v_st_state,v_fs_state,v_a_state,v_fl_state;
        ArrayList<StateModel> list;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            state_table= (MaterialCardView) itemView.findViewById(R.id.schedules_state_card);

            /** Valores**/
            v_e_state = (TextView) itemView.findViewById(R.id.s_company_state);
            v_u_state = (TextView) itemView.findViewById(R.id.s_unit_state);
            v_o_state = (TextView) itemView.findViewById(R.id.s_origin_state);
            v_d_state = (TextView) itemView.findViewById(R.id.s_destiny_state);
            v_st_state = (TextView) itemView.findViewById(R.id.s_state_st);
            v_fs_state = (TextView) itemView.findViewById(R.id.s_odate_state);
            v_a_state = (TextView) itemView.findViewById(R.id.s_advance_estado);
            v_fl_state = (TextView) itemView.findViewById(R.id.s_state_ardate);
        }

        @SuppressLint("SetTextI18n")
        public void bind(StateModel stateModel) {
            list = SqliteClass.getInstance(ctx).databasehelp.appStateSql.getAllStates();
            v_e_state.setText(stateModel.getEnterprise());
            v_u_state.setText(stateModel.getUnit());
            v_o_state.setText(stateModel.getOrigin());
            v_d_state.setText(stateModel.getDestination());
            v_st_state.setText(stateModel.getState());
            v_fs_state.setText(stateModel.getStart_date());
            v_a_state.setText(stateModel.getAdvance()+"%");
            v_fl_state.setText(stateModel.getEnd_date());

            switch (v_st_state.getText().toString()){
                case "PROGRAMED":
                    v_st_state.setBackgroundResource(R.drawable.programed_shape);
                    v_st_state.setTextColor(Color.WHITE);
                    break;
                case "CANCELED":
                    v_st_state.setBackgroundResource(R.drawable.canceled_shape);
                    v_st_state.setTextColor(Color.WHITE);
                    break;
                case "CONFIRMED":
                    v_st_state.setBackgroundResource(R.drawable.confirmed_shape);
                    v_st_state.setTextColor(Color.WHITE);
                    break;
                case "DELAYED":
                    v_st_state.setBackgroundResource(R.drawable.delayed_shape);
                    v_st_state.setTextColor(Color.WHITE);
                    break;
                case  "ON_ROUTE":
                    v_st_state.setBackgroundResource(R.drawable.onroute_shape);
                    v_st_state.setBackgroundColor(Color.parseColor("#343a40"));
                    v_st_state.setTextColor(Color.WHITE);
                    break;
                case "ARRIVED":
                    v_st_state.setBackgroundResource(R.drawable.arrived_shape);
                    v_st_state.setTextColor(Color.WHITE);
                    break;
                case "COMPLETED":
                    v_st_state.setBackgroundResource(R.drawable.completed_shape);
                    v_st_state.setTextColor(Color.WHITE);
                    break;

            }
        }
    }
}
