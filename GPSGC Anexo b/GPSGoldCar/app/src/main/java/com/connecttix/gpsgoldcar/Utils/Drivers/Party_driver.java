package com.connecttix.gpsgoldcar.Utils.Drivers;

public class Party_driver {
    private int pk;
    private String license_due_date;
    private Driver_anexo_b driver_anexo_b;
    private int company;

    public Party_driver(int pk, String license_due_date, Driver_anexo_b driver_anexo_b, int company) {
        this.pk = pk;
        this.license_due_date = license_due_date;
        this.driver_anexo_b = driver_anexo_b;
        this.company = company;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getLicense_due_date() {
        return license_due_date;
    }

    public void setLicense_due_date(String license_due_date) {
        this.license_due_date = license_due_date;
    }

    public Driver_anexo_b getDriver_anexo_b() {
        return driver_anexo_b;
    }

    public void setDriver_anexo_b(Driver_anexo_b driver_anexo_b) {
        this.driver_anexo_b = driver_anexo_b;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }
}
