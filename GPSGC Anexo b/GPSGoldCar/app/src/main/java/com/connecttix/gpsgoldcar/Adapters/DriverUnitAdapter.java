package com.connecttix.gpsgoldcar.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.connecttix.gpsgoldcar.Models.DriUnitModel;
import com.connecttix.gpsgoldcar.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DriverUnitAdapter extends RecyclerView.Adapter<DriverUnitAdapter.ViewHolder> {
    Context context;
    List<String> drivers, units;
    List<Integer> diverId, unitId;
    List<DriUnitModel> cardsDU;
    OnItemClickListener onItemClickListener;


    public DriverUnitAdapter(Context context, List<String> drivers, List<String> units, List<Integer> diverId, List<Integer> unitId, List<DriUnitModel> cardsDU, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.drivers = drivers;
        this.units = units;
        this.diverId = diverId;
        this.unitId = unitId;
        this.cardsDU = cardsDU;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View dri_uni = inflater.inflate(R.layout.driver_unit, parent, false);
        dri_uni.setBackgroundColor(Color.parseColor("#fafafa"));
        return new ViewHolder(dri_uni, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(cardsDU.get(position));
        holder.button.setEnabled(true);
    }


    @Override
    public int getItemCount() {
        return cardsDU.size();
    }


    public interface OnItemClickListener {
        void onDeleteClick(int position,DriUnitModel driUnitModel);
    }




    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        FloatingActionButton button;
        String drivername, unitname;
        int auxidDriver, auxidUnit, idDriver, idUnit;
        OnItemClickListener onItemClickListener;
        ArrayList<DriUnitModel> list;
        ArrayAdapter<String> adapterDrivers, adapterUnits;
        AutoCompleteTextView driver, unit;
        TextInputLayout ti_driver, ti_unit;




        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            this.onItemClickListener = onItemClickListener;
            //Driver
            ti_driver = (TextInputLayout) itemView.findViewById(R.id.driver_filter1);
            driver = (AutoCompleteTextView) itemView.findViewById(R.id.drivers1);
            adapterDrivers = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, drivers);

            driver.setAdapter(adapterDrivers);

            //Unit
            ti_unit = (TextInputLayout) itemView.findViewById(R.id.unit_filter1);
            unit = (AutoCompleteTextView) itemView.findViewById(R.id.units1);
            adapterUnits = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, units);
            unit.setAdapter(adapterUnits);
            //Eliminar cardview
            button = (FloatingActionButton) itemView.findViewById(R.id.deleteCard);

            button.setOnClickListener(this);
        }

        //Metodo para eliminar los forms de conductor-unidad
        @Override
        public void onClick(View view) {
            onItemClickListener.onDeleteClick(getAdapterPosition(),cardsDU.get(getAdapterPosition()));
            button.setEnabled(false);
            cleanFields();
        }

        //Seteamos la data necesaria
        public void bind(DriUnitModel driUnitModel) {
            list = new ArrayList(Arrays.asList(driUnitModel, driUnitModel));
            driver.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    drivername = adapterView.getItemAtPosition(i).toString();
                    if (drivers.size() == 1) {
                        return;
                    } else {
                        auxidDriver = (int) adapterView.getItemIdAtPosition(i);
                        idDriver = diverId.get(auxidDriver);
                        driUnitModel.setDriverpk(idDriver);
                        driUnitModel.setDriver(drivername);

                    }
                }
            });

            unit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    unitname = adapterView.getItemAtPosition(i).toString();
                    if (units.size() == 1) {
                        return;
                    } else {
                        auxidUnit = (int) adapterView.getItemIdAtPosition(i);
                        idUnit = unitId.get(auxidUnit);
                        driUnitModel.setUnitpk(idUnit);
                        driUnitModel.setUnit(unitname);

                    }
                }
            });
            validateFields();


        }
        public void cleanFields() {
            driver.setText("");
            unit.setText("");
            ti_driver.setErrorEnabled(false);
            ti_unit.setErrorEnabled(false);

        }


        public void validateFields() {
            for (int i = 0; i < cardsDU.size(); i++) {
                driver.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (driver.getText().toString().isEmpty()) {
                            ti_driver.setError("Seleccione un Conductor");
                        } else {
                            ti_driver.setErrorEnabled(false);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (driver.getText().toString().isEmpty()) {
                            ti_driver.setError("Seleccione un Conductor");
                        } else {
                            ti_driver.setErrorEnabled(false);
                        }
                    }
                });
                unit.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (unit.getText().toString().isEmpty()) {
                            ti_unit.setError("Seleccione una Unidad");
                        } else {
                            ti_unit.setErrorEnabled(false);

                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (unit.getText().toString().isEmpty()) {
                            ti_unit.setError("Seleccione una Unidad");
                        } else {
                            ti_unit.setErrorEnabled(false);

                        }
                    }
                });
            }


        }
    }


}
