package com.connecttix.gpsgoldcar.Utils.Drivers;

public class Driver_anexo_b {
    private int pk;
    private String sctr;
    private String medical_pass;
    private String induction;
    private String photo_check;

    public Driver_anexo_b(int pk, String sctr, String medical_pass, String induction, String photo_check) {
        this.pk = pk;
        this.sctr = sctr;
        this.medical_pass = medical_pass;
        this.induction = induction;
        this.photo_check = photo_check;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getSctr() {
        return sctr;
    }

    public void setSctr(String sctr) {
        this.sctr = sctr;
    }

    public String getMedical_pass() {
        return medical_pass;
    }

    public void setMedical_pass(String medical_pass) {
        this.medical_pass = medical_pass;
    }

    public String getInduction() {
        return induction;
    }

    public void setInduction(String induction) {
        this.induction = induction;
    }

    public String getPhoto_check() {
        return photo_check;
    }

    public void setPhoto_check(String photo_check) {
        this.photo_check = photo_check;
    }
}
