package com.connecttix.gpsgoldcar.Views.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.connecttix.gpsgoldcar.Config.ConstValue;
import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.DriverModel;
import com.connecttix.gpsgoldcar.Models.ScheduleModel;
import com.connecttix.gpsgoldcar.Models.StateModel;
import com.connecttix.gpsgoldcar.Models.VehicleModel;
import com.connecttix.gpsgoldcar.Network.CheckInternet;
import com.connecttix.gpsgoldcar.Network.Protocol;
import com.connecttix.gpsgoldcar.R;
import com.connecttix.gpsgoldcar.Utils.Company.Company;
import com.connecttix.gpsgoldcar.Utils.Drivers.DriversAll;
import com.connecttix.gpsgoldcar.Utils.InterfaceAPI;

import com.connecttix.gpsgoldcar.Utils.Retrofit.RetrofitClientInstance;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.Filter;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.Routing_details;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.ScheduleModelInterface;
import com.connecttix.gpsgoldcar.Utils.Schedules_State.SchedulesStateAll;
import com.connecttix.gpsgoldcar.Utils.Vehicles.VehicleModelInterface;
import com.connecttix.gpsgoldcar.Views.Activities.Drivers.DriversActivity;
import com.connecttix.gpsgoldcar.Views.Activities.Schedules.SchedulesActivity;
import com.connecttix.gpsgoldcar.Views.Activities.Vehicles.VehiclesActivity;
import com.connecttix.gpsgoldcar.Views.Dialogs.Dialogs;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import java.util.List;

import es.dmoral.toasty.Toasty;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, EasyPermissions.PermissionCallbacks {

    MaterialToolbar toolbar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Intent i;
    Context ctx;
    CardView schedules, state, drivers, vehicles1;
    VehicleModel vehicleModel;
    DriverModel driverModel;
    ScheduleModel scheduleModel;
    StateModel stateModel;
    CheckInternet checkInternet;
    Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
    final InterfaceAPI api = retrofit.create(InterfaceAPI.class);

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions();
        ctx = this;
        checkInternet = new CheckInternet(ctx);
        toolbar = (MaterialToolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.draw_lay);
        SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
        String username = sharedPref.getString("username", "");
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.bringToFront();
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.Usuario);
        navUsername.setText(username);
        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_d, R.string.close_d);
        drawerLayout.addDrawerListener(toogle);
        toogle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        schedules = (CardView) findViewById(R.id.listadom);
        state = (CardView) findViewById(R.id.estadom);
        drivers = (CardView) findViewById(R.id.driversm);
        vehicles1 = (CardView) findViewById(R.id.vehiclesm);
        schedules.setOnClickListener(this);
        state.setOnClickListener(this);
        drivers.setOnClickListener(this);
        vehicles1.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
        Toasty.warning(ctx, "No puede salir de la aplicación", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.map:
                i = new Intent(ctx,MapActivity.class);
                startActivity(i);
                break;*/
            case R.id.list:
                i = new Intent(ctx, SchedulesActivity.class);
                startActivity(i);
                navigationView.getMenu().findItem(R.id.state).setEnabled(false);
                navigationView.getMenu().findItem(R.id.drivers).setEnabled(false);
                navigationView.getMenu().findItem(R.id.vehicles).setEnabled(false);
                navigationView.getMenu().findItem(R.id.logout).setEnabled(false);
                navigationView.getMenu().findItem(R.id.update_data).setEnabled(false);
                return true;
            case R.id.state:
                i = new Intent(ctx, StatesActivity.class);
                startActivity(i);
                navigationView.getMenu().findItem(R.id.list).setEnabled(false);
                navigationView.getMenu().findItem(R.id.drivers).setEnabled(false);
                navigationView.getMenu().findItem(R.id.vehicles).setEnabled(false);
                navigationView.getMenu().findItem(R.id.logout).setEnabled(false);
                navigationView.getMenu().findItem(R.id.update_data).setEnabled(false);
                return true;
            case R.id.drivers:
                i = new Intent(ctx, DriversActivity.class);
                startActivity(i);
                navigationView.getMenu().findItem(R.id.state).setEnabled(false);
                navigationView.getMenu().findItem(R.id.list).setEnabled(false);
                navigationView.getMenu().findItem(R.id.vehicles).setEnabled(false);
                navigationView.getMenu().findItem(R.id.logout).setEnabled(false);
                navigationView.getMenu().findItem(R.id.update_data).setEnabled(false);
                return true;
            case R.id.logout:
                Dialogs.showLogoutDialog(MainActivity.this, ctx);
                return true;

            case R.id.vehicles:
                i = new Intent(ctx, VehiclesActivity.class);
                startActivity(i);
                navigationView.getMenu().findItem(R.id.state).setEnabled(false);
                navigationView.getMenu().findItem(R.id.drivers).setEnabled(false);
                navigationView.getMenu().findItem(R.id.list).setEnabled(false);
                navigationView.getMenu().findItem(R.id.logout).setEnabled(false);
                navigationView.getMenu().findItem(R.id.update_data).setEnabled(false);
                return true;
            case R.id.update_data:
                if (checkInternet.isNetworkConnected()) {
                    new updateTask().execute(true);
                } else {
                    Toasty.warning(ctx, "Su dispositivo no cuenta con conexión a Internet", Toast.LENGTH_LONG,true).show();
                }
                return true;

            default:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.listadom:
                schedules.setEnabled(false);
                state.setEnabled(false);
                drivers.setEnabled(false);
                vehicles1.setEnabled(false);
                i = new Intent(ctx, SchedulesActivity.class);
                startActivity(i);
                break;
            case R.id.estadom:
                schedules.setEnabled(false);
                state.setEnabled(false);
                drivers.setEnabled(false);
                vehicles1.setEnabled(false);
                i = new Intent(ctx, StatesActivity.class);
                startActivity(i);
                break;
            case R.id.driversm:
                schedules.setEnabled(false);
                state.setEnabled(false);
                drivers.setEnabled(false);
                vehicles1.setEnabled(false);
                i = new Intent(ctx, DriversActivity.class);
                startActivity(i);
                break;
            case R.id.vehiclesm:
                schedules.setEnabled(false);
                state.setEnabled(false);
                drivers.setEnabled(false);
                vehicles1.setEnabled(false);
                i = new Intent(ctx, VehiclesActivity.class);
                startActivity(i);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        schedules.setEnabled(true);
        state.setEnabled(true);
        drivers.setEnabled(true);
        vehicles1.setEnabled(true);
        navigationView.getMenu().findItem(R.id.list).setEnabled(true);
        navigationView.getMenu().findItem(R.id.state).setEnabled(true);
        navigationView.getMenu().findItem(R.id.drivers).setEnabled(true);
        navigationView.getMenu().findItem(R.id.vehicles).setEnabled(true);
        navigationView.getMenu().findItem(R.id.logout).setEnabled(true);
        navigationView.getMenu().findItem(R.id.update_data).setEnabled(true);
    }

    public void requestPermissions(){
        String[] perms={
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q){
            if(EasyPermissions.hasPermissions(this,perms)) {

            }
            else {
                EasyPermissions.requestPermissions(this,"Necesita permisos para continuar",123,perms);
            }
        }else {
            EasyPermissions.hasPermissions(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            );
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this,perms)){
            new AppSettingsDialog.Builder(this).build().show();
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint("StaticFieldLeak")
    class updateTask extends AsyncTask<Boolean, Void, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.deleteAppVehicleTable();
            SqliteClass.getInstance(ctx).databasehelp.appDriverSql.deleteAppDriverTable();
            SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.deleteAppScheduleTable();
            SqliteClass.getInstance(ctx).databasehelp.appStateSql.deleteAppStateTable();
            dialog = ProgressDialog.show(ctx, "", getString(R.string.action_loading), true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Intent intent = new Intent(ctx, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            finish();
            startActivity(intent);
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
            String username = sharedPref.getString("username", "");
            String password = sharedPref.getString("password", "");
            boolean isAdmin = sharedPref.getBoolean("user_is_super", true);
            int idCompany = sharedPref.getInt("employee_company_id", 0);
            try {
              /**Obtenemos compañias*/
                String AUTH = createBasicAuth(username, password);
                Call<List<Company>> getCompanies = api.getCompanies(AUTH);
                List<Company> companies = getCompanies.execute().body();
                /** Actualizando vehiculos**/
                Call<List<VehicleModelInterface>> getAllVehicles = api.getAllVehicles(AUTH);
                List<VehicleModelInterface> vehicleModelInterfaceList = getAllVehicles.execute().body();

                if (vehicleModelInterfaceList != null) {
                    if (vehicleModelInterfaceList.size() > 0) {
                        if (isAdmin) {
                            for (VehicleModelInterface vehicle : vehicleModelInterfaceList) {
                                vehicleModel = new VehicleModel();
                                vehicleModel.setId_vehicle(vehicle.getPk());
                                vehicleModel.setId_company(vehicle.getCompany());
                                if(companies!=null){
                                    if(companies.size()>0){
                                        for(Company company : companies){
                                            if(company.getPk()==vehicle.getCompany()){
                                                vehicleModel.setName_company(company.getParty());
                                            }
                                        }
                                    }
                                }else {
                                    vehicleModel.setName_company("Sin asignar");
                                }
                                vehicleModel.setEstado(vehicle.isIs_active());
                                vehicleModel.setPlaca(vehicle.getAvl_unit().getAvl_unit_name());
                                vehicleModel.setSoat(vehicle.getAvl_unit().getVehicle().getInsurance_soat());
                                vehicleModel.setPoliza(vehicle.getAvl_unit().getVehicle().getInsurance_policy());
                                vehicleModel.setRev_tec(vehicle.getAvl_unit().getVehicle().getTechnical_review());
                                vehicleModel.setVig_contrato(vehicle.getAvl_unit().getVehicle().getContract_validity());
                                vehicleModel.setGps(vehicle.getAvl_unit().getVehicle().getContrac_gps());
                                SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.addVehicle(vehicleModel);
                            }
                        } else {
                            for (VehicleModelInterface vehicle : vehicleModelInterfaceList) {
                                vehicleModel = new VehicleModel();
                                vehicleModel.setId_vehicle(vehicle.getPk());
                                int company = vehicle.getCompany();
                                if (company == idCompany) {
                                    if(companies!=null){
                                        if(companies.size()>0){
                                            for(Company compan : companies){
                                                if(compan.getPk()==vehicle.getCompany()){
                                                    vehicleModel.setName_company(compan.getParty());
                                                }
                                            }
                                        }
                                    }else {
                                        vehicleModel.setName_company("Sin asignar");
                                    }
                                    vehicleModel.setEstado(vehicle.isIs_active());
                                    vehicleModel.setPlaca(vehicle.getAvl_unit().getAvl_unit_name());
                                    vehicleModel.setSoat(vehicle.getAvl_unit().getVehicle().getInsurance_soat());
                                    vehicleModel.setPoliza(vehicle.getAvl_unit().getVehicle().getInsurance_policy());
                                    vehicleModel.setRev_tec(vehicle.getAvl_unit().getVehicle().getTechnical_review());
                                    vehicleModel.setVig_contrato(vehicle.getAvl_unit().getVehicle().getContract_validity());
                                    vehicleModel.setGps(vehicle.getAvl_unit().getVehicle().getContrac_gps());
                                    SqliteClass.getInstance(ctx).databasehelp.appVehicleSql.addVehicle(vehicleModel);
                                }
                            }
                        }

                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.warning(ctx, "La empresa no contiene vehículos", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en recuperación de vehículos", Toast.LENGTH_LONG, true).show();
                        }
                    });
                }
                /** Actualizando conductores**/
                String AUTH1 = createBasicAuth(username, password);
                Call<List<DriversAll>> getDrivers = api.getAllDrivers(AUTH1);
                List<DriversAll> driversList = getDrivers.execute().body();

                if(driversList!=null) {
                    if (driversList.size() > 0) {
                        if (isAdmin) {
                            for (DriversAll driver : driversList) {
                                driverModel = new DriverModel();
                                driverModel.setId_driv(driver.getPk());
                                if(companies!=null){
                                    if(companies.size()>0){
                                        for(Company company : companies){
                                            if(company.getPk()==driver.getParty_driver().getCompany()){
                                                driverModel.setCompania(company.getParty());
                                            }
                                        }
                                    }
                                }else {
                                    driverModel.setCompania("Sin asignar");
                                }
                                driverModel.setNombre(driver.getName());
                                driverModel.setDni(driver.getParty_identifiers().get(0).getIdentifier_number());
                                driverModel.setF_dni(driver.getParty_identifiers().get(0).getIdentifier_due_date());
                                driverModel.setLic_con(driver.getParty_driver().getLicense_due_date());
                                driverModel.setSctr(driver.getParty_driver().getDriver_anexo_b().getSctr());
                                driverModel.setPas_med(driver.getParty_driver().getDriver_anexo_b().getMedical_pass());
                                driverModel.setInduccion(driver.getParty_driver().getDriver_anexo_b().getInduction());
                                driverModel.setFtcheck(driver.getParty_driver().getDriver_anexo_b().getPhoto_check());
                                driverModel.setId_compania(driver.getParty_driver().getCompany());

                                driverModel.setEstado(driver.getIs_active());
                                SqliteClass.getInstance(ctx).databasehelp.appDriverSql.addDriver(driverModel);
                            }
                        } else {
                            for (DriversAll driver : driversList) {
                                driverModel = new DriverModel();
                                if (driver.getParty_driver().getCompany() == idCompany) {
                                    driverModel.setId_driv(driver.getPk());
                                    if(companies!=null){
                                        if(companies.size()>0){
                                            for(Company company : companies){
                                                if(company.getPk()==driver.getParty_driver().getCompany()){
                                                    driverModel.setCompania(company.getParty());
                                                }
                                            }
                                        }
                                    }else {
                                        driverModel.setCompania("Sin asignar");
                                    }
                                    driverModel.setNombre(driver.getName());
                                    driverModel.setDni(driver.getParty_identifiers().get(0).getIdentifier_number());
                                    driverModel.setF_dni(driver.getParty_identifiers().get(0).getIdentifier_due_date());
                                    driverModel.setLic_con(driver.getParty_driver().getLicense_due_date());
                                    driverModel.setSctr(driver.getParty_driver().getDriver_anexo_b().getSctr());
                                    driverModel.setPas_med(driver.getParty_driver().getDriver_anexo_b().getMedical_pass());
                                    driverModel.setInduccion(driver.getParty_driver().getDriver_anexo_b().getInduction());
                                    driverModel.setFtcheck(driver.getParty_driver().getDriver_anexo_b().getPhoto_check());
                                    driverModel.setId_compania(driver.getParty_driver().getCompany());
                                    driverModel.setEstado(driver.getIs_active());
                                    SqliteClass.getInstance(ctx).databasehelp.appDriverSql.addDriver(driverModel);
                                }
                            }
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.warning(ctx, "La empresa no contiene conductores", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en recuperación de conductores", Toast.LENGTH_LONG, true).show();
                        }
                    });

                }

                /** Actualizando schedules**/
                Filter filter = new Filter();
                filter.setAll_programming("true");
                filter.setDate_end("");
                filter.setDate_ini("");
                filter.setPk_route("");
                filter.setPk_state("");
                if (isAdmin) {
                    filter.setPk_company("");
                } else {
                    filter.setPk_company(String.valueOf(idCompany));
                }
                Call<List<ScheduleModelInterface>> getSchedules = api.getSchedules(AUTH1, filter);
                List<ScheduleModelInterface> scheduleModelInterfaces = getSchedules.execute().body();
                if (scheduleModelInterfaces != null) {
                    if (scheduleModelInterfaces.size() > 0) {
                        for (ScheduleModelInterface scheduleModelInterface : scheduleModelInterfaces) {
                            scheduleModel = new ScheduleModel();
                            scheduleModel.setId_schedules(scheduleModelInterface.getId());
                            if (scheduleModelInterface.getPrograming() != null) {
                                scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                scheduleModel.setId_compania(scheduleModelInterface.getPrograming().getCompany().getPk());
                                scheduleModel.setCompania(scheduleModelInterface.getPrograming().getCompany().getParty());
                                List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                String[] conductores = new String[routing_details_list.size()];
                                String[] unidades = new String[routing_details_list.size()];
                                if (routing_details_list.size() > 0) {
                                    for (int i = 0; i < routing_details_list.size(); i++) {
                                        conductores[i] = routing_details_list.get(i).getDriver();
                                        unidades[i] = routing_details_list.get(i).getAvl_unit();
                                    }
                                    scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                    scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                } else {
                                    scheduleModel.setConductor("Sin asignar");
                                    scheduleModel.setUnidad("Sin asignar");
                                }
                                scheduleModel.setProducto(scheduleModelInterface.getPrograming().getProduct().getName());
                                scheduleModel.setEstado(scheduleModelInterface.getState());

                            } else {
                                scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                scheduleModel.setId_compania(0);
                                scheduleModel.setCompania("Sin asignar");
                                List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                String[] conductores = new String[routing_details_list.size()];
                                String[] unidades = new String[routing_details_list.size()];
                                if (routing_details_list.size() > 0) {
                                    for (int i = 0; i < routing_details_list.size(); i++) {
                                        conductores[i] = routing_details_list.get(i).getDriver();
                                        unidades[i] = routing_details_list.get(i).getAvl_unit();
                                    }
                                    scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                    scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                } else {
                                    scheduleModel.setConductor("Sin asignar");
                                    scheduleModel.setUnidad("Sin asignar");
                                }
                                scheduleModel.setProducto("Sin asignar");
                                scheduleModel.setEstado(scheduleModelInterface.getState());

                            }
                            SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.addSchedule(scheduleModel);
                        }
                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.warning(ctx, "La empresa no contiene programaciones", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en recuperación de programaciones", Toast.LENGTH_LONG, true).show();
                        }
                    });

                }
                /**Actualizando Estados*/
                String AUTH3 = createBasicAuth(username, password);
                Call<List<SchedulesStateAll>> getSchedulesState = api.getSchedulesState(AUTH3);
                List<SchedulesStateAll> schedulesStates = getSchedulesState.execute().body();
                if (schedulesStates != null) {
                    if (schedulesStates.size() > 0) {
                        if (isAdmin) {
                            for (SchedulesStateAll scheduleState : schedulesStates) {
                                stateModel = new StateModel();
                                stateModel.setEnterprise(scheduleState.getEnterprise());
                                stateModel.setId_enterprise(scheduleState.getId_enterprise());
                                stateModel.setUnit(scheduleState.getUnit());
                                stateModel.setOrigin(scheduleState.getOrigin());
                                stateModel.setDestination(scheduleState.getDestination());
                                stateModel.setState(scheduleState.getState());
                                stateModel.setStart_date(scheduleState.getStar_date());
                                stateModel.setEnd_date(scheduleState.getEnd_date());
                                stateModel.setAdvance(scheduleState.getAdvance());
                                SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);
                            }
                        } else {

                            for (SchedulesStateAll scheduleState : schedulesStates) {
                                stateModel = new StateModel();
                                if (scheduleState.getId_enterprise() == idCompany) {
                                    stateModel.setEnterprise(scheduleState.getEnterprise());
                                    stateModel.setId_enterprise(scheduleState.getId_enterprise());
                                    stateModel.setUnit(scheduleState.getUnit());
                                    stateModel.setOrigin(scheduleState.getOrigin());
                                    stateModel.setDestination(scheduleState.getDestination());
                                    stateModel.setState(scheduleState.getState());
                                    stateModel.setStart_date(scheduleState.getStar_date());
                                    stateModel.setEnd_date(scheduleState.getEnd_date());
                                    stateModel.setAdvance(scheduleState.getAdvance());
                                    SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);
                                }
                            }

                        }

                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.warning(ctx, "La empresa no contiene estados", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en recuperación de estados", Toast.LENGTH_LONG, true).show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String createBasicAuth(String username, String password) {
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }
}