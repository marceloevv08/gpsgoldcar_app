package com.connecttix.gpsgoldcar.Network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class Protocol {
    public JSONObject postLogin(String url, JSONObject postData){
        BufferedReader reader = null;
        String result="";
        JSONObject objReturn = null;
        HttpURLConnection httpURLConnection =null;
        try {
            httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Accept","application/json");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
            os.writeBytes(postData.toString());
            os.flush();
            InputStream stream = httpURLConnection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");

            }
            os.close();
            stream.close();
            result = buffer.toString();
            objReturn = new JSONObject(result);
            httpURLConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return objReturn;

    }
    public JSONArray postJson(String url, JSONObject postData){
        BufferedReader reader = null;
        String result="";
        JSONArray objReturn = null;
        HttpURLConnection httpURLConnection =null;
        try {
            httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Accept","application/json");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            DataOutputStream os = new DataOutputStream(httpURLConnection.getOutputStream());
            os.writeBytes(postData.toString());
            os.flush();
            InputStream stream = httpURLConnection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");

            }
            os.close();
            stream.close();
            result = buffer.toString();
            objReturn = new JSONArray(result);

            httpURLConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return objReturn;

    }


    public String getJson(String url,String token){
        String result="";
        HttpURLConnection httpURLConnection = null;
        try {
            URL u = new URL(url);
            httpURLConnection = (HttpURLConnection) u.openConnection();
            httpURLConnection.setRequestMethod("GET");

            if(!token.equals("default")){
                httpURLConnection.setRequestProperty("Authorization", token); /** Aqui va al token **/
            }
            httpURLConnection.setRequestProperty("Content-length", "0");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setAllowUserInteraction(false);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode()==200){
                BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();
                result = sb.toString();
            }else {
                JSONArray jsonArray = new JSONArray();
                result=jsonArray.toString();
            }
            httpURLConnection.disconnect();


        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;

    }

}
