package com.connecttix.gpsgoldcar.Views.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.connecttix.gpsgoldcar.Adapters.StatesAdapter;
import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.StateModel;
import com.connecttix.gpsgoldcar.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textfield.TextInputLayout;
import java.util.ArrayList;
import java.util.Objects;

public class StatesActivity extends AppCompatActivity {
    Context ctx =this;
    MaterialToolbar toolbar;
    RecyclerView recyclerView;
    ArrayList<StateModel> states;
    EditText campo;
    StatesAdapter adapter;
    TextInputLayout filtersearch;
    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_states);
        toolbar = (MaterialToolbar) findViewById(R.id.toolbarstate);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_state);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        states = SqliteClass.getInstance(ctx).databasehelp.appStateSql.getAllStates();

        adapter = new StatesAdapter(ctx,states);
        recyclerView.setAdapter(adapter);
        filtersearch = (TextInputLayout)findViewById(R.id.search_filter_state);
        campo = (EditText) findViewById(R.id.campo);
        campo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void afterTextChanged(Editable s) {
                filterByCampo(s.toString());
            }
        });
    }
    private void filterByCampo(String s) {
        ArrayList<StateModel> stateList = new ArrayList<>();
        for(StateModel state: states){
            if(state.getEnterprise().toLowerCase().contains(s.toLowerCase())|| (state.getUnit().toLowerCase().contains(s.toLowerCase()))
            || (state.getOrigin().toLowerCase().contains(s.toLowerCase())) || (state.getDestination().toLowerCase().contains(s.toLowerCase()))
            || (state.getState().toLowerCase().contains(s.toLowerCase())) || state.getStart_date().toLowerCase().contains(s.toLowerCase())
            || (state.getAdvance().toLowerCase().contains(s.toLowerCase())) || (state.getEnd_date().toLowerCase().contains(s.toLowerCase()))){
                stateList.add(state);
            }
        }
        adapter.filteredList(stateList);
    }
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
}
