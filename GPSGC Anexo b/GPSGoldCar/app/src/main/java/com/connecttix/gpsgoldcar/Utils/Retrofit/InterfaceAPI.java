package com.connecttix.gpsgoldcar.Utils;
import com.connecttix.gpsgoldcar.Utils.Binnacle.BinnacleModel;
import com.connecttix.gpsgoldcar.Utils.Company.Company;
import com.connecttix.gpsgoldcar.Utils.Driver_Filtered.DriverFiltered;
import com.connecttix.gpsgoldcar.Utils.Drivers.DriversAll;
import com.connecttix.gpsgoldcar.Utils.Schedules_State.SchedulesStateAll;
import com.connecttix.gpsgoldcar.Utils.Vehicles.VehicleModelInterface;
import com.connecttix.gpsgoldcar.Utils.LoginAuth.AuthClass;
import com.connecttix.gpsgoldcar.Utils.ModalUnit.Geofence;
import com.connecttix.gpsgoldcar.Utils.ModalUnit.Point;
import com.connecttix.gpsgoldcar.Utils.ModalUnit.UnitPosition;
import com.connecttix.gpsgoldcar.Utils.Product.Product;
import com.connecttix.gpsgoldcar.Utils.Routing_details.Route;
import com.connecttix.gpsgoldcar.Utils.Routing_details.AvlUnitRouting;
import com.connecttix.gpsgoldcar.Utils.ScheduleCreator.NewSchedule;
import com.connecttix.gpsgoldcar.Utils.ScheduleEditor.ScheduleEdited;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.Filter;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.ScheduleModelInterface;
import com.connecttix.gpsgoldcar.Utils.Vehicles_Filtered.VehicleFiltered;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

public interface InterfaceAPI {
    //Login
    @POST("/api/authentication/login/")
    Call<AuthClass> checkLogin(@Body AuthClass body);

    //Obtenemos Conductores
    @GET("api/anexob/drivers/all/")
    Call<List<DriversAll>> getAllDrivers(@Header ("Authorization") String auth);

    //Obtenemos Vehiculos
    @GET("api/anexob/vehicles/")
    Call<List<VehicleModelInterface>> getAllVehicles(@Header ("Authorization") String auth);

    //Obtener Programaciones
    @POST("api/anexob/programings_filter/")
    Call<List<ScheduleModelInterface>> getSchedules(@Header ("Authorization") String auth, @Body Filter body);

    //Obtenemos el estado de las Programaciones
    @GET("api/anexob/stateprogramings/")
    Call<List<SchedulesStateAll>> getSchedulesState(@Header ("Authorization") String auth);

    //Obtener última posición de los vehiculos
    @GET("/api/avls/last_position/")
    Call<List<UnitPosition>> getLastPositions(@Header ("Authorization") String auth);

    //Obtener Geozonas segun unidad
    @POST("/api/anexob/avl_in_geofence/")
    Call<Geofence> getGeofenceByPosition(@Header ("Authorization") String auth , @Body Point body);

    //Obtener RoutingDetails
    @GET
    Call<AvlUnitRouting> getDataRouting(@Url String url, @Header ("Authorization") String auth);

    //Obtener Productos
    @GET("/api/anexob/products/")
    Call<List<Product>> getProducts(@Header ("Authorization") String auth);

    //Crear nuevos Productos
    @POST("/api/anexob/products/")
    Call<Product> createProduct(@Header ("Authorization") String auth ,@Body Product body);

    //Crear nuevas Programaciones
    @POST("/api/anexob/programings/")
    Call<NewSchedule> createSchedule(@Header ("Authorization") String auth ,@Body NewSchedule body);

    //Obtener rutas de la empresa
    @GET("/api/avls/routes/")
    Call <List<Route>> getRoutes(@Header ("Authorization") String auth );

    //Obtener Compañias
    @GET("/api/companies/companies/")
    Call<List<Company>> getCompanies(@Header ("Authorization") String auth);

    //Obtener Conductores Filtrados
    @GET("/api/anexob/driver_filtered/")
    Call<List<DriverFiltered>> getDrivers(@Header ("Authorization") String auth);

    //Obtener Vehiculos Filtrados
    @GET("/api/anexob/vehicle_filtered/")
    Call<List<VehicleFiltered>> getVehicles(@Header ("Authorization") String auth);

    //Editar Programaciones
    //Obtenemos la programacion a editar
    @GET
    Call<ScheduleModelInterface> getSchedule(@Url String url,  @Header ("Authorization") String auth);

    //Eviamos los datos editados
    @PUT
    Call<ScheduleModelInterface> updateSchedule(@Url String url, @Header ("Authorization") String auth, @Body ScheduleEdited body);

    @POST("api/anexob/bitacora_history/")
    Call<BinnacleModel> postBinaccle(@Header ("Authorization") String auth , @Body BinnacleModel body);



}
