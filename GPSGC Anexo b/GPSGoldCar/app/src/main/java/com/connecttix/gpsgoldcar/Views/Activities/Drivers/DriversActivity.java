package com.connecttix.gpsgoldcar.Views.Activities.Drivers;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.connecttix.gpsgoldcar.Adapters.DriversAdapter;
import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.DriverModel;
import com.connecttix.gpsgoldcar.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textfield.TextInputLayout;
import java.util.ArrayList;
import java.util.Objects;


public class DriversActivity extends AppCompatActivity implements DriversAdapter.OnItemClickListener {
    Context ctx =this;
    MaterialToolbar toolbar;
    RecyclerView recyclerView;
    ArrayList<DriverModel> drivers;
    EditText nombrec;
    DriversAdapter adapter;
    RadioGroup typeFilter;
    RadioButton f_name,f_dni;
    TextInputLayout filtersearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drivers);
        toolbar = (MaterialToolbar) findViewById(R.id.toolbard);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_driver);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        drivers = SqliteClass.getInstance(ctx).databasehelp.appDriverSql.getAllDrivers();
        adapter = new DriversAdapter(ctx,drivers,this);
        recyclerView.setAdapter(adapter);
        typeFilter = findViewById(R.id.radioGroup);
        f_name = findViewById(R.id.radio_button_1);
        f_dni = findViewById(R.id.radio_button_2);
        filtersearch = (TextInputLayout)findViewById(R.id.search_filter);
        nombrec = (EditText) findViewById(R.id.nombrec);
        nombrec.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void afterTextChanged(Editable s) {
                filterByName(s.toString());
            }
        });
    }
    private void filterByName(String s) {
        ArrayList<DriverModel> driversList = new ArrayList<>();
        for(DriverModel driver: drivers){
            if(driver.getNombre().toLowerCase().contains(s.toLowerCase())){
                driversList.add(driver);
            }
        }
        adapter.filteredList(driversList);
    }
    private void filterByDNi(String s) {
        ArrayList<DriverModel> driversList = new ArrayList<>();
        for(DriverModel driver: drivers){
            if(driver.getDni().toLowerCase().contains(s.toLowerCase())){
                driversList.add(driver);
            }
        }
        adapter.filteredList(driversList);
    }
    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }
    public void onRadioButtonClicked (View view){
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()){
            case R.id.radio_button_1:
                if(checked){
                    filtersearch.setHint("Ingrese Nombre");
                    nombrec.setInputType(InputType.TYPE_CLASS_TEXT);
                    nombrec.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                        @Override
                        public void afterTextChanged(Editable s) {
                            filterByName(s.toString());
                        }
                    });
                }
                break;
            case R.id.radio_button_2:
                if(checked){
                    filtersearch.setHint("Ingrese DNI");
                    nombrec.setInputType(InputType.TYPE_CLASS_NUMBER);
                    nombrec.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
                        @Override
                        public void afterTextChanged(Editable s) {
                            filterByDNi(s.toString());
                        }
                    });
                }
                break;
        }
    }

    @Override
    public void onItemClick(DriverModel driverModel) {
        String name = driverModel.getNombre();
        String dni = driverModel.getDni();
        String company = driverModel.getCompania();
        String fdni = driverModel.getF_dni();
        String licon = driverModel.getLic_con();
        String ind = driverModel.getInduccion();
        String ftck = driverModel.getFtcheck();
        String sctr = driverModel.getSctr();
        String medpass= driverModel.getPas_med();
        String state = driverModel.getEstado();
        Intent i = new Intent(this, DriverDetail.class);
        i.putExtra("Name",name);
        i.putExtra("Dni",dni);
        i.putExtra("Company",company);
        i.putExtra("Fdni",fdni);
        i.putExtra("Licon",licon);
        i.putExtra("Ind",ind);
        i.putExtra("Ftchk",ftck);
        i.putExtra("Sctr",sctr);
        i.putExtra("Medpass",medpass);
        i.putExtra("State",state);
        startActivity(i);
    }
}
