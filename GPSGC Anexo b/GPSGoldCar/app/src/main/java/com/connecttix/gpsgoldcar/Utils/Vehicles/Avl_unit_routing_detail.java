package com.connecttix.gpsgoldcar.Utils.Vehicles;

public class Avl_unit_routing_detail {
    private Routing routing;

    public Avl_unit_routing_detail(Routing routing) {
        this.routing = routing;
    }

    public Routing getRouting() {
        return routing;
    }

    public void setRouting(Routing routing) {
        this.routing = routing;
    }
}
