package com.connecttix.gpsgoldcar.Utils.Vehicles;

public class Routing {
    private String departure_time;
    private String time_to_arrive;
    private int route;

    public Routing(String departure_time, String time_to_arrive, int route) {
        this.departure_time = departure_time;
        this.time_to_arrive = time_to_arrive;
        this.route = route;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public String getTime_to_arrive() {
        return time_to_arrive;
    }

    public void setTime_to_arrive(String time_to_arrive) {
        this.time_to_arrive = time_to_arrive;
    }

    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }
}
