package com.connecttix.gpsgoldcar.Utils.SchedulesModel;

public class Target_Place {
    private int id;
    private String name;

    public Target_Place(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Target_Place() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
