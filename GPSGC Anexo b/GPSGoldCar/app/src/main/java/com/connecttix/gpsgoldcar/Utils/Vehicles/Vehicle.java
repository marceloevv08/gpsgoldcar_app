package com.connecttix.gpsgoldcar.Utils.Vehicles;

public class Vehicle {
    private int pk;
    private String insurance_soat;
    private String insurance_policy;
    private String technical_review;
    private String contract_validity;
    private String contrac_gps;

    public Vehicle(int pk, String insurance_soat, String insurance_policy, String technical_review, String contract_validity, String contrac_gps) {
        this.pk = pk;
        this.insurance_soat = insurance_soat;
        this.insurance_policy = insurance_policy;
        this.technical_review = technical_review;
        this.contract_validity = contract_validity;
        this.contrac_gps = contrac_gps;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getInsurance_soat() {
        return insurance_soat;
    }

    public void setInsurance_soat(String insurance_soat) {
        this.insurance_soat = insurance_soat;
    }

    public String getInsurance_policy() {
        return insurance_policy;
    }

    public void setInsurance_policy(String insurance_policy) {
        this.insurance_policy = insurance_policy;
    }

    public String getTechnical_review() {
        return technical_review;
    }

    public void setTechnical_review(String technical_review) {
        this.technical_review = technical_review;
    }

    public String getContract_validity() {
        return contract_validity;
    }

    public void setContract_validity(String contract_validity) {
        this.contract_validity = contract_validity;
    }

    public String getContrac_gps() {
        return contrac_gps;
    }

    public void setContrac_gps(String contrac_gps) {
        this.contrac_gps = contrac_gps;
    }
}
