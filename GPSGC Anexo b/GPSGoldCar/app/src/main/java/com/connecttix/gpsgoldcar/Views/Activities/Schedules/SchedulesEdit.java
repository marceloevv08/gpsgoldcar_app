package com.connecttix.gpsgoldcar.Views.Activities.Schedules;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.connecttix.gpsgoldcar.Adapters.DriverUnitEditAdapter;
import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Models.DriUnitModel;
import com.connecttix.gpsgoldcar.Models.ScheduleModel;
import com.connecttix.gpsgoldcar.Models.StateModel;
import com.connecttix.gpsgoldcar.Network.CheckInternet;
import com.connecttix.gpsgoldcar.R;
import com.connecttix.gpsgoldcar.Utils.Binnacle.BinnacleModel;
import com.connecttix.gpsgoldcar.Utils.Binnacle.Values;
import com.connecttix.gpsgoldcar.Utils.Driver_Filtered.DriverFiltered;
import com.connecttix.gpsgoldcar.Utils.InterfaceAPI;
import com.connecttix.gpsgoldcar.Utils.Retrofit.RetrofitClientInstance;
import com.connecttix.gpsgoldcar.Utils.ScheduleCreator.RoutingDetails;
import com.connecttix.gpsgoldcar.Utils.ScheduleEditor.Programing;
import com.connecttix.gpsgoldcar.Utils.ScheduleEditor.ScheduleEdited;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.Filter;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.Routing_details;
import com.connecttix.gpsgoldcar.Utils.SchedulesModel.ScheduleModelInterface;
import com.connecttix.gpsgoldcar.Utils.Schedules_State.SchedulesStateAll;
import com.connecttix.gpsgoldcar.Utils.Vehicles_Filtered.VehicleFiltered;
import com.connecttix.gpsgoldcar.Views.Activities.MainActivity;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonIOException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SchedulesEdit extends AppCompatActivity implements DriverUnitEditAdapter.OnItemClickListener {
    NestedScrollView createForm;
    TextInputLayout ti_date, ti_hour, ti_route, ti_product;
    EditText date, hour;
    ArrayList<String> drivers, units;
    ArrayList<Integer> driverId, unitId;
    List<DriUnitModel> cardsDU;
    RecyclerView recyclerView;
    DriverUnitEditAdapter adapter;
    AutoCompleteTextView route, product;
    Context ctx = this;
    MaterialToolbar toolbar;
    FloatingActionButton addDUcard;
    MaterialButton edit, cancel;
    Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();
    final InterfaceAPI api = retrofit.create(InterfaceAPI.class);
    String pk, geofence;
    int product_id,ruta_id;
    List<Values> pastDriverValues = new ArrayList<>();
    List<Values> pastUnitValues = new ArrayList<>();
    List<Values> newDriverValues = new ArrayList<>();
    List<Values> newUnitValues = new ArrayList<>();
    // Update data
    //Update data
    ScheduleModel scheduleModel;
    StateModel stateModel;
    CheckInternet checkInternet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_edit);
        pk = getIntent().getStringExtra("pk");
        //Update states
        checkInternet = new CheckInternet(ctx);
        //Barra de navegación
        toolbar = (MaterialToolbar) findViewById(R.id.toolbarschcre);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //Formulario completo
        createForm = (NestedScrollView) findViewById(R.id.creatorForm);
        //Fecha
        ti_date = (TextInputLayout) findViewById(R.id.date_filter);
        date = (EditText) findViewById(R.id.et_date);
        //Hora
        ti_hour = (TextInputLayout) findViewById(R.id.hour_filter);
        hour = (EditText) findViewById(R.id.et_hour);
        //Ruta
        ti_route = (TextInputLayout) findViewById(R.id.route_filter);
        route = (AutoCompleteTextView) findViewById(R.id.rutas);
        //Producto
        ti_product = (TextInputLayout) findViewById(R.id.product_filter);
        product = (AutoCompleteTextView) findViewById(R.id.productos);

        //Recycler
        recyclerView = (RecyclerView) findViewById(R.id.recycler_dri_unit);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        //Declaro las listas para la data
        cardsDU = new ArrayList<DriUnitModel>();
        GetSchedule getSchedule = new GetSchedule();
        getSchedule.execute();
        drivers = new ArrayList<String>();
        driverId = new ArrayList<>();
        GetDrivers getDrivers = new GetDrivers();
        getDrivers.execute();
        units = new ArrayList<String>();
        unitId = new ArrayList<>();
        GetUnits getUnits = new GetUnits();
        getUnits.execute();

        //Boton para añadir Cardviews
        addDUcard = (FloatingActionButton) findViewById(R.id.addCard);
        addDUcard.bringToFront();
        addDUcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItem(new DriUnitModel());
            }
        });


        // Crear y cancelar
        edit = (MaterialButton) findViewById(R.id.createSchedule);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //validamos que no esten vacíos para crearlos
                if (validateEmptyFields()) {
                    if(checkInternet.isNetworkConnected()){
                        editSchedule();
                    }else {
                        Toasty.warning(ctx, "Su dispositivo no cuenta con conexión a Internet", Toast.LENGTH_LONG,true).show();
                    }


                } else {
                    createForm.scrollTo(0, 0);
                    Toasty.error(ctx, "No hay conductores y unidades en su formulario", Toast.LENGTH_SHORT).show();
                }

            }
        });
        cancel = (MaterialButton) findViewById(R.id.cancelSchedule);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onDeleteClick(int position, DriUnitModel driUnitModel) {
        cardsDU.remove(driUnitModel);
        adapter.notifyItemRemoved(position);
    }

    // Método para añadir el formulario conductor-unidad
    public void addItem(DriUnitModel driUnitModel) {
        cardsDU.add(driUnitModel);
        adapter.notifyItemInserted(cardsDU.size() + 1);
    }

    //Metodo para crear la autenticación
    public String createBasicAuth(String username, String password) {
        return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
    }


    //Validar campos para la creacion
    public boolean validateEmptyFields() {
        boolean isValid = true;
        // Formulario Unit-Driver
        if (cardsDU.size() == 0) {
            isValid = false;
        }

        return isValid;
    }

    public void editSchedule() {
        View view = getLayoutInflater().inflate(R.layout.modal_change_reason, null);
        EditText description = view.findViewById(R.id.ti_reason_description);

        //Creo el modal
        MaterialAlertDialogBuilder reasonModal = new MaterialAlertDialogBuilder(ctx, R.style.BinnacleModal)
                .setView(view)
                .setCancelable(false)
                .setPositiveButton("Enviar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                            String username = sharedPref.getString("username", "");
                            String password = sharedPref.getString("password", "");
                            int user_id = sharedPref.getInt("user_id",0);
                            String AUTH = createBasicAuth(username, password);
                            //PUT for update schecule
                            String routing_date = date.getText().toString();
                            String routing_time = hour.getText().toString()+ ":00";
                            String is_active = "true";
                            String state = "PROGRAMED";
                            //Para conductores y unidades
                            List<RoutingDetails> routing_detailsList = new ArrayList<RoutingDetails>();
                            int idDriver,idUnit;
                            String nameDriver, nameUnit;
                            boolean isRDvalid = true;

                            for (int i = 0; i < cardsDU.size(); i++) {
                                //Driver
                                if (cardsDU.get(i).getDriver() != null ) {
                                    if (cardsDU.get(i).getUnit() != null) {
                                        idDriver = cardsDU.get(i).getDriverpk();
                                        nameDriver = cardsDU.get(i).getDriver();
                                        //Unit
                                        idUnit = cardsDU.get(i).getUnitpk();
                                        nameUnit = cardsDU.get(i).getUnit();
                                        RoutingDetails routingDetailsobj = new RoutingDetails(nameDriver, nameUnit, idUnit, idDriver);
                                        routing_detailsList.add(routingDetailsobj);
                                        isRDvalid=true;

                                    } else {
                                        Toasty.error(ctx, "Data de Vehículos inválida", Toast.LENGTH_SHORT, true).show();
                                        isRDvalid=false;
                                    }

                                } else {
                                    Toasty.error(ctx, "Data de Conductores inválida", Toast.LENGTH_SHORT, true).show();
                                    isRDvalid= false;
                                }
                                Values newDriver = new Values("",cardsDU.get(i).getDriver());
                                Values newUnit = new Values("",cardsDU.get(i).getUnit());
                                newDriverValues.add(newDriver);
                                newUnitValues.add(newUnit);

                            }
                            boolean validateEmptyData = validateEmptyFieldsRouting(routing_detailsList);
                            boolean validateDuplicatedData = validateDuplicatedData(routing_detailsList);
                            if(isRDvalid){
                                if(validateDuplicatedData && validateEmptyData){
                                    Programing programing = new Programing(product_id,geofence);
                                    ScheduleEdited scheduleEdited = new ScheduleEdited(ruta_id,programing, routing_date, routing_time, is_active, routing_detailsList, state, Integer.parseInt(pk));
                                    Call<ScheduleModelInterface> updateSchedule = api.updateSchedule("http://anexob.gpsgoldcar.com/api/anexob/programings/" + pk+"/", AUTH, scheduleEdited);
                                    updateSchedule.enqueue(new Callback<ScheduleModelInterface>() {
                                        @Override
                                        public void onResponse(Call<ScheduleModelInterface> call, Response<ScheduleModelInterface> response) {
                                            if(response.isSuccessful()){
                                                Toasty.success(ctx, "Registro editado correctamente", Toast.LENGTH_SHORT).show();
                                            }else {
                                                Toasty.error(ctx, "Error en edición del registro", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        @Override
                                        public void onFailure(Call<ScheduleModelInterface> call, Throwable t) {

                                        }
                                    });
                                }


                                List<Values> values = new ArrayList<>();

                                //Posting the binnacle
                                //Necesito el instante exacto
                                Date today = Calendar.getInstance().getTime();
                                @SuppressLint("SimpleDateFormat")
                                SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                String todayString = formatdate.format(today);
                                for(int i=0; i<cardsDU.size();i++){
                                    Values drivers = new Values (pastDriverValues.get(i).getValue_past(),newDriverValues.get(i).getValue_new());
                                    Values units = new Values(pastUnitValues.get(i).getValue_past(),newUnitValues.get(i).getValue_new());
                                    if(pastDriverValues.get(i).getValue_past().equals(newDriverValues.get(i).getValue_new())){
                                        Toasty.warning(ctx, "No hay cambios en conductores", Toast.LENGTH_SHORT).show();
                                        values.add(units);
                                    }else if(pastUnitValues.get(i).getValue_past().equals(newUnitValues.get(i).getValue_new()) ){
                                        Toasty.warning(ctx, "No hay cambios en unidades", Toast.LENGTH_SHORT).show();
                                        values.add(drivers);
                                    }
                                    else {

                                        values.add(drivers);
                                        values.add(units);
                                    }
                                }
                                BinnacleModel binnacleModel = new BinnacleModel(Integer.parseInt(pk), description.getText().toString(), todayString,user_id,values);
                                Call<BinnacleModel> createBinnacle = api.postBinaccle(AUTH,binnacleModel);
                                createBinnacle.enqueue(new Callback<BinnacleModel>() {
                                    @Override
                                    public void onResponse(@NonNull Call<BinnacleModel> call, @NonNull Response<BinnacleModel> response) {
                                        if(response.isSuccessful()){
                                            Toasty.success(ctx, "Bitácora registrada", Toast.LENGTH_SHORT).show();
                                        }else {
                                            Toasty.error(ctx, "Error en registro de bitácora", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(@NonNull Call<BinnacleModel> call, @NonNull Throwable t) {
                                        t.printStackTrace();
                                    }
                                });
                            }
                            //Tengo que actualizar la data nuevamente
                            new updateTask().execute(true);
                            onBackPressed();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
        reasonModal.show();


    }

    //Obtenemos la data de la schedule
    class GetSchedule extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;
        String hora, fecha, ruta, producto;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(ctx, "", "Cargando Datos", true);
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH = createBasicAuth(username, password);
                Call<ScheduleModelInterface> getSchedule = api.getSchedule("http://anexob.gpsgoldcar.com/api/anexob/programings/" + pk, AUTH);
                if (getSchedule != null) {
                    ScheduleModelInterface schedule = getSchedule.execute().body();
                    List<Routing_details> routing_details = schedule.getRouting_details();
                    //Lista reciclable del formulario conductor-unidad
                    for (int i = 0; i < routing_details.size(); i++) {
                        int idDriver = routing_details.get(i).getDriver_id();
                        String Driver = routing_details.get(i).getDriver();
                        int idUnit = routing_details.get(i).getAvl_unit_id();
                        String Unit = routing_details.get(i).getAvl_unit();
                        DriUnitModel driUnitModel = new DriUnitModel(idDriver, Driver, Unit, idUnit);
                        Values driver = new Values(Driver,"");
                        Values unit = new Values(Unit,"");
                        cardsDU.add(driUnitModel);
                        pastDriverValues.add(driver);
                        pastUnitValues.add(unit);
                    }
                    fecha = schedule.getRouting_date();
                    hora = schedule.getRouting_time();
                    ruta = schedule.getRoute().getOrigin_place().getName() + " - " + schedule.getRoute().getTarget_place().getName();
                    ruta_id = schedule.getRoute().getID();
                    producto = schedule.getPrograming().getProduct().getName();
                    product_id = schedule.getPrograming().getProduct().getPk();
                    geofence = schedule.getPrograming().getGeofence();
                } else {
                    Toasty.error(ctx, "Error en la recuperación de los datos", Toast.LENGTH_SHORT, true).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            date.setText(fecha);
            date.setEnabled(false);
            hour.setText(hora);
            hour.setEnabled(false);
            route.setText(ruta);
            route.setEnabled(false);
            product.setText(producto);
            product.setEnabled(false);
            //Creamos los recycler view
            adapter = new DriverUnitEditAdapter(ctx, drivers, units, driverId, unitId, cardsDU, SchedulesEdit.this::onDeleteClick);
            recyclerView.setAdapter(adapter);
        }
    }


    //Obtenemos los conductores
    class GetDrivers extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ctx, "", "Cargando Datos", true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH2 = createBasicAuth(username, password);
                Call<List<DriverFiltered>> getDriversFiltered = api.getDrivers(AUTH2);
                List<DriverFiltered> getDF = getDriversFiltered.execute().body();
                if (!getDF.equals(null)) {
                    if (getDF.size() > 0) {
                        for (DriverFiltered driverFiltered : getDF) {
                            drivers.add(driverFiltered.getFields().getName());
                            driverId.add(driverFiltered.getPk());
                        }
                    } else {
                        String data = "No hay datos disponibles";
                        drivers.add(data);
                    }
                } else {
                    Toasty.error(ctx, "Error en la recuperación de Conductores", Toast.LENGTH_SHORT,true).show();

                }

            } catch (JsonIOException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
        }
    }

    //Obtenemos las unidades
    class GetUnits extends AsyncTask<Boolean, Void, String> {
        Dialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ctx, "", "Cargando Datos", true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            try {
                SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                String username = sharedPref.getString("username", "");
                String password = sharedPref.getString("password", "");
                String AUTH3 = createBasicAuth(username, password);
                Call<List<VehicleFiltered>> getVF = api.getVehicles(AUTH3);
                List<VehicleFiltered> vehicleFiltereds = getVF.execute().body();
                if (!vehicleFiltereds.equals(null)) {
                    if (vehicleFiltereds.size() > 0) {
                        for (VehicleFiltered vehicleFiltered : vehicleFiltereds) {
                            units.add(vehicleFiltered.getFields().getAvl_unit_name());
                            unitId.add(vehicleFiltered.getPk());
                        }
                    } else {
                        String data = "No hay datos disponibles";
                        units.add(data);
                    }

                } else {
                    Toasty.error(ctx, "Error en la recuperación de Vehículos", Toast.LENGTH_SHORT,true).show();

                }

            } catch (JsonIOException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
        }
    }

    public boolean validateEmptyFieldsRouting(List<RoutingDetails> routing_detailsList) {
        boolean isValid = true;
        for (int i = 0; i < routing_detailsList.size(); i++) {
            if (routing_detailsList.get(i).getDriver().equals("")) {
                Toasty.error(ctx, "Datos de Conductores Vacía", Toast.LENGTH_SHORT, true).show();
                isValid = false;
            } else if (routing_detailsList.get(i).getAvl_unit().equals("")) {
                Toasty.error(ctx, "Datos de Vechículos Vacía", Toast.LENGTH_SHORT, true).show();
                isValid = false;
            } else {
                isValid = true;
            }

        }
        return isValid;
    }

    public boolean validateDuplicatedData(List<RoutingDetails> routing_detailsList) {
        boolean isValid = true;
        for (int i = 0; i < routing_detailsList.size(); i++) {
            for (int j = i + 1; j < routing_detailsList.size(); j++) {
                if (routing_detailsList.get(i).getDriver_id() == routing_detailsList.get(j).getDriver_id()) {
                    Toasty.error(ctx, "Datos de Conductores Repetidos", Toast.LENGTH_SHORT, true).show();
                    isValid = false;
                } else if (routing_detailsList.get(i).getAvl_unit_id() == routing_detailsList.get(j).getAvl_unit_id()) {
                    Toasty.error(ctx, "Datos de Vechículos Repetidos", Toast.LENGTH_SHORT, true).show();
                    isValid = false;
                } else {
                    isValid = true;
                }
            }
        }
        return isValid;
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    class updateTask extends AsyncTask<Boolean, Void, String> {

        @Override
        protected void onPreExecute() {
            SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.deleteAppScheduleTable();
            SqliteClass.getInstance(ctx).databasehelp.appStateSql.deleteAppStateTable();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            Intent intent = new Intent(ctx, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            finish();
            startActivity(intent);
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(Boolean... booleans) {
            SharedPreferences sharedPref = getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
            String username = sharedPref.getString("username", "");
            String password = sharedPref.getString("password", "");
            String AUTH1 = createBasicAuth(username, password);
            boolean isAdmin = sharedPref.getBoolean("user_is_super", true);
            int idCompany = sharedPref.getInt("employee_company_id", 0);
            try {

                /** Actualizando schedules**/
                Filter filter = new Filter();
                filter.setAll_programming("true");
                filter.setDate_end("");
                filter.setDate_ini("");
                filter.setPk_route("");
                filter.setPk_state("");
                if (isAdmin) {
                    filter.setPk_company("");
                } else {
                    filter.setPk_company(String.valueOf(idCompany));
                }
                Call<List<ScheduleModelInterface>> getSchedules = api.getSchedules(AUTH1, filter);
                List<ScheduleModelInterface> scheduleModelInterfaces = getSchedules.execute().body();
                if (scheduleModelInterfaces != null) {
                    if (scheduleModelInterfaces.size() > 0) {
                        for (ScheduleModelInterface scheduleModelInterface : scheduleModelInterfaces) {
                            scheduleModel = new ScheduleModel();
                            scheduleModel.setId_schedules(scheduleModelInterface.getId());
                            if (scheduleModelInterface.getPrograming() != null) {
                                scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                scheduleModel.setId_compania(scheduleModelInterface.getPrograming().getCompany().getPk());
                                scheduleModel.setCompania(scheduleModelInterface.getPrograming().getCompany().getParty());
                                List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                String[] conductores = new String[routing_details_list.size()];
                                String[] unidades = new String[routing_details_list.size()];
                                if (routing_details_list.size() > 0) {
                                    for (int i = 0; i < routing_details_list.size(); i++) {
                                        conductores[i] = routing_details_list.get(i).getDriver();
                                        unidades[i] = routing_details_list.get(i).getAvl_unit();
                                    }
                                    scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                    scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                } else {
                                    scheduleModel.setConductor("Sin asignar");
                                    scheduleModel.setUnidad("Sin asignar");
                                }
                                scheduleModel.setProducto(scheduleModelInterface.getPrograming().getProduct().getName());
                                scheduleModel.setEstado(scheduleModelInterface.getState());

                            } else {
                                scheduleModel.setOrigen(scheduleModelInterface.getRoute().getOrigin_place().getName());
                                scheduleModel.setDestino(scheduleModelInterface.getRoute().getTarget_place().getName());
                                scheduleModel.setFecha(scheduleModelInterface.getRouting_date());
                                scheduleModel.setHora(scheduleModelInterface.getRouting_time());
                                scheduleModel.setId_compania(0);
                                scheduleModel.setCompania("Sin asignar");
                                List<Routing_details> routing_details_list = scheduleModelInterface.getRouting_details();
                                String[] conductores = new String[routing_details_list.size()];
                                String[] unidades = new String[routing_details_list.size()];
                                if (routing_details_list.size() > 0) {
                                    for (int i = 0; i < routing_details_list.size(); i++) {
                                        conductores[i] = routing_details_list.get(i).getDriver();
                                        unidades[i] = routing_details_list.get(i).getAvl_unit();
                                    }
                                    scheduleModel.setConductor(TextUtils.join("\n", conductores));
                                    scheduleModel.setUnidad(TextUtils.join("\n", unidades));
                                } else {
                                    scheduleModel.setConductor("Sin asignar");
                                    scheduleModel.setUnidad("Sin asignar");
                                }
                                scheduleModel.setProducto("Sin asignar");
                                scheduleModel.setEstado(scheduleModelInterface.getState());

                            }
                            SqliteClass.getInstance(ctx).databasehelp.appScheduleSql.addSchedule(scheduleModel);
                        }
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en recuperación de programaciones o el usuario no contiene programaciones", Toast.LENGTH_LONG, true).show();
                        }
                    });

                }
                /** Actualizando estados**/

                String AUTH3 = createBasicAuth(username, password);
                Call<List<SchedulesStateAll>> getSchedulesState = api.getSchedulesState(AUTH3);
                List<SchedulesStateAll> schedulesStates = getSchedulesState.execute().body();
                if (schedulesStates != null) {
                    if (schedulesStates.size() > 0) {
                        if (isAdmin) {
                            for (SchedulesStateAll scheduleState : schedulesStates) {
                                stateModel = new StateModel();
                                stateModel.setEnterprise(scheduleState.getEnterprise());
                                stateModel.setId_enterprise(scheduleState.getId_enterprise());
                                stateModel.setUnit(scheduleState.getUnit());
                                stateModel.setOrigin(scheduleState.getOrigin());
                                stateModel.setDestination(scheduleState.getDestination());
                                stateModel.setState(scheduleState.getState());
                                stateModel.setStart_date(scheduleState.getStar_date());
                                stateModel.setEnd_date(scheduleState.getEnd_date());
                                stateModel.setAdvance(scheduleState.getAdvance());
                                SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);
                            }
                        } else {

                            for (SchedulesStateAll scheduleState : schedulesStates) {
                                stateModel = new StateModel();
                                if (scheduleState.getId_enterprise() == idCompany) {
                                    stateModel.setEnterprise(scheduleState.getEnterprise());
                                    stateModel.setId_enterprise(scheduleState.getId_enterprise());
                                    stateModel.setUnit(scheduleState.getUnit());
                                    stateModel.setOrigin(scheduleState.getOrigin());
                                    stateModel.setDestination(scheduleState.getDestination());
                                    stateModel.setState(scheduleState.getState());
                                    stateModel.setStart_date(scheduleState.getStar_date());
                                    stateModel.setEnd_date(scheduleState.getEnd_date());
                                    stateModel.setAdvance(scheduleState.getAdvance());
                                    SqliteClass.getInstance(ctx).databasehelp.appStateSql.addState(stateModel);
                                }
                            }

                        }

                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toasty.warning(ctx, "La empresa no contiene estados", Toast.LENGTH_LONG, true).show();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toasty.error(ctx, "Error en recuperación de estados", Toast.LENGTH_LONG, true).show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
