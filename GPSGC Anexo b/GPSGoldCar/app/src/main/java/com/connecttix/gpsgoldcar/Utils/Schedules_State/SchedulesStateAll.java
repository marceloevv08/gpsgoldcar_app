package com.connecttix.gpsgoldcar.Utils.Schedules_State;

public class SchedulesStateAll {
    private String enterprise;
    private int id_enterprise;
    private String unit;
    private String origin;
    private String destination;
    private String state;
    private String star_date;
    private String end_date;
    private String advance;

    public SchedulesStateAll(String enterprise, int id_enterprise, String unit, String origin, String destination, String state, String star_date, String end_date, String advance) {
        this.enterprise = enterprise;
        this.id_enterprise = id_enterprise;
        this.unit = unit;
        this.origin = origin;
        this.destination = destination;
        this.state = state;
        this.star_date = star_date;
        this.end_date = end_date;
        this.advance = advance;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public int getId_enterprise() {
        return id_enterprise;
    }

    public void setId_enterprise(int id_enterprise) {
        this.id_enterprise = id_enterprise;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStar_date() {
        return star_date;
    }

    public void setStar_date(String star_date) {
        this.star_date = star_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getAdvance() {
        return advance;
    }

    public void setAdvance(String advance) {
        this.advance = advance;
    }
}
