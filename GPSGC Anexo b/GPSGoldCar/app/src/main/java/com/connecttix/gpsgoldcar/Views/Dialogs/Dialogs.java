package com.connecttix.gpsgoldcar.Views.Dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;
import com.connecttix.gpsgoldcar.Database.SqliteClass;
import com.connecttix.gpsgoldcar.Views.Activities.LoginActivity;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import es.dmoral.toasty.Toasty;

public class Dialogs {
    public static void showLogoutDialog(Activity activity, Context context){

        new MaterialAlertDialogBuilder(activity)
                .setTitle("Salir")
                .setMessage("¿Está seguro que desea salir de la aplicación, se borrarán todos los datos almacenados?")
                .setNegativeButton("Cancelar", null)
                .setCancelable(false)
                .setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SqliteClass.getInstance(context).databasehelp.appVehicleSql.deleteAppVehicleTable();
                        SqliteClass.getInstance(context).databasehelp.appDriverSql.deleteAppDriverTable();
                        SqliteClass.getInstance(context).databasehelp.appScheduleSql.deleteAppScheduleTable();
                        SqliteClass.getInstance(context).databasehelp.appStateSql.deleteAppStateTable();
                        SharedPreferences sharedPref = context.getSharedPreferences("login_preferences", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit().clear();
                        editor.apply();
                        activity.finish();
                        Toasty.success(activity, "Sesión cerrada", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(activity, LoginActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        activity.startActivity(i);

                    }
                })
                .show();
    }


}
