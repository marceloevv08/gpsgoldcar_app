package com.connecttix.gpsgoldcar.Utils.Drivers;

import java.util.List;

public class DriversAll {
    private int pk;
    private String name;
    private List<Party_Identifier> party_identifiers;
    private Party_driver party_driver;
    private String is_active;


    public DriversAll(int pk, String name, List<Party_Identifier> party_identifiers, Party_driver party_driver, String is_active) {
        this.pk = pk;
        this.name = name;
        this.party_identifiers = party_identifiers;
        this.party_driver = party_driver;
        this.is_active = is_active;
    }

    public DriversAll() {
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Party_Identifier> getParty_identifiers() {
        return party_identifiers;
    }

    public void setParty_identifiers(List<Party_Identifier> party_identifiers) {
        this.party_identifiers = party_identifiers;
    }

    public Party_driver getParty_driver() {
        return party_driver;
    }

    public void setParty_driver(Party_driver party_driver) {
        this.party_driver = party_driver;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }
}
