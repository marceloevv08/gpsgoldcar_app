package com.connecttix.gpsgoldcar.Views.Fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.connecttix.gpsgoldcar.R;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(),R.style.DialogTheme,(TimePickerDialog.OnTimeSetListener)
                getActivity(),hour,minute,android.text.format.DateFormat.is24HourFormat(getActivity()));
    }
}
