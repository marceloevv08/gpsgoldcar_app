package com.connecttix.gpsgoldcar.Utils.ModalUnit;

public class Geofence {
    private int id;
    private String geofence;

    public Geofence() {
    }

    public Geofence(int id, String geofence) {
        this.id = id;
        this.geofence = geofence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGeofence() {
        return geofence;
    }

    public void setGeofence(String geofence) {
        this.geofence = geofence;
    }
}
